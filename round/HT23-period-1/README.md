![What now?](exercise/hello_world/img/hello_world.png)

Weekly log - 2023 period 1
==========================

This is an overview of the course setup, resources and what happens week by week. The content will be updated all during the course, as each week goes by.

[[_TOC_]]

<!--
IMPROVEMENTS

* Förtydliga labbinstruktionerna så det blir tydligare hur tärningsspelet skall fungera. Främst lab 2 och lab 3.

* Skapa mer lärmaterial så att "programmeringssvaga" får en lättare start in i Java (när de kommer från Python).
    * Övnings- och videoserie som visar handfast hur det kan se ut.

* Kika över Lab 2 som blev lite svår i andra delen, kanske delvis svårt att se framför sig hur koden skall fungera, skapa exempelprogram som visar hur det kan se ut?

* Övning eller labb med Swing GUI?
    * Eventdriven programmeringsmodell
    * Visa bild, text, knappar, rita objekt, on click, keybinding, text input
    * Koda masken?
    * Adventure med GUI?
    * Design mönster MVC för GUI som exempel?
    * Puzzle game
    * Tic tac toe

* Mer exempel där man använder externa lib?
    * url mot quiz-server (med swing)

* Jobba igenom föreläsningarna.
    * Känns som design pattern hamnade på rätt nivå i rätt läge (analys/deisgn i projektet med creator patterns).

* Filhantering, crud
* Mer om typer
    * Collections
* exceptions

* Finns det något som kan tas bort?

* Inkludera mer linters i gradle build, löpande under kursen.
    * Finns övning "linters" som går att bygga på.
* Wrap all tasks (javadoc, test, verification, code coverage, metrics) in Gradle

* Clean code & philosophies
    * Lecture on software philosophies?
    * How about lecture on clean code?
    * Here or next or where?

* Exercise on good and clean code, get metrics on your own code from the labs.

* (Exercise create a pipeline in GitLab and automate unit testing)

SAKER SOM INTE ANVÄNDS

* Fläs: Features of Java and its eco system


TIDIGARE NOTERINGAR, STÄDA UPP:

Extra lectures to put in

* Clean code in Java
    * SOLID
    * Dependency injection

Reuse
* Hur tänka kring objektorientering och principer om snygg kod?
    * https://dbwebb.se/kurser/mvc-v2/forelasning/hur-tanka-kring-objectorientering-och-snygg-kod
* Clean code & metrics 
    * Software documentation (inspelad)
    * What about good and clean code? (inspelad)
    * Software development philosophies (inspelad)
    * Static code analysis (inspelad)
* Design patterns MVC (skapa och återanvänd?)

-->


Prepare yourself
--------------------------

These are things you can do to prepare yourself and get going with the course.

1. Check the [recommended study plan of the course](https://docs.google.com/spreadsheets/d/1aOd0TyOG4GAstisomhNcSb5iqbqwjtB-EsdHwNGCu48/edit?usp=sharing), to get an overview of the course details.

1. [Install the lab environment](../../exercise/lab_environment/README.md).

1. [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).

1. Get the literature and start reading the first chapter in each book.

    1. Core Java SE 9 for the Impatient, 2nd Edition
        * Chapter 1. Fundamental Programming Structures
    1. Object-Oriented Thought Process, 5th Edition
        * Chapter 1. Introduction To Object-Oriented Concepts



Assignments and deadlines
--------------------------

* [A01](./assignment/A01.md) contains a serie of labs which should be done and  submitted together with a lab report.
    * Start work 28/8.
    * Submit code, lab report & oral presentation (show code working) 2, 9, 16/10.

* [A02](./assignment/A02.md) is a two part project, part 1 should be done before starting on part 2.
    * Start work part 1: 2/10.
    * Start work part 2: 16/10.
    * Submit code, lab report 3/11.
        * Oral presentation 23, 30/10.

* Dates for re-examination of A01 & A02.
    * try 1 (re-exam) 2023-12-01 (oral presentation the week after).
    * try 2 (rest-exam) 2024-02-02 (oral presentation the week after).



Literature
--------------------------

The following literature will be used.

1. [Core Java SE 9 for the Impatient, 2nd Edition](https://learning.oreilly.com/library/view/core-java-se/9780134694849/)

1. [Object-Oriented Thought Process, 5th Edition](https://learning.oreilly.com/library/view/the-object-oriented-thought/9780135182130/)

If you enjoy video, the author of Core Java SE 9 has a video serie on "Core Java 11 for the Impatient".

1. [Core Java 11 for the Impatient (video serie)](https://learning.oreilly.com/videos/core-java-11/9780135235331/)



Resources
--------------------------

These are useful resources for reference and learning, check them out and make an actice choice to use them or not.

Official Java documentation.

* [The Java® Language Specification](https://docs.oracle.com/javase/specs/jls/se20/html/index.html)
* [Java® Platform, Standard Edition & Java Development Kit API Specification](https://docs.oracle.com/en/java/javase/20/docs/api/index.html)
* [The Java™ Tutorials](https://docs.oracle.com/javase/tutorial/)

Tutorials providing an insight into the basic constructs of the Java language.

* [JavaPoint Java Tutorial](https://www.javatpoint.com/java-tutorial)
* [Tutorialspoint Java Tutorial](https://www.tutorialspoint.com/java/index.htm)
* [W3Schools Java Tutorial](https://www.w3schools.com/java/default.asp)
* [Learn Java through dev.java](https://dev.java/learn/)



Week 01 (w35): Prepare and get going with Java
--------------------------

<!--
TODO

* Ta det lugnare, glöm inte förklara konstruktorns roll bättre, kanske in med den tydligare i lab 1.
-->

We start up the course, install the lab environment and write an object-oriented program in Java.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with A01 and lab1.
* Thursday is tutoring on campus. Use it to get help with the environment and start assignment 1 with lab 1.

Lectures with slides:

<!--
1. [Course introduction](https://mikael-roos.gitlab.io/java/lecture/course-intro-23/slide.html)
1. [Introduction to Java](https://mikael-roos.gitlab.io/java/lecture/java-intro/slide.html)
-->

1. [Course introduction](https://mikael-roos.gitlab.io/java/lecture/course-intro/slide_23lp1.html)
1. [Introduction to Java](https://mikael-roos.gitlab.io/java/lecture/java-intro/slide.html)
1. [Programming with Java](https://mikael-roos.gitlab.io/java/lecture/programming-with-java/slide.html)

Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 1. Fundamental Programming Structures

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 1. Introduction To Object-Oriented Concepts

1. The Java Tutorials
    * [Lesson: Language Basics](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/index.html)
    * [Lesson: Classes and Objects](https://docs.oracle.com/javase/tutorial/java/javaOO/index.html)

Work:

* Work through the exercise:
    * [Install the lab environment](../../exercise/lab_environment/README.md).
    * [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).
    * [Create a menu driven application](../../exercise/menu/README.md)
* Try out and inspect the source code of the following sample programs to learn more:
    * [argv](../../sample/argv)
    * [dice_single_file](../../sample/dice_single_file)
* [A01](./assignment/A01.md) is presented and you can start working with Lab 1.



Week 02 (w36): Classes, Objects, Inheritance and Composition
--------------------------

<!--

* Exceptions som övning, kanske från ctrl-d i menu?
* Krav i lab att ctrl-d skall fungera?

* Första föreläsningen är lite tung. Gör den enklare (och generell och inte kopplad till Java). Överväg om interface skall vara med i denna veckan eller nästa som föreläsning...?
    * Flytta vissa saker framåt, alla frågor behöver inte besvaras
    * Förenkla och förkorta föreläsningen
    * Lägg till lite bilder på inledningen om OO
    * Förtydliga koncepten inheritance, composition, interface i sliden
    * Sista delen av föreläsningen känns dubbel mot föreläsning 2?

* Föreläsning 2 överlappar lite mot fläs 1. Dubbelkolla vad som tas upp vad och strukturera om.
    * Arv. komposition, interface
    * Abstrakta basklasser (koppling Collections)

* Överväg att plocka bort interface från denna veckan och bygg mer på det i vecka 3.

* Jobba igenom föreläsning 3 också, kanske med lkite förklarande bilder på datastrukturer

Om det skall vara tre föreläsningar så behövs nog 3h föreläsning.

* Lab 2 har krav om att rita uml, men övningen ligger i v3?

* Finns praktisk övning på abstrakt class (eller till vecka 3)?

-->

Code more Java and split your code into files and packages. Work with object oriented techniques and structure your code into classes, method, properties and practice inheritance and composition.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 2.
* Thursday is tutoring on campus. Use it to get help and finalize lab 1 and get started with lab 2.


Lectures with slides:

1. [Object-Oriented Programming Concepts](../../public/lecture/oo-programming-concepts/)
1. [Inheritance and composition with Java](../../public/lecture/inheritance-and-composition-with-java/)
1. [Java Collection Framework](../../public/lecture/collections-with-java/)

Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 2. Object-Oriented Programming
    * Chapter 4. Inheritance and Reflection

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 1. Introduction To Object-Oriented Concepts (same as week 1)

1. The Java Tutorials
    * [Lesson: Inheritance](https://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html) (approx 10 pages)

1. Read the article "[How to Write a Git Commit Message](https://cbea.ms/git-commit/)" to get seven golden rules that helps you in creating better commit messages.


Work:

* Work and finalize lab 1.
* Work with exercises
    * [Split your Java project into files and packages](../../exercise/package/README.md)

* Try out and inspect the sample programs:
    * [python_class](../../sample/python_class) (fun to compare)

* Start working with [A01: Lab 2](./assignment/A01.md).



Week 03 (w37): Interface and implements versus extends
--------------------------

<!--
Lesson plan

* Jobba igenom övningen med Gradle
* Rita uml diagram och använd övningen som bas?
* Föreläs om inheritance, interface & abstrakt klasser
* (Titta på exempelprogrammet med dice_hand_interface, diskutera rätt och fel)

* Visa debugger?
* Exceptions?
* Exempelprogram om UTF-8
* Lab med shape till animals.
* Lägg till class diagram till föreläsningen

-->

Introduce the tool Gradle to use to structure, build and test your Java source code.

Work with the object-oriented concept of interfaces and implements versus the thoughts of inheritance and multiple inheritance.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 3.
* Thursday is tutoring on campus. Use it to get help and finalize lab 2 and get started with lab 3.

Lectures with slides:

* [Design and program with interfaces and abstract classes](../../public/lecture/interface-implements-abstract/README.md)

<!--
* [Features of Java and Java eco system](../../public/lecture/features-of-java/)
-->


Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 3. Interfaces and Lambda Expressions

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 2. How to Think in Terms of Objects

1. The Java Tutorials
    * [Lesson: Interfaces](https://docs.oracle.com/javase/tutorial/java/IandI/createinterface.html) (approx 7 pages)
    * [Lesson: Abstract Methods and Classes](https://docs.oracle.com/javase/tutorial/java/IandI/abstract.html) (1 page)

1. [What is Gradle?](https://docs.gradle.org/current/userguide/userguide.html)

1. [How to Write Doc Comments for the Javadoc Tool](https://www.oracle.com/se/technical-resources/articles/java/javadoc-tool.html)


Work:

* Work and finalize lab 2.
* Work with exercises
    * [Build your Java project with Gradle](../../exercise/gradle/README.md)
    * [UML class diagrams and tools](../../exercise/uml_class_diagram/README.md)
* Try out and inspect the sample programs:
    * [Example code - DiceHand with interfaces](../../sample/dice_hand_interface/)
* Start working with [A01: Lab 3](./assignment/A01.md).



Week 04 (w38): Unit testing with JUnit
--------------------------

<!--

Plan inför onsdagen.

1. Föreläsning om Unit testing
1. Föreläsning om Unit testing i Java
1. Jobba igenom övningen med JUnit (på egen hand)

* Städa bort gamla assignments och labbar, använd enbart de under HT22.
* gradle visa alla varningar
    * checkstyle, kodstil
* Bygg på med föreläsning om "how to write testable code" som passar ihop med snygg kod.

-->

We learn the concept of unit testing and we practice it using JUnit. We write a test suite with test cases to perform unittesting of the code we have written earlier in the course.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 4.
* Thursday is tutoring on campus. Use it to get help and work with the labs.


Lectures with slides:

* [Unit testing](../../public/lecture/unit-testing/)

* [Unit testing with Java](../../public/lecture/unit-testing-with-java/)


Read:

1. Get aquainted with the JUnit project by reading its user documentation "[JUnit 5 User Guide](https://junit.org/junit5/docs/current/user-guide/)".
    * 1. Overview
    * 2. Writing Tests
    * 4. Running Tests

1. Get aquainted with test integration in Gradle.
    * [Testing in Java & JVM projects](https://docs.gradle.org/current/userguide/java_testing.html)
        * There is a section on [Using Junit 5](https://docs.gradle.org/current/userguide/java_testing.html#using_junit5).

1. There is not much to read on the tool "[JaCoCo Java Code Coverage Library](https://www.jacoco.org/jacoco/)" but I would still like to mention it here since we will use it.


Work:

* Work and finalize lab 3.
* Work with exercises
    * [Unit testing with JUnit](../../exercise/junit/README.md)
* Start working with [A01: Lab 4](./assignment/A01.md).



Week 05 (w39): Filemanagement, CRUD and Static code analysis
--------------------------

<!--
Lesson planning

1. Jobba igenom övningen med Sonarcube
1. Föreläsning om kodkvalitet
    * https://docs.sonarqube.org/latest/user-guide/metric-definitions/

TODO
* Add Java Bean when working with JSON/CSV?
* Check your code with checkstyle
(move to its own exercise)
Verification
checkstyle
https://docs.gradle.org/current/userguide/checkstyle_plugin.html
* Spin on to code metrics?
* Add gradle plugin PMD & Checkstyle?
-->

How to work with files and streams in Java. Build an application doing CRUD (Create, Read, Update, Delete) with files. 

Talk about static code analysis and software metrics and how we can use them in quality assurance and to improve our Java code.


Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 5.
* Thursday is tutoring on campus. Use it to get help and work with the labs.


Lectures with slides:

* [Programming with Java - Working with files and streams](../../public/lecture/file-stream-management/README.md)
* [Static code analysis and software metrics](https://mikael-roos.gitlab.io/java/lecture/static-code-analysis/slide.html)

<!--
Review the lecture on static code analysis.
Separate parts related to clean code (?), show metrics, use images from tool, add text based tools for static code analysis.
Dela föreläsningen i två, en som är mer fokus på SonarCube & terminalverktyg.
Lägg till stycke om clean code rörande kod som är enklare att testa
Röd tråd till de andra föreläsningarna som tar upp filosofier.
Kan bli en serie om 5 föreläsningar.

* Finns  övning om linters.

-->


Read:

1. [The Java™ Tutorials Lesson: Basic I/O](https://docs.oracle.com/javase/tutorial/essential/io/)


Work:

* Work and finalize lab 4.
* Work with exercises
    * [File and stream management with Java](../../exercise/file-management/README.md)
    * OPTIONAL [Static code analysis and software metrics with SonarCube](../../exercise/sonarcube/README.md)
* Start working with [A01: Lab 5](./assignment/A01.md).



Week 06 (w40): OO Analysis and Design
--------------------------

<!--
PLAN
* Walk through the project
* Lecture on OOAD
* Show template on how to write the report
* Talk about the different project tasks and what their implications might be

TODO


-->

Start working on the A02 project (part 1).

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with A02 part 1.
* Thursday is tutoring on campus. Use it to get help and work with the labs and project.


Lectures with slides:

* [OO analysis and design](https://mikael-roos.gitlab.io/java/lecture/oo-design/slide.html)


Read:

1. Object-Oriented Thought Process, 5th Edition
    * 6 Designing with Objects
    * 9 Building Objects and Object-Oriented Design


Work:

* Work and finalize lab 5.
* Begin to submit A01, deadline is next week.
* Start working with part 1 of [A02](./assignment/A02.md).



Week 07 (w41): JavaFX - GUI
--------------------------

<!--
TODO

* Simple example on Guess game using MVVM or MVC

-->

Work with your analysis of the A02 project. When you are donde with "good enough", then proceed with the design and perhaps building some prototypes to start coding and understanding the application domain.

Learn how to create graphical user interface with JavaFX and here about architectural design patterns like MVC and MVVM.

Teacher activities:

* Monday is lecture & tutoring on campus, talking about GUI and how to proceed with the project.
* Thursday is tutoring on campus. Use it to get help and work with the labs and project.


Lectures with slides:

* [User interface with JavaFX](../../public/lecture/user-interface-javafx/README.md)


Read:

* Study about JavaFX as needed, if you choose to work with it in the project.
    * [JavaFX Documentation Project](https://fxdocs.github.io/docs/html5/) as a guide and tutorial on how to work with JavaFX.
    * [JavaFX Tutorial](https://jenkov.com/tutorials/javafx/index.html) with code examples on the ui elements and some of the various options for layout.

* Review the basics on related architectural design patterns for GUI applications.
    * [Wikipedia on Model View ViewModel (MVVM)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel)
    * [Wikipedia on Model View Controller (MVC)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)


Work:

* Work with exercises
    * [User interface with JavaFX](../../exercise/user-interface-javafx/README.md)
* Work with A02 (analysis, design, prototypes - investigate the application domain).
* (Submit and present A01)



Week 08 (w42): Design patterns
--------------------------

<!--

TODO

* What is design patterns
* Singleton, Iterator, Observer och Factory

Resources design patterns
https://www.youtube.com/watch?v=tv-_1er1mWI
https://refactoring.guru/design-patterns/
https://en.wikipedia.org/wiki/Design_Patterns
https://blog.codinghorror.com/rethinking-design-patterns/

* Add more examples to sample/designpatterns.

-->

Lecture on software design patterns. Try to use this knowledge in your project to structure your code.

Teacher activities:

* Monday is lecture & tutoring on campus, work with project.
* Thursday is tutoring on campus. Use it to get help and work with the labs and project.

Lectures with slides:

* [Design patterns](../../public/lecture/design-patterns/README.md)

Read:

1. Object-Oriented Thought Process, 5th Edition
    * 15. Design Patterns

Work:

* Work with A02, try to integrate design patterns into your solution.

<!--
* Check code samples
    * [Design patterns](../../sample/design-patterns)

TODO Needs more example with builder and abstract factory 
-->



Week 09 (w43): Work with project
--------------------------

Work with the A02 project, next week is submission time.

Teacher activities:

* Monday is tutoring on campus, work with project.
* Thursday is tutoring on campus. Use it to get help and work with the labs and project.

<!--
Lectures with slides:

* TBD

Read:

* TBD
-->

Work:

* Work with A02.



Week 10 (w44): Examination
--------------------------

The last week of the course where you finalize and submit and present your project.

Teacher activities:

* Monday is tutoring on campus, work with project.
* Thursday is tutoring on campus. Use it to get help and work with the labs and project.

<!--
Lectures with slides:

* TBD

Read:

* TBD
-->

Work:

* Done and submit A02.
