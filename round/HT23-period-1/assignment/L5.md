---
revision:
    "2023-09-21": "(A, mos) First release (new from last year)."
---
Lab 5: Work with files (CRUD)
==========================

Put all your code in the directory `lab5`.

CRUD means file operations for:

* Create
* Read
* Update
* Delete

[[_TOC_]]

<!--
TODO

* Make config.json as required. Use serialisation for some part? To enable that they must use different variants of filemanagement
* Add unit test and ensure the data dir is in test/data?
-->


About
--------------------------

This lab is separated into tasks, some tasks are mandatory and some tasks are optional.

Each task has points attached to it. You need to collect 10 points to pass this lab. Collecting more points might help you achive a higher grade.

The number of points assigned to a lab does not reflect the work needed to solve the lab, nor the complexity of the lab. The points is simply a way to calculate the base for your grade.



Prepare
--------------------------

You should have worked through the exercise.

1. [File and stream management with Java](../../exercise/file-management/README.md)



Task 1: Work with gradle (1p)
--------------------------

1. Put your code into a Gradle structure so it can be build and run using `.gradlew run`.

1. Generate JavaDocs for your code.

1. Feel free to add unit testing to your code (optional and extra points).



Task 2: A receipt file database (9p)
--------------------------

Create a menu driven application "Online Farmacy" where you can manage (CRUD) receipts on drugs. A receipt on a drug contains at least:

* A unique id
* Date and time for issued
* The name of the drug
* Number of packages of the drug
* Price for each package

Each receipt can only contain one drug.

In your menu you should be able to at least do the following:

1. View all receipts
1. View a receipt
1. Add a new receipt
1. Update/change a receipt
1. Remove a receipt
1. Export all receipts as JSON
1. Export all receipts as CSV

Showing a receipt also shows the total sum it costs.

There should be at least 5 receipts in your storage to start with. Each receipt should be stored in its own file.

Create and use a configuration file (JSON) where you store the name of your "Online Farmacy" and the path to the directory where all receipts are stored.



Task 3: Unit testing (optional 2p)
--------------------------

Add unit tests for each class you have, add at least a couple of unit tests in each class. Reach 50% coverage or more. Remember that it is not coverage alone that is the proof of a proper test suite.

Consider, how can one test an application that works with files? Could you perhaps use the configuration file to run the tests in another storage directory?



Task 4: Sonarcube code quality (optional 2p)
--------------------------

Work through the exercise on "[Static code analysis and software metrics with SonarCube](../../exercise/sonarcube/README.md)" and run this lab with SonarCube and analyse it.

Show images on how your code quality rating improved with some fixes you make.



Questions to answer
--------------------------

Questions to answer in the lab report (approximate 1-2 page of text or more when including images).

1. What are your learnings from working with files and streams in Java?

1. How satisfied are you with the application code (and your test suite), do you see any improvement areas to make improvements?

1. Did you do any of the optional part, if so, elaborate on how you solved it.

1. What is your TIL?
