![What now?](exercise/hello_world/img/hello_world.png)

Weekly log - 2024 period 1
==========================

This is an overview of the course setup, resources and what happens week by week. The content will be updated all during the course, as each week goes by.

[[_TOC_]]

<!--
IMPROVEMENTS

* Skapa mer lärmaterial så att "programmeringssvaga" får en lättare start in i Java (när de kommer från Python).
    * Övnings- och videoserie som visar handfast hur det kan se ut.

* Förtydliga labbinstruktionerna så det blir tydligare hur tärningsspelet skall fungera. Främst lab 2 och lab 3.

* Kika över Lab 2 som blev lite svår i andra delen, kanske delvis svårt att se framför sig hur koden skall fungera, skapa exempelprogram som visar hur det kan se ut?

* Mer exempel där man använder externa lib?
    * url mot quiz-server (med swing)

* Övning eller labb med Swing GUI?
    * Eventdriven programmeringsmodell
    * Visa bild, text, knappar, rita objekt, on click, keybinding, text input
    * Koda masken?
    * Adventure med GUI?
    * Design mönster MVC för GUI som exempel?
    * Puzzle game
    * Tic tac toe

* Jobba igenom föreläsningarna.
    * Känns som design pattern hamnade på rätt nivå i rätt läge (analys/deisgn i projektet med creator patterns).

* Mer om typer
    * Collections
* exceptions

* Finns det något som kan tas bort?

* Inkludera mer linters i gradle build, löpande under kursen.
    * Finns övning "linters" som går att bygga på.
* Wrap all tasks (javadoc, test, verification, code coverage, metrics) in Gradle

* Clean code & philosophies
    * Lecture on software philosophies?
    * How about lecture on clean code?
    * Here or next or where?

* Exercise on good and clean code, get metrics on your own code from the labs.

* (Exercise create a pipeline in GitLab and automate unit testing)

SAKER SOM INTE ANVÄNDS

* Fläs: Features of Java and its eco system


TIDIGARE NOTERINGAR, STÄDA UPP:

Extra lectures to put in

* Clean code in Java
    * SOLID
    * Dependency injection

Reuse
* Hur tänka kring objektorientering och principer om snygg kod?
    * https://dbwebb.se/kurser/mvc-v2/forelasning/hur-tanka-kring-objectorientering-och-snygg-kod
* Clean code & metrics 
    * Software documentation (inspelad)
    * What about good and clean code? (inspelad)
    * Software development philosophies (inspelad)
    * Static code analysis (inspelad)
* Design patterns MVC (skapa och återanvänd?)

-->


Prepare yourself
--------------------------

These are things you can do to prepare yourself and get going with the course.

1. Check the [recommended study plan of the course](https://docs.google.com/spreadsheets/d/1aOd0TyOG4GAstisomhNcSb5iqbqwjtB-EsdHwNGCu48/edit?usp=sharing&gid=1760951067#gid=1760951067), to get an overview of the course details.

1. [Install the lab environment](../../exercise/lab_environment/README.md).

1. [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).

1. Get the literature and start reading the first chapter in each book.

    1. Core Java SE 9 for the Impatient, 2nd Edition
        * Chapter 1. Fundamental Programming Structures
    1. Object-Oriented Thought Process, 5th Edition
        * Chapter 1. Introduction To Object-Oriented Concepts



Literature
--------------------------

The following literature will be used.

1. [Core Java SE 9 for the Impatient, 2nd Edition](https://learning.oreilly.com/library/view/core-java-se/9780134694849/)

1. [Object-Oriented Thought Process, 5th Edition](https://learning.oreilly.com/library/view/the-object-oriented-thought/9780135182130/)

If you enjoy video, the author of Core Java SE 9 has a video serie on "Core Java 11 for the Impatient".

1. [Core Java 11 for the Impatient (video serie)](https://learning.oreilly.com/videos/core-java-11/9780135235331/)



Resources
--------------------------

These are useful resources for reference and learning, check them out and make an actice choice to use them or not.

Official Java documentation.

* [The Java® Language Specification](https://docs.oracle.com/javase/specs/jls/se21/html/index.html)
* [Java® Platform, Standard Edition & Java Development Kit API Specification](https://docs.oracle.com/en/java/javase/21/docs/api/index.html)
* [The Java™ Tutorials](https://docs.oracle.com/javase/tutorial/)

Tutorials providing an insight into the basic constructs of the Java language.

* [JavaPoint Java Tutorial](https://www.javatpoint.com/java-tutorial)
* [Tutorialspoint Java Tutorial](https://www.tutorialspoint.com/java/index.htm)
* [W3Schools Java Tutorial](https://www.w3schools.com/java/default.asp)
* [Learn Java through dev.java](https://dev.java/learn/)



Assignments and deadlines
--------------------------

* [A01](./assignment/A01.md) contains a serie of labs which should be done and  submitted together with a lab report.
    * Start work course week 1.
    * Submit code, lab report & oral (or video) presentation (show code working) course week 6, 7, 8.

* [A02](./assignment/A02.md) is a two part project, part 1 should be done before starting on part 2.
    * Start work part 1: course week 6.
    * Start work part 2: course week 8.
    * Submit code, lab report end of course week 10.
        * Oral (or video) presentation 23, course week 10.

* Dates for re-examination of A01 & A02.
    * try 2 (re-exam) 2024-12-01 (oral/video presentation the week after).
    * try 3 (rest-exam) 2025-02-02 (oral/video presentation the week after).



Week 01 (w36): Prepare and get going with Java
--------------------------

<!--
TODO

* Efter första förläsning & labbtid om 2h hinner man installera labbmiljön. Man hinner inte riktigt igång med sista övningen som förbereder inför labben med menysystemet. (det kan man egentlige ha en separat session med)
* Man kan vara tydligare med själva installationsdelen. Vilket är det föredragna sättet att installera? Vad skall installeras för extension i vscode?
-->

We start up the course, install the lab environment and write an object-oriented program in Java.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with A01 and lab1.
* Tuesday is tutoring on campus. Use it to get help with the environment and start assignment 1 with lab 1.

Lectures with slides:

1. [Course introduction](https://mikael-roos.gitlab.io/java/lecture/course-intro/slide_24lp1.html)
1. [Introduction to Java](https://mikael-roos.gitlab.io/java/lecture/java-intro/slide.html)
1. [Programming with Java](https://mikael-roos.gitlab.io/java/lecture/programming-with-java/slide.html)

Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 1. Fundamental Programming Structures

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 1. Introduction To Object-Oriented Concepts

1. The Java Tutorials
    * [Lesson: Language Basics](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/index.html)
    * [Lesson: Classes and Objects](https://docs.oracle.com/javase/tutorial/java/javaOO/index.html)

Work:

* Work through the exercise:
    * [Install the lab environment](../../exercise/lab_environment/README.md).
    * [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).
    * [Create a menu driven application](../../exercise/menu/README.md)
* Try out and inspect the source code of the following sample programs to learn more:
    * [argv](../../sample/argv)
    * [dice_single_file](../../sample/dice_single_file)
* [A01](./assignment/A01.md) is presented and you can start working with Lab 1.



Week 02 (w37): Classes, Objects, Inheritance and Composition
--------------------------

<!--
TODO

* Övning med collections (kodexempel finns), bygg på delar av de olika kodexemplen?

* Föreläsning collections
    * Förtydliga vilka metoder som implementeras i respektive collection för att visa vad som skiljer emellan dem.

-->

Code more Java and split your code into files and packages. Work with object oriented techniques and structure your code into classes, method, properties and practice inheritance and composition.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 2.
* Tuesday is tutoring on campus. Use it to get help and finalize lab 1 and get started with lab 2.


Lectures with slides, code samples and reading instructions:

1. [Object-Oriented Programming Concepts](../../public/lecture/oo-programming-concepts/)
1. [Inheritance and composition with Java](../../public/lecture/inheritance-and-composition-with-java/)
1. [Java Collection Framework](../../public/lecture/collections-with-java/)

Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 2. Object-Oriented Programming
    * Chapter 4. Inheritance and Reflection

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 1. Introduction To Object-Oriented Concepts (same as week 1)

1. The Java Tutorials
    * [Lesson: Inheritance](https://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html) (approx 10 pages)
    * [Lesson: Introduction to Collections](https://docs.oracle.com/javase/tutorial/collections/intro/index.html) (roughly review the different types of collections)

1. Read the article "[How to Write a Git Commit Message](https://cbea.ms/git-commit/)" to get seven golden rules that helps you in creating better commit messages.


Work:

* Work and finalize lab 1.
* Work with exercises
    * [Split your Java project into files and packages](../../exercise/package/README.md)

* Try out and inspect the sample programs:
    * [python_class](../../sample/python_class) (fun to compare)

* Start working with [A01: Lab 2](./assignment/A01.md).



Week 03 (w38): Interface and implements versus extends
--------------------------

<!--
* Exceptions
    * Exceptions som övning, kanske från ctrl-d i menu?
    * Krav i lab att ctrl-d skall fungera?

Lesson plan

* Gör föreläsningan om interface/abstract
    * Lägg till uml-bilder så att klassidagrammet framträder
    * Inled med att förklara begreppe interface & abstract

* Jobba igenom övningen med Gradle

* Rita uml diagram och använd övningen som bas?
    * UML dök upp i lab, så knyt ihop den arvshirarkin och bygg på. Kanske rita UML utifrån något i collections hierarkin?
* Titta på exempelprogrammet med dice_hand_interface, diskutera rätt och fel

* Week2?
    * Removed a part of interace from the first lecture, reuse here?
    * Föreläsning 2 kommenterade ut diverse bra att ha saker i slutet av den föreläsningen, när ta upp?
        * Modifiers
            * static
            * abstract
            * final
        * Nested classes


* Visa debugger?
* Exceptions?
* Exempelprogram om UTF-8
* Lägg till class diagram till föreläsningen
* Finns praktisk övning på abstrakt class eller fixa till en.
* Exempelprogram till static, men det kan vända till design patterns och ta som ett exempel på Singleton.

-->

Introduce the tool Gradle to structure, build and test your Java source code.

Work with the object-oriented concept of interface and implement versus the thoughts of inheritance, include abstract classes.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 3.
* Tuesday is tutoring on campus. Use it to get help and finalize lab 2 and get started with lab 3.

Lectures with slides:

* [Design and program with interfaces and abstract classes](../../public/lecture/interface-implements-abstract/README.md)

<!--
* [Features of Java and Java eco system](../../public/lecture/features-of-java/)
-->


Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 3. Interfaces and Lambda Expressions

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 2. How to Think in Terms of Objects

1. The Java Tutorials
    * [Lesson: Interfaces](https://docs.oracle.com/javase/tutorial/java/IandI/createinterface.html) (approx 7 pages)
    * [Lesson: Abstract Methods and Classes](https://docs.oracle.com/javase/tutorial/java/IandI/abstract.html) (1 page)

1. [What is Gradle?](https://docs.gradle.org/current/userguide/userguide.html)

1. [How to Write Doc Comments for the Javadoc Tool](https://www.oracle.com/se/technical-resources/articles/java/javadoc-tool.html)


Work:

* Work and finalize lab 2.
* Work with exercises
    * [Build your Java project with Gradle](../../exercise/gradle/README.md)
    * [UML class diagrams and tools](../../exercise/uml_class_diagram/README.md)
* Try out and inspect the sample programs:
    * [Example code - DiceHand with interfaces](../../sample/dice_hand_interface/)
    * [Example code - Gradle](../../sample/gradle/)
    * [Example code - Gradle with multiple main](../../sample/gradle-multiple-main/)
* Start working with [A01: Lab 3](./assignment/A01.md).



Week 04 (w39): Unit testing with JUnit
--------------------------

<!--

Plan inför onsdagen.

1. Föreläsning om Unit testing
1. Föreläsning om Unit testing i Java
1. Jobba igenom övningen med JUnit (på egen hand)

* gradle visa alla varningar
    * checkstyle, kodstil
* Bygg på med föreläsning om "how to write testable code" som passar ihop med snygg kod.

-->

We learn the concept of unit testing and we practice it using JUnit. We write a test suite with test cases to perform unittesting of the code we have written earlier in the course.

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 4.
* Tuesday is tutoring on campus. Use it to get help and work with the labs.


Lectures with slides:

* [Unit testing](../../public/lecture/unit-testing/)

* [Unit testing with Java](../../public/lecture/unit-testing-with-java/)


Read:

1. Get aquainted with the JUnit project by reading its user documentation "[JUnit 5 User Guide](https://junit.org/junit5/docs/current/user-guide/)".
    * 1. Overview
    * 2. Writing Tests
    * 4. Running Tests

1. Get aquainted with test integration in Gradle.
    * [Testing in Java & JVM projects](https://docs.gradle.org/current/userguide/java_testing.html)
        * There is a section on [Using Junit 5](https://docs.gradle.org/current/userguide/java_testing.html#using_junit5).

1. There is not much to read on the tool "[JaCoCo Java Code Coverage Library](https://www.jacoco.org/jacoco/)" but I would still like to mention it here since we will use it.


Work:

* Work and finalize lab 3.
* Work with exercises
    * [Unit testing with JUnit](../../exercise/junit/README.md)
* Start working with [A01: Lab 4](./assignment/A01.md).



Week 05 (w40): Filemanagement, CRUD and Static code analysis
--------------------------

<!--
Lesson planning

1. Föreläs om files & streams
1. Föreläs om static code analysis

TODO

* Add Java Bean when working with JSON/CSV in the file exercise

1. Lägg till föreläsning om SonarCube, checkstyle, code standard så man ser hur man skall göra i Java

    * googleJavaFormat()
    * prettier()
    * clangFormat()

* Check your code with checkstyle
(move to its own exercise)
Verification
checkstyle
https://docs.gradle.org/current/userguide/checkstyle_plugin.html
* Spin on to code metrics?
* Add gradle plugin PMD & Checkstyle?
* Add to vscode?

* https://alanzeichick.com/2016/11/wheres-best-java-coding-style-guide-not-oracle.html

PS: Håll Good & Clean Code borta från detta då det är mer "filosofiskt", att börja med att använda verktyg som identifierar svaheter i programmeringen är tillräckligt bra.

-->

How to work with files and streams in Java. Build an application doing CRUD (Create, Read, Update, Delete) with files. 

Talk about static code analysis and software metrics and how we can use them in quality assurance and to improve our Java code.


Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with lab 5.
* Tuesday is tutoring on campus. Use it to get help and work with the labs.


Lectures with slides:

* [Programming with Java - Working with files and streams](../../public/lecture/file-stream-management/README.md)
* [Static code analysis and software metrics](https://mikael-roos.gitlab.io/java/lecture/static-code-analysis/slide.html)


Read:

1. [The Java™ Tutorials Lesson: Basic I/O](https://docs.oracle.com/javase/tutorial/essential/io/)


Work:

* Work and finalize lab 4.
* Work with exercises
    * [File and stream management with Java](../../exercise/file-management/README.md)
    * OPTIONAL [Static code analysis and software metrics with SonarCube](../../exercise/sonarcube/README.md)
* Start working with [A01: Lab 5](./assignment/A01.md).



Week 06 (w41): OO Analysis and Design
--------------------------

<!--
PLAN
* Walk through the project
* Lecture on OOAD
* Show template on how to write the report
* Talk about the different project tasks and what their implications might be

TODO

* Review the lecture, make it more crisp, add example with images, less slides with text

-->

Start working on the A02 project (part 1).

Teacher activities:

* Monday is lecture & tutoring on campus, aiming at presenting the details needed to start with A02 part 1.
* Tuesday is tutoring on campus. Use it to get help and work with the labs and project.


Lectures with slides:

* [OO analysis and design](https://mikael-roos.gitlab.io/java/lecture/oo-design/slide.html)


Read:

1. Object-Oriented Thought Process, 5th Edition
    * 6 Designing with Objects
    * 9 Building Objects and Object-Oriented Design


Work:

* Work and finalize lab 5.
* Begin to submit A01, deadline is next week.
* Start working with part 1 of [A02](./assignment/A02.md).



Week 07 (w42): JavaFX - GUI
--------------------------

<!--
TODO

* Simple example on Guess game using MVVM or MVC

* Text GUI as alternative?
    * Perhaps asteroids game or equal?
    * https://github.com/mabe02/lanterna
-->

Work with your analysis of the A02 project. When it is "good enough", then proceed with the design and perhaps build some prototypes to start coding and understanding the application domain.

Learn how to create graphical user interface with JavaFX and here about architectural design patterns like MVC and MVVM.

Teacher activities:

* Monday is lecture & tutoring on campus, talking about GUI and how to proceed with the project.
* Tuesday is tutoring on campus. Use it to get help and work with the labs and project.


Lectures with slides:

* [User interface with JavaFX](../../public/lecture/user-interface-javafx/README.md)


Read:

* Study about JavaFX as needed, if you choose to work with it in the project.
    * [JavaFX Documentation Project](https://fxdocs.github.io/docs/html5/) as a guide and tutorial on how to work with JavaFX.
    * [JavaFX Tutorial](https://jenkov.com/tutorials/javafx/index.html) with code examples on the ui elements and some of the various options for layout.

* Review the basics on related architectural design patterns for GUI applications.
    * [Wikipedia on Model View ViewModel (MVVM)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel)
    * [Wikipedia on Model View Controller (MVC)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)


Work:

* Work with exercises
    * [User interface with JavaFX](../../exercise/user-interface-javafx/README.md)
* Work with A02 (analysis, design, prototypes - investigate the application domain).
* (Submit and present A01)



Week 08 (w43): Design patterns
--------------------------

<!--
TODO

* Could add more code example with builder and abstract factory

* Slide, could add UML diagrams for design patterns
-->

Lecture on software design patterns. Try to use this knowledge in your project to structure your code.

Teacher activities:

* Monday is lecture & tutoring on campus, work with project.
* Tuesday is tutoring on campus. Use it to get help and work with the labs and project.

Lectures with slides:

* [Design patterns](../../public/lecture/design-patterns/README.md)

Read:

1. Object-Oriented Thought Process, 5th Edition
    * 15. Design Patterns

Work:

* Work with A02, try to integrate design patterns into your solution.

<!--
* Check code samples on design patterns
    * [Design patterns](../../sample/design-patterns)
-->



Week 09 (w44): Work with project
--------------------------

Work with the A02 project, next week is submission time.

Teacher activities:

* Monday is tutoring on campus, work with project.
* Tuesday is tutoring on campus. Use it to get help and work with the labs and project.

<!--
Lectures with slides:

* TBD

Read:

* TBD
-->

Work:

* Work with A02.



Week 10 (w45): Examination
--------------------------

The last week of the course where you finalize and submit and present your project.

Teacher activities:

* Monday is tutoring on campus, work with project.
* Tuesday is tutoring on campus. Use it to get help and work with the labs and project.

<!--
Lectures with slides:

* TBD

Read:

* TBD
-->

Work:

* Be done and submit A02.
