#!/bin/bash

# Run the script locally (replace `curl` with the correct command if you want to fetch a script from a URL)
# bash a01-check.bash

# Run the script locally, download the script from a URL
# curl -s "https://gitlab.com/mikael-roos/java/-/raw/main/round/HT24-period-2/script/a01-check.bash" | bash

# Expand an archive for testing purposes
# unzip -o "$zipFile" -d "$destinationFolder"

# Define the paths to check
paths=(
    "a01/README.md"
    "a01/report.pdf"
    "a01/task_01"
    "a01/task_02"
    "a01/task_02/gradle"
    "a01/task_02/app/src/main/java/se/company/resource/Employee.java"
    "a01/task_02/app/src/main/java/se/company/resource/NormalEmployee.java"
    "a01/task_02/app/src/main/java/se/company/resource/SuperEmployee.java"
    "a01/task_02/app/src/main/java/se/company/resource/SuperPower.java"
    "a01/task_02/app/src/main/java/se/company/resource/Team.java"
    "a01/task_03"
    "a01/task_03/gradle"
    "a01/task_03/app/src/main/java/se/adlez/game/AbstractItem.java"
    "a01/task_03/app/src/main/java/se/adlez/game/AbstractMoveableItem.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Castle.java"
    "a01/task_03/app/src/main/java/se/adlez/game/FirTree.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Forest.java"
    "a01/task_03/app/src/main/java/se/adlez/game/ForestToFile.java"
    "a01/task_03/app/src/main/java/se/adlez/game/ForestToJson.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Item.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Moveable.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Position.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Robot.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Rock.java"
    "a01/task_03/app/src/main/java/se/adlez/game/Wolf.java"
)

# Check each path
for path in "${paths[@]}"; do
    if [[ -e "$path" ]]; then
        echo "✅ $path"
    else
        echo "❌ $path IS MISSING."
    fi
done

# Create the zip file
source="a01"
destination="a01.zip"

if zip -r "$destination" "$source" >/dev/null; then
    echo "✅ ZIP file created successfully: $destination"
else
    echo "❌ Failed to create ZIP file."
fi
