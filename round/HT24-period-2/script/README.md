---
revision:
    "2025-01-06": "(A, mos) First release."
---
Prepare and verify your submission
===========================

This is documentation and how-to for scripts that aids in submission of assignments and to verify that some/all parts are in the correct place.

[[_TOC_]]

<!--
TODO

* Add yellow bricks for the optional parts.
-->



A01 checker and zipper
---------------------------

The A01 checker script will verify that some of the directories and files are in the correct place and it will uze zip to create an archive `a01.zip` that can be submitted.

The first thing you should do is to go to the assignment specification and visually check that you have the correct directory structure. Then go to that directory.

```
# Go to the directory where a01/ is

# Verify visually that your structure is the same as the requested structure
# in the assignment specification.
ls a01
```

Now you can run the script, depending on your platform.



<details>
<summary>Windows PowerShell: Run a01-check.ps1.</summary>

The source code for the script can be [reviewed online](a01-check.ps1).

Download and execute the script like this.

```powershell
# Execute the script locally, download script from url
Invoke-Expression (Invoke-WebRequest -Uri "https://gitlab.com/mikael-roos/java/-/raw/main/round/HT24-period-2/script/a01-check.ps1" -UseBasicParsing).Content
```

The output from the execution can something look like this.

```
✅ a01\README.md
❌ a01\report.pdf IS MISSING.
❌ a01\task_01 IS MISSING.
✅ a01\task_02
✅ a01\task_02\gradle
✅ a01\task_02\app\src\main\java\se\company\resource\Employee.java
✅ a01\task_02\app\src\main\java\se\company\resource\NormalEmployee.java
✅ a01\task_02\app\src\main\java\se\company\resource\SuperEmployee.java
✅ a01\task_02\app\src\main\java\se\company\resource\SuperPower.java
✅ a01\task_02\app\src\main\java\se\company\resource\Team.java
✅ a01\task_03
✅ a01\task_03\gradle
✅ a01\task_03\app\src\main\java\se\adlez\game\AbstractItem.java
✅ a01\task_03\app\src\main\java\se\adlez\game\AbstractMoveableItem.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Castle.java
✅ a01\task_03\app\src\main\java\se\adlez\game\FirTree.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Forest.java
✅ a01\task_03\app\src\main\java\se\adlez\game\ForestToFile.java
✅ a01\task_03\app\src\main\java\se\adlez\game\ForestToJson.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Item.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Moveable.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Position.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Robot.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Rock.java
✅ a01\task_03\app\src\main\java\se\adlez\game\Wolf.java
✅ ZIP file created successfully: a01.zip
```

You should fix all red to become green to ensure your submission contains the vital parts.

As a final result you will get the file `a01.zip` created for you. 

Check that the zip file was generated, it can look something like this.

```bash
$ ls -l a01.zip 
-rw-rw-r-- 1 mos mos 442K jan  6 14:50 a01.zip
```

You can open that file in your filemanager to view its content, or you can execute the following command in the terminal to unpack it into a test directory `test_a01`.

```powershell
# Exapand archive for testing purpose
Expand-Archive -Path a01.zip -DestinationPath test_a01 -Force
```

</details>



<details>
<summary>Linux, Mac, Windows with bash terminal: Run a01-check.bash.</summary>

The source code for the script can be [reviewed online](a01-check.bash).

Download and execute the script like this by either using curl or wget.

Opt to use curl.

```bash
# Using curl
curl -s "https://gitlab.com/mikael-roos/java/-/raw/main/round/HT24-period-2/script/a01-check.bash" | bash
```

Or opt to use wget.

```bash
# Using wget
wget -qO- "https://gitlab.com/mikael-roos/java/-/raw/main/round/HT24-period-2/script/a01-check.bash" | bash
```

The output from the execution can something look like this.

```
❌ a01/README.md IS MISSING.
❌ a01/report.pdf IS MISSING.
❌ a01/task_01 IS MISSING.
✅ a01/task_02
✅ a01/task_02/gradle
✅ a01/task_02/app/src/main/java/se/company/resource/Employee.java
✅ a01/task_02/app/src/main/java/se/company/resource/NormalEmployee.java
✅ a01/task_02/app/src/main/java/se/company/resource/SuperEmployee.java
✅ a01/task_02/app/src/main/java/se/company/resource/SuperPower.java
✅ a01/task_02/app/src/main/java/se/company/resource/Team.java
✅ a01/task_03
✅ a01/task_03/gradle
✅ a01/task_03/app/src/main/java/se/adlez/game/AbstractItem.java
✅ a01/task_03/app/src/main/java/se/adlez/game/AbstractMoveableItem.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Castle.java
✅ a01/task_03/app/src/main/java/se/adlez/game/FirTree.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Forest.java
✅ a01/task_03/app/src/main/java/se/adlez/game/ForestToFile.java
✅ a01/task_03/app/src/main/java/se/adlez/game/ForestToJson.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Item.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Moveable.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Position.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Robot.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Rock.java
✅ a01/task_03/app/src/main/java/se/adlez/game/Wolf.java
✅ ZIP file created successfully: a01.zip
```

You should fix all red to become green to ensure your submission contains the vital parts.

As a final result you will get the file `a01.zip` created for you. 

Check that the zip file was generated, it can look something like this.

```bash
$ ls -l a01.zip 
-rw-rw-r-- 1 mos mos 442K jan  6 14:50 a01.zip
```

You can open that file in your filemanager to view its content, or you can execute the following command to unpack it into a test directory `test_a01`.

```bash
# Expand an archive for testing purposes
unzip -o a01.zip -d test_a01
```

</details>



When you are done you will have the zipfile that you can submit.
