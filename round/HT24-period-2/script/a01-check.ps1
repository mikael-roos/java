# Execute the script locally
# powershell -ExecutionPolicy Bypass -File 'a01-check.ps1'

# Execute the script locally, download script from url
# Invoke-Expression (Invoke-WebRequest -Uri "https://gitlab.com/mikael-roos/java/-/raw/main/round/HT24-period-2/script/a01-check.ps1" -UseBasicParsing).Content

# Exapand archive for testing purpose
# Expand-Archive -Path $zipFile -DestinationPath $destinationFolder -Force

# Define the paths to check
$paths = @(
    "a01\README.md",
    "a01\report.pdf",
    "a01\task_01",
    "a01\task_02",
    "a01\task_02\gradle",
    "a01\task_02\app\src\main\java\se\company\resource\Employee.java",
    "a01\task_02\app\src\main\java\se\company\resource\NormalEmployee.java",
    "a01\task_02\app\src\main\java\se\company\resource\SuperEmployee.java",
    "a01\task_02\app\src\main\java\se\company\resource\SuperPower.java",
    "a01\task_02\app\src\main\java\se\company\resource\Team.java",
    "a01\task_03",
    "a01\task_03\gradle",
    "a01\task_03\app\src\main\java\se\adlez\game\AbstractItem.java",
    "a01\task_03\app\src\main\java\se\adlez\game\AbstractMoveableItem.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Castle.java",
    "a01\task_03\app\src\main\java\se\adlez\game\FirTree.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Forest.java",
    "a01\task_03\app\src\main\java\se\adlez\game\ForestToFile.java",
    "a01\task_03\app\src\main\java\se\adlez\game\ForestToJson.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Item.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Moveable.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Position.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Robot.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Rock.java",
    "a01\task_03\app\src\main\java\se\adlez\game\Wolf.java"
)

# Check each path
foreach ($path in $paths) {
    if (Test-Path $path) {
        Write-Output "✅ $path"
    } else {
        Write-Output "❌ $path IS MISSING."
    }
}

# Create the zip file
$source = "a01"
$destination = "a01.zip"
Compress-Archive -Force -Path $source -DestinationPath $destination

if (Test-Path $destination) {
    Write-Output "✅ ZIP file created successfully: $destination"
} else {
    Write-Output "❌ Failed to create ZIP file."
}
