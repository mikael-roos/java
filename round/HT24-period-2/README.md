![What now?](exercise/hello_world/img/hello_world.png)

Weekly log - 2024 period 2
==========================

This is an overview of the course setup, resources and what happens week by week. The content will be updated all during the course, as each week goes by. This document is the teacher's material and used for planning and communication with the students on what to do each week.

[[_TOC_]]

<!--
IMPROVEMENTS

* Setters and getters
* Method overloading
* (Nested class)
* Exceptions
* Jobba mer med Collections?

* Hur placera in problemlösning? Flödesdiagram, pseudo code, sekvensdiagram ?

* Play with date and time, create a calender (lab?, del av task?)
https://docs.oracle.com/javase/tutorial/datetime/iso/datetime.html

* Skapa mer lärmaterial så att "programmeringssvaga" får en lättare start in i Java (när de kommer från Python).
    * Övnings- och videoserie som visar handfast hur det kan se ut.
    * Installation av java
    * Mitt första java program
    * Installera och jobba med gradle
    * Spela in programmeringsövningar som även kan ske live

* Inkludera mer linters i gradle build, löpande under kursen.
    * Finns övning "linters" som går att bygga på.
    * Wrap all tasks (javadoc, test, verification, code coverage, metrics) in Gradle

-->


<!--
Prepare yourself
--------------------------

These are things you can do to prepare yourself and get going with the course.

1. Check the [recommended study plan of the course](https://docs.google.com/spreadsheets/d/1aOd0TyOG4GAstisomhNcSb5iqbqwjtB-EsdHwNGCu48/edit?usp=sharing&gid=599187012#gid=599187012), to get an overview of the course details.

1. [Install the lab environment](../../exercise/lab_environment/README.md).

1. [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).

1. Get the literature and start reading the first chapter in each book.

    1. Core Java SE 9 for the Impatient, 2nd Edition
        * Chapter 1. Fundamental Programming Structures
    1. Object-Oriented Thought Process, 5th Edition
        * Chapter 1. Introduction To Object-Oriented Concepts
-->

<!--
For the fun of it, show how to get going with some Java games.
https://github.com/AlexPetrusca/java-games
-->



Literature
--------------------------

You might find that the literature is "free and online" if you access it through your library and identify yourself as a student or member of a school.

The following literature will be used and it is one book to learn Java programming language and one book to talk about general concepts in object orientation.

This is alternative 1 for the books.

* [Java™ How To Program (Early Objects), Tenth Edition](https://learning.oreilly.com/library/view/javatm-how-to/9780133813036/?ar=) (should use edition 11)
* [UML @ Classroom An Introduction to Object-Oriented Modeling](https://link-springer-com.miman.bib.bth.se/book/10.1007/978-3-319-12742-2)

This is alternative 2 for the books.

1. [Core Java for the Impatient, 3rd Edition](https://learning.oreilly.com/library/view/core-java-for/9780138051846/?ar=)

1. [Object-Oriented Thought Process, 5th Edition](https://learning.oreilly.com/library/view/the-object-oriented-thought/9780135182130/)

Feel free to choose any mix of the books.



### Additional resources to the literature

If you enjoy video, the author of "Core Java" has a video serie on "Core Java 11 for the Impatient".

1. [Core Java 11 for the Impatient (video serie)](https://learning.oreilly.com/videos/core-java-11/9780135235331/)



Resources
--------------------------

These are useful resources for reference and learning, check them out and make an active choice to use them or not.

**Official Java documentation.**

* [The Java® Language Specification](https://docs.oracle.com/javase/specs/jls/se21/html/index.html)
* [Java® Platform, Standard Edition & Java Development Kit API Specification](https://docs.oracle.com/en/java/javase/21/docs/api/index.html)
* [The Java™ Tutorials](https://docs.oracle.com/javase/tutorial/)

Remember that the reference documentation is the core of it all. Learn to use it and you can answer any question.

**Tutorials** providing an insight into the basic constructs of the Java language.

* [JavaPoint Java Tutorial](https://www.javatpoint.com/java-tutorial)
* [Tutorialspoint Java Tutorial](https://www.tutorialspoint.com/java/index.htm)
* [W3Schools Java Tutorial](https://www.w3schools.com/java/default.asp)
* [Learn Java through dev.java](https://dev.java/learn/)

Tutorials are great resourses when you are learning new stuff.

Do not forget to **google "java" followed by your question**, that us usually the fastest way to get to one of the resources above.


<!--
Assignments and deadlines
--------------------------

* [A01](./assignment/A01.md) contains a serie of labs which should be done and submitted together with a lab report.
    * Start work course week 1.
    * Submit code, lab report & oral (or video) presentation (show code working) course week 6, 7, 8.
    * Dates for re-examination.
        * try 2 (re-exam) 2024-12-01 (oral/video presentation the week after).
        * try 3 (rest-exam) 2025-02-02 (oral/video presentation the week after).

* [A02](./assignment/A02.md) is the written examination at the end of the course.
    * Participate in quizz to earch bonus points to the written exam.
    * Do optional work in A01 to earn even more bonus points.
    * Dates for re-examination.
        * try 2 (re-exam) 2024-12-01.
        * try 3 (rest-exam) 2025-02-02.
-->



Week 01 (w46): Prepare and get going with Java
--------------------------

<!--
TODO

* Spela in video till grundläggande artiklar för att förenkla de första stegen
-->

We start up the course, install the lab environment and we compile and executes an object-oriented program in Java.

We also get an opportunity to practice coding trivial Java constructs in the first lab.


### Teacher activities

* Tuesday is lecture & tutoring, aiming at presenting the details needed to get going with the course and its environment.
* Wednesday is tutoring with teacher assistants.
* Friday is tutoring.

Participate on Tuesdays to get all the details needed for the week. Use the tutoring sessions to get help with the environment and start practicing the first lab.


### Lectures with slides

<!--
1. [Course introduction](https://mikael-roos.gitlab.io/java/lecture/course-intro/slide_24lp2.html)
-->
1. [Introduction to Java](../../public/lecture/java-intro/README.md)
1. [Programming with Java](../../public/lecture/programming-with-java/README.md)


### Read

**Read the literature,** _either these_:

1. Core Java for the Impatient, 3rd Edition
    * Chapter 1. Fundamental Programming Structures

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 1. Introduction To Object-Oriented Concepts 

_or these_: 

1. Java™ How To Program (Early Objects), Tenth Edition
    * Chapter 2. Introduction to Java Applications; Input/Output and Operators
    * Chapter 4. Control Statements: Part 1; Assignment, ++ and -- Operators
    * Chapter 5. Control Statements: Part 2; Logical Operators

1. UML @ Classroom An Introduction to Object-Oriented Modeling
    * Chapter 1. Introduction

**Read (and work) through the following tutorials:**

1. The Java Tutorials
    * [Lesson: Language Basics](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/index.html)


### Work

* Work through the exercise:
    * [Install the lab environment](../../exercise/lab_environment/README_no-git.md).
    * [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).

* Work through the lab:
    * [Lab 01: Data types, expressions, statements and built in functions](../../lab/lab_01/README.md)



Week 02 (w47): Classes and objects
--------------------------

<!--
TODO

* setters, getters
* (felhantering)
* lägg till class diagram, use case diagram till task?
* Lär ut pseudo kod och/eller flödesdiagram?

-->

Learn about classes and objects and their properties, learn how to create them and how to use them. Work with object oriented techniques and structure your code into classes with method and properties. Practice coding more Java.


### Teacher activities

* Tuesday is lecture & tutoring, aiming at presenting the details needed to get going with the course and its environment.
* Wednesday is tutoring with teacher assistants.
* Friday is live coding and tutoring.

Participate on Tuesdays to get all the details needed for the week. Use the tutoring sessions to get help. Use the live coding to enhance your skills.


### Lectures with slides, code samples and reading instructions

1. [Object-Oriented Programming Concepts: Class and object](../../public/lecture/oo-class-object/README.md)

1. [Programming in Java: Class and object](../../public/lecture/oo-class-object-java/README.md)


### Read

**Read the literature,** _either these_:

1. Core Java for the Impatient, 3rd Edition
    * Chapter 2. Object-Oriented Programming

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 2. How to Think in Terms of Objects

_or these_: 

1. Java™ How To Program (Early Objects), Tenth Edition
    * Chapter 3. Introduction to Classes, Objects, Methods and Strings

1. UML @ Classroom An Introduction to Object-Oriented Modeling
    * Chapter 2. A Short Tour of UML

**Read (and work) through the following tutorials:**

1. The Java Tutorials
    * [Lesson: Classes and Objects](https://docs.oracle.com/javase/tutorial/java/javaOO/index.html)


### Work

* Work through the exercise:
    * [A dice class in Java](../../exercise/dice-class/README.md)
    * [Create a menu driven application](../../exercise/menu/README.md)

* Quickly review the exercise:
    * [UML class diagrams and tools](../../exercise/uml_class_diagram/README.md)

* OPTIONAL exercises to review:
    * [A dice class in Python](../../exercise/python-class/README.md)

* Work through the lab:
    * [Lab 02: Conditions and iterations](../../lab/lab_02/README.md)

* [A01](./assignment/A01.md) is presented and you can start working with:
    * [Task 01: Get going with Java classes](./assignment/T01.md).



Week 03 (w48): Inheritance and composition
--------------------------

<!--
TODO

* 
-->

Code more Java and split your code into directories and packages. Work with object oriented techniques and structure your code using inheritance and composition.



### Teacher activities

* Tuesday is lecture & tutoring, aiming at presenting the details needed to get going with the course and its environment.
* Wednesday is tutoring with teacher assistants.
* Friday is live coding and tutoring.



### Lectures with slides, code samples and reading instructions

1. [Programming in Java: Packages, inheritance and composition](../../public/lecture/oo-inheritance-composition-java/README.md)



### Read

**Read the literature,** _either these_:

1. Core Java for the Impatient, 3rd Edition
    * Chapter 4. Inheritance and Reflection (focus on inheritance)

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 3. More Object-Oriented Concepts (read for an overview)

_or these_: 

1. Java™ How To Program (Early Objects), Tenth Edition
    * Chapter 9. Object-Oriented Programming: Inheritance
    * (Chapter 14. Strings, Characters and Regular Expressions) (use as reference)

1. UML @ Classroom An Introduction to Object-Oriented Modeling
    * Chapter 4. The Class Diagram (read for an overview)

**Read (and work) through the following tutorials:**

1. The Java Tutorials
    * [Lesson: Inheritance](https://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html) (approx 10 pages)



### Work

<!--
TODO

* [Inheritance and composition](../../exercise/inheritance-composition/README.md) needs to be done, just started... replaced with live coding, could/should also be in a smaller exercise.
-->

* Work through the exercise:
    * [Split your Java project into files and packages](../../exercise/package/README_v2.md)

* Work through the lab:
    * [Lab 03: Strings, characters and arrays](../../lab/lab_03/README.md) <!-- Make it a bit easier -->

* [A01](./assignment/A01.md) is ongoing, continue working with task 01.

<!--
* [A01](./assignment/A01.md) is ongoing, continue working with the next task:
    * [Task 02: Get going with inheritance and composition](./assignment/T02.md).
    * Person, supermen, staff, salary (needs collections)
    * Hangman?
-->



Week 04 (w49): Collection framework
--------------------------

<!--
TODO

* Programming in Java: Arrays and Collections (gather the example from the slide programs into an exercise)

-->

Java has a collection library with various data structures that can be use to store and work with collections of items. The collection framework is implemented using inheritance with abstract classes and implementation of interfaces.

The tool Gradle is a build tool that helps to structure and build the application and eases when using external modules.



### Teacher activities

* Tuesday is lecture & tutoring, aiming at presenting the details needed to get going with the course and its environment.
* Wednesday is tutoring with teacher assistants.
* Friday is live coding and tutoring.



### Lectures with slides, code samples and reading instructions

Lectures with slides, code samples and reading instructions:

1. [The Gradle build and automation tool](../../public/lecture/gradle/README.md)
1. [Java Collection Framework](../../public/lecture/collections-with-java/README.md)



### Read

**Read the literature,** _either these_:

1. Core Java for the Impatient, 3rd Edition
    * Chapter 6. Generic Programming (only 6.1, 6.2)
    * Chapter 7. Collections (overview)

1. Object-Oriented Thought Process, 5th Edition
    * No reading this week

_or these_: 

1. Java™ How To Program (Early Objects), Tenth Edition
    * Chapter 7. Arrays and ArrayLists
    * Chapter 16. Generic Collections 
        * 16.1 Introduction
        * 16.2 Collections overview
        * 16.6 Lists
        * 16.7 Collections methods
        * 16.10 Sets
        * 16.11 Maps

1. UML @ Classroom An Introduction to Object-Oriented Modeling
    * No reading this week

**Read (and work) through the following tutorials:**

1. Read this quick introduction to [What is Gradle?](https://docs.gradle.org/current/userguide/userguide.html)

1. The Java Tutorials
    * [Lesson: Introduction to Collections](https://docs.oracle.com/javase/tutorial/collections/intro/index.html) (roughly review the different types of collections)

1. [How to Write Doc Comments for the Javadoc Tool](https://www.oracle.com/se/technical-resources/articles/java/javadoc-tool.html) to learn the basics of how to write javadoc documentation.



### Work

* Work through the exercise:
    * [Build your Java project with Gradle](../../exercise/gradle/README.md)
    
* Work through the lab:
    * [Lab 04: Numbers, arrays and built-in methods](../../lab/lab_04/README.md)

* [A01](./assignment/A01.md) is ongoing, continue working with the next task:
    * [Task 02: Classes, Objects, Inheritance and Composition](./assignment/T02.md).



Week 05 (w50): Implement interface and abstract class
--------------------------

<!--
TODO

* Exceptions
    * Exceptions som övning, kanske från ctrl-d i menu?
    * Krav i lab att ctrl-d skall fungera?

-->

Work with the object-oriented concept of implementing interface and abstract classes and how it relates to inheritance and composition, including examples on what polymorfism is.



### Teacher activities

* Tuesday is lecture & tutoring, aiming at presenting the details needed to get going with the course and its environment.
* Wednesday is tutoring with teacher assistants.
* Friday is live coding and tutoring.



### Lectures with slides, code samples and reading instructions

Lectures with slides, code samples and reading instructions:

* [Programming with Java: Interface and abstract classes](../../public/lecture/interface-implements-abstract/README.md)



### Read

**Read the literature,** _either these_:

1. Core Java for the Impatient, 3rd Edition
    * Chapter 3. Interfaces and Lambda Expressions
        * 3.1 Interfaces
        * 3.2 Static, Default, and Private Methods
        * 3.3 Examples of Interfaces

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 2. How to Think in Terms of Objects

_or these_: 

1. Java™ How To Program (Early Objects), Tenth Edition
    * Chapter 10. Object-Oriented Programming: Polymorphism and Interfaces
        * 10.1 Introduction
        * 10.2 Polymorphism Examples
        * 10.3 Demonstrating Polymorphic Behavior
        * 10.4 Abstract Classes and Methods
        * 10.5 Case Study: Payroll System Using Polymorphism

1. UML @ Classroom An Introduction to Object-Oriented Modeling
    * No reading this week

**Read (and work) through the following tutorials:**

1. The Java Tutorials
    * [Lesson: Interfaces](https://docs.oracle.com/javase/tutorial/java/IandI/createinterface.html) (approx 7 pages)
    * [Lesson: Abstract Methods and Classes](https://docs.oracle.com/javase/tutorial/java/IandI/abstract.html) (1 page)



### Work

* Work through the exercise:
    * [Java programming: Implement interface and abstract class](../../exercise/interface-abstract/README.md)

* [A01](./assignment/A01.md) is ongoing, continue working with the next task:
    * [Task 03: Interface, implements and file management](./assignment/T03.md).



Week 06 (w51): Work with files
--------------------------

<!--
TODO

* Exercise hur man sparar filer med tidsstämpel i en katalog och sedan väljer ut en av dem för att läsa in, även radera, kopiera, byta namn.

-->

How to work with files and streams in Java and how to use an external module for saving and reading JSON data to files.



### Teacher activities

* Tuesday is lecture & tutoring, aiming at presenting the details needed to get going with the course and its environment.
* Wednesday is tutoring with teacher assistants.
* Friday is live coding and tutoring.



### Lectures with slides, code samples and reading instructions

Lectures with slides, code samples and reading instructions:

* [Programming with Java - Working with files and streams](../../public/lecture/file-stream-management/README.md)



### Read

**Read the literature,** _either these_:

1. Core Java for the Impatient, 3rd Edition
    * Chapter 9. Processing Input and Output
        * 9.1 Input/Output Streams, Readers, and Writers
        * 9.2 Paths, Files, and Directories

1. Object-Oriented Thought Process, 5th Edition
    * No reading this week

_or these_: 

1. Java™ How To Program (Early Objects), Tenth Edition
    * Chapter 15. Files, Streams and Object Serialization
        * 15.1 Introduction
        * 15.2 Files and Streams
        * 15.3 Using NIO Classes and Interfaces to Get File and Directory Information
        * 15.4 Sequential-Access Text Files
        * 15.5 Object Serialization

1. UML @ Classroom An Introduction to Object-Oriented Modeling
    * No reading this week

**Read (and work) through the following tutorials:**

1. The Java Tutorials
    * 1. [The Java™ Tutorials Lesson: Basic I/O](https://docs.oracle.com/javase/tutorial/essential/io/)
  (review its overall content and use as a reference)



### Work

* Work through the exercise:
    * [Java programming: File and stream management](../../exercise/file-management/README.md)
    * [Java programming: Save and read a serialized object to file](../../exercise/file-management/README_serialize.md)
    * [Java programming: Create, print and save a JSON data structure from an object](../../exercise/file-management/README_json.md)

* Continue working with the labs and finalize them.

* Continue working with [A01](./assignment/A01.md) and finalize the tasks.



Week 07 (w52): Work on your own
--------------------------

Christmas week. Work on your own with the exercises, labs and tasks in the assignment.



### Work

* Continue working with the labs and finalize them.
* Continue working with A01 and finalize the tasks.



Week 08 (w01): Work on your own
--------------------------

New years week. Work on your own with the exercises, labs and tasks in the assignment.



### Work

* Continue working with the labs and finalize them.
* Continue working with A01 and finalize the tasks.



Week 09 (w02): Repetition
--------------------------

We walk through the course from start to its end and recapulate what we have talked about and we discuss what is of more or lesser importance.


### Teacher activities

* Tuesday is lecture & tutoring, aiming at walking through the important parts of the course material.
* Wednesday is tutoring with teacher assistants.
* Friday is tutoring.


### Lectures with slides

* Repetition


### Work

* Be done and submit A01.



Week 10 (w03): Examination
--------------------------

Practise for the written examination.


### Teacher activities

* Tuesday is lecture & tutoring, aiming at walking through the important parts of the course material.


### Lectures with slides

* Repetition


### Work

* Prepare for and execute the written exam.
