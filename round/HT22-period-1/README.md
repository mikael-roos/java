![What now?](exercise/hello_world/img/hello_world.png)

Weekly log - 2022 period 1
==========================

This is an overview of the course setup, resources and what happens week by week. The content will be updated all during the course, as each week goes by.

[[_TOC_]]

<!--
IMPROVEMENTS

* Förtydliga labbinstruktionerna så det blir tydligare hur tärningsspelet skall fungera. Främst lab 2 och lab 3.

* Skapa mer lärmaterial så att "programmeringssvaga" får en lättare start in i Java (när de kommer från Python).
    * Övnings- och videoserie som visar handfast hur det kan se ut.

* Kika över Lab 2 som blev lite svår i andra delen, kanske delvis svårt att se framför sig hur koden skall fungera, skapa exempelprogram som visar hur det kan se ut?

* Övning eller labb med Swing GUI?
    * Eventdriven programmeringsmodell
    * Visa bild, text, knappar, rita objekt, on click, keybinding, text input
    * Koda masken?
    * Adventure med GUI?
    * Design mönster MVC för GUI som exempel?
    * Puzzle game
    * Tic tac toe

* Mer exempel där man använder externa lib?
    * url mot quiz-server (med swing)

* Jobba igenom föreläsningarna.
    * Känns som design pattern hamnade på rätt nivå i rätt läge (analys/deisgn i projektet med creator patterns).

* Finns det något som kan tas bort?
-->


Prepare yourself
--------------------------

These are things you can do to prepare yourself and get going with the course.

1. [Install the lab environment](../../exercise/lab_environment/README.md).

1. [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).

1. Get the literature and start reading the first chapter in each book.

    1. Core Java SE 9 for the Impatient, 2nd Edition
        * Chapter 1. Fundamental Programming Structures
    1. Object-Oriented Thought Process, 5th Edition
        * Chapter 1. Introduction To Object-Oriented Concepts



Assignments and deadlines
--------------------------

* [A01](./assignment/A01.md) contains a serie of labs which should be done and  submitted together with a lab report.
    * Start work 31/8.
    * Submit code, lab report & recorded presentation 14/10.

* [A02](./assignment/A02.md) is a two part project, part 1 should be done before starting on part 2.
    * Start work part 1: 5/10.
    * Start work part 2: 19/10.
    * Submit code, lab report & recorded presentation 4/11.
        * You can opt to do a oral presentation 2/11 instead of recording.

* Dates for re-examination of A01 & A02.
    * try 1 (re-exam) 2022-11-27.
    * try 2 (rest-exam) 2023-01-27.



Literature
--------------------------

The following literature will be used.

1. Core Java SE 9 for the Impatient, 2nd Edition

1. Object-Oriented Thought Process, 5th Edition



Resources
--------------------------

These are useful resources for reference and learning, check them out and make an actice choice to use them or not.

Official Java documentation.

* [The Java® Language Specification](https://docs.oracle.com/javase/specs/jls/se18/html/index.html)
* [Java® Platform, Standard Edition & Java Development Kit Version 18 API Specification](https://docs.oracle.com/en/java/javase/18/docs/api/index.html)
* [The Java™ Tutorials](https://docs.oracle.com/javase/tutorial/)

Tutorials providing an insight into the basic constructs of the Java language.

* [JavaPoint Java Tutorial](https://www.javatpoint.com/java-tutorial)
* [Tutorialspoint Java Tutorial](https://www.tutorialspoint.com/java/index.htm)
* [W3Schools Java Tutorial](https://www.w3schools.com/java/default.asp)
* [Learn Java through dev.java](https://dev.java/learn/)



Week 01 (w35): Prepare and get going with Java
--------------------------

We start up the course, install the lab environment and writes our first object-oriented program in Java.

Teacher activities:

* Wednesday is lecture & tutoring on campus, aiming at presenting the details needed to start with A01.

Lectures with slides:

1. [Course introduction](https://mikael-roos.gitlab.io/java/lecture/course-intro/slide_22lp1.html)
1. [Introduction to Java](https://mikael-roos.gitlab.io/java/lecture/java-intro/slide.html)

Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 1. Fundamental Programming Structures
1. Object-Oriented Thought Process, 5th Edition
    * Chapter 1. Introduction To Object-Oriented Concepts

Work:

* Work through the exercise:
    * [Install the lab environment](../../exercise/lab_environment/README.md).
    * [Hello world - Build and execute your first program in Java](../../exercise/hello_world/README.md).
    * [Create a menu driven application](../../exercise/menu/README.md)
* Try out and inspect the sample programs:
    * [time_date](../../sample/time_date)
    * [array](../../sample/array)
    * [argv](../../sample/argv)
    * [dice_single_file](../../sample/dice_single_file)
    * [menu](../../sample/menu)
* [A01](./assignment/A01.md) is presented and you can start working with Lab 1.



Week 02 (w36): Classes, Objects, Inheritance and Composition
--------------------------

Code more Java and split your code into files and packages. Work with object oriented technices and structure your code into classes, method, proerties and practice inheritance and composition.

Teacher activities:

* Wednesday is lecture 13-15 & tutoring 15-17 on campus.
* Friday is tutoring on campus.
    * Use it to get help and finalize lab 1 and get started with lab 2.


Lectures with slides:

1. [Object-Oriented Thought Process](https://mikael-roos.gitlab.io/java/lecture/oo-programming-concepts/slide.html)
1. [Object-Oriented Programmming with Java](https://mikael-roos.gitlab.io/java/lecture/oo-programming-with-java/slide.html)


Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 2. Object-Oriented Programming
    * Chapter 4. Inheritance and Reflection

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 1. Introduction To Object-Oriented Concepts (same as week 1)

1. Read the article "[How to Write a Git Commit Message](https://cbea.ms/git-commit/)" to get seven golden rules that helps you in creating better commit messages.


Work:

* Work and finalize lab 1.
* Work with exercises
    * [Split your Java project into files and packages](../../exercise/package/README.md)
* Try out and inspect the sample programs:
    * [python_class](../../sample/python_class)
    * [dice](../../sample/dice)
    * [dice_graphic](../../sample/dice_graphic)
    * [dice_hand](../../sample/dice_hand)
* Start working with [A01: Lab 2](./assignment/A01.md).



Week 03 (w37): Interface and implements versus extends
--------------------------

<!--
TODO

Plan inför onsdagen

* Övning med debugger
* Jobba igenom övningen med gradle
* Skapa ett nytt Gradle och flytta över dice hand till det
* Föreläsning om interface
    1. [Design and program with interfaces](https://mikael-roos.gitlab.io/java/lecture/interface-implements/slide.html)
* Bygg om dicehand med interface
    * Rita upp dicehand exemple med UML klassdiagram
    * Gör samma men med interface
    * Visa koden
* Släpp lab 3

-->

Introduce the tool Gradle to use to structure, build and test your Java source code.

Work with the Object-oriented concept of interfaces and implements versus the thoughts of inheritance and multiple inheritance.

Teacher activities:

* Wednesday is a coding session in zoom (to be recorded) were we practice Gradle and how to work with interfaces by reviewing some of the exercises and code samples.

Lectures with slides:

* [Design and program with interfaces](https://mikael-roos.gitlab.io/java/lecture/interface-implements/slide.html)

Read:

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 3. Interfaces and Lambda Expressions
1. Object-Oriented Thought Process, 5th Edition
    * Chapter 2. How to Think in Terms of Objects

1. [What is Gradle?](https://docs.gradle.org/current/userguide/what_is_gradle.html)
1. [How to Write Doc Comments for the Javadoc Tool](https://www.oracle.com/se/technical-resources/articles/java/javadoc-tool.html)


Work:

* Work and finalize lab 2.
* Work with exercises
    * [Build your Java project with Gradle](../../exercise/gradle/README.md)
* Try out and inspect the sample programs:
    * [dice_hand with Gradle](../../sample/gradle)
    * [dice_hand_interface](../../sample/dice_hand_interface)
* Start working with [A01: Lab 3](./assignment/A01.md).



Week 04 (w38): Unit testing with JUnit
--------------------------

<!--

Plan inför onsdagen.

1. Föreläsning om “Software Testing”
1. Föreläsning om “Software Unit Testing"
1. Föreläsning om TDD?
1. Jobba igenom övningen med JUnit

-->

We learn the concept of unit testing and we practice it using [JUnit](https://junit.org/junit5/). We write a testbed with test cases to perform unittesting of the code we have written earlier in the course.

Teacher activities:

* Wednesday is lecture & tutoring on campus, introducing the general concept of Unit Testing and in particular JUnit with Java.
* Friday is tutoring on campus.

Lectures with slides:

* [Unit testing](https://mikael-roos.gitlab.io/java/lecture/unit-testing/slide.html)

<!-- add the lecture on TDD -->


Read:

1. Get aquainted with the JUnit project by reading its user documentation "[JUnit 5 User Guide](https://junit.org/junit5/docs/current/user-guide/)".
    * 1. Overview
    * 2. Writing Tests
    * 4. Running Tests
1. Get aquainted with test integration in Gradle.
    * [Testing in Java & JVM projects](https://docs.gradle.org/current/userguide/java_testing.html)
        * There is a section on [Using Junit 5](https://docs.gradle.org/current/userguide/java_testing.html#using_junit5).
1. There is not much to read on the tool "[JaCoCo Java Code Coverage Library](https://www.jacoco.org/jacoco/)" but I would still like to mention it here since we will use it.

<!--
* System testing, unit testing, TDD?
-->

Work:

* Work and finalize lab 3.
* Work with exercises
    * [Unit testing with JUnit](../../exercise/junit/README.md)
* Try out and inspect the sample programs:
    * [Gradle build with JUnit examples](../../sample/junit5)
* Start working with [A01: Lab 4](./assignment/A01.md).



Week 05 (w39): Static code analysis and clean code
--------------------------

<!--
1. Jobba igenom övningen med Sonarcube
1. Föreläsning om kodkvalitet
    * https://docs.sonarqube.org/latest/user-guide/metric-definitions/

-->

About static code analysis and software metrics and how we can use them in quality assurance and to improve our Java code. We will also talk a bit about the concept of good and clean code.

Teacher activities:

* Wednesday work with exercise on Sonarcube.
* There is time for tutoring and help session.

Lectures with slides:

* [Static code analysis and software metrics](https://mikael-roos.gitlab.io/java/lecture/static-code-analysis/slide.html)

Read:

* Roughly review a few concepts and terms of clean code through Wikipedia.
    * [Coding conventions](https://en.wikipedia.org/wiki/Coding_conventions)
    * [Code smell](https://en.wikipedia.org/wiki/Code_smell)
    * [Cohesion](https://en.wikipedia.org/wiki/Cohesion_(computer_science))
    * [Coupling](https://en.wikipedia.org/wiki/Coupling_(computer_programming))
    * [Code coverage](https://en.wikipedia.org/wiki/Code_coverage)
    * [Cyclomatic complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity)
    * [Technical debt](https://en.wikipedia.org/wiki/Technical_debt)
        * [Broken windows theory](https://en.wikipedia.org/wiki/Broken_windows_theory)

<!--
* Good and clean code
* The fiv/six C to code quality
-->

Work:

* Work and finalize lab 4.
* Work with exercises
    * [Static code analysis and software metrics with SonarCube](../../exercise/sonarcube/README.md)
* Start working with [A01: Lab 5](./assignment/A01.md).

<!--
* Add gradle plugin PMD & Checkstyle?
-->



Week 06 (w40): OO Analysis and Design
--------------------------

<!--
* Walk through the project
* Show template on how to write the report
* Lecture on OOAD
* Talk about the different project tasks and what their implications might be
-->

Start working on the A02 project (part 1).

Teacher activities:

* Wednesday is lecture & tutoring on campus, aiming at presenting the details needed to start with A02 part 1 which includes UML and Object-oriented Design (OOD).
* Friday is tutoring in Slack.

Lectures with slides:

* [OO analysis and design](https://mikael-roos.gitlab.io/java/lecture/oo-design/slide.html)

Read:

1. Object-Oriented Thought Process, 5th Edition
    * 6 Designing with Objects
    * 9 Building Objects and Object-Oriented Design

Work:

* Work and finalize lab 5.
* Begin to submit A01, deadline is next week.
* Work with exercises
    * [UML class diagrams and tools](../../exercise/uml_class_diagram/2022/)
* Start working with part 1 of [A02](./assignment/A02.md).

<!--
* Add example of a UML use case diagram
-->



Week 07 (w41): Work with project
--------------------------

<!--
* Project spec is complete.
* Outline for the report.
-->

Work with part 1 of the A02 project. The A02 part 2 is released and we see an outline of the report.

Teacher activities:

* On Wednesday is the A02 part 2 released, the requirements around the implementation of the project.
* Walkthrough of the assignment report template.
* Wednesday is then an online tutoring and help session for the project.

<!--

Lectures with slides:

* TBD

Read:

* TBD
* Design patterns in Java
-->

Work:

* Work with A02 (part 1 & 2).
* (Deadline to submit A01. Deadline moved to end of the course)

<!--
More? 

* Wrap all tasks (javadoc, test, verification, code coverage, metrics) in Gradle
* Exercise design pattern?
* Exercise on good and clean code, get metrics on your own code from the labs.
* (Exercise create a pipeline in GitLab and automate unit testing)
-->



Week 08 (w42): Design patterns
--------------------------

<!--

TODO

* What is design patterns
* Data structures in Java (and other cool things)

-->

Lecture on design patterns. See if you can use this in your project to enhance your code and make it more loosely coupled.

Teacher activities:

* Wednesday is lecture & tutoring on campus.
* Friday is tutoring on zoom.

Lectures with slides:

* [Design patterns](https://mikael-roos.gitlab.io/java/lecture/design-patterns/slide.html)

Read:

1. Object-Oriented Thought Process, 5th Edition
    * 15. Design Patterns
1. Wikipedia on "[Software design pattern](https://en.wikipedia.org/wiki/Software_design_pattern)" for a quick overview of some design patterns.
1. Website for "[Catalog of Patterns of Enterprise Application Architecture](https://www.martinfowler.com/eaaCatalog/)" where you can look up the definition of some more design patters.

Work:

* Work with A02.
* Check code samples
    * [Design patterns](../../sample/design-patterns)



Week 09 (w43): Work with project...
--------------------------

Work with the A02 project, next week is submission time.

Teacher activities:

* Wednesday is an online tutoring and help session for the project.

<!--
Lectures with slides:

* TBD

Read:

* TBD
-->

Work:

* Work with A02.



Week 10 (w44): Examination
--------------------------

The last week of the course where you finalize and submit and present your project.

Teacher activities:

* Wednesday is an opportunity to present A02 orally to the teacher at campus, otherwise you may record a video presentation to submit.
* Friday is tutoring on campus, last chance to get help.

<!--
Lectures with slides:

* TBD

Read:

* TBD
-->

Work:

* Submit A02 (part 1 & 2).



<!--

* Core Java SE 9 for the Impatient, 2nd Edition
    * 1 FUNDAMENTAL PROGRAMMING STRUCTURES (w01)
    * 2 OBJECT-ORIENTED PROGRAMMING (w2)
    * 3 INTERFACES AND LAMBDA EXPRESSIONS (w03 interface week)
    * 4 INHERITANCE AND REFLECTION (w02)
    * 5 EXCEPTIONS, ASSERTIONS, AND LOGGING (?)
    * 6 GENERIC PROGRAMMING (?)
    * 7 COLLECTIONS (project?)
    * 8 STREAMS (optional?)
    * 9 PROCESSING INPUT AND OUTPUT (?)
    * 10 CONCURRENT PROGRAMMING (no)
    * 11 ANNOTATIONS (no)
    * 12 THE DATE AND TIME API (exercise + lab to make calendar with ascii art?)
    * 13 INTERNATIONALIZATION (swedish calendar?)
    * 14 COMPILING AND SCRIPTING (no)
    * 15 THE JAVA PLATFORM MODULE SYSTEM (no)

* Object-Oriented Thought Process, 5th Edition
    * 1 Introduction to Object-Oriented Concepts (w02)
    * 2 How to Think in Terms of Objects (w02interface week?)
    * 3 More Object-Oriented Concepts (w3?)
    * 4 The Anatomy of a Class (w4?)
    * 5 Class Design Guidelines (clean code week)
    * 6 Designing with Objects (uml week, a02)
    * 7 Mastering Inheritance and Composition (uml week +1 extra?)
    * 8 Frameworks and Reuse: Designing with Interfaces and Abstract Classes (uml week +1 extra?)
    * 9 Building Objects and Object-Oriented Design (uml week +1 extra?)
    * 10 Design Patterns ()
    * 11 Avoiding Dependencies and Highly Coupled Classes (clean code, project weeks)
    * 12 The SOLID Principles of Object-Oriented Design (clean code, project weeks)


Extra lectures to put in

* Clean code in Java
    * SOLID
    * Dependency injection


Reuse
* Hur tänka kring objektorientering och principer om snygg kod?
    * https://dbwebb.se/kurser/mvc-v2/forelasning/hur-tanka-kring-objectorientering-och-snygg-kod
* Clean code & metrics 
    * Software documentation (inspelad)
    * What about good and clean code? (inspelad)
    * Software development philosophies (inspelad)
    * Static code analysis (inspelad)
* Design patterns MVC (skapa och återanvänd?)

-->