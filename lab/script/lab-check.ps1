# Execute the script locally
# powershell -ExecutionPolicy Bypass -File 'lab-check.ps1'

# Execute the script locally, download script from url
# Invoke-Expression (Invoke-WebRequest -Uri "https://gitlab.com/mikael-roos/java/-/raw/main/lab/script/lab-check.ps1" -UseBasicParsing).Content

# Run the script by downloading the script from a URL
# curl -s "https://gitlab.com/mikael-roos/java/-/raw/main/lab/script/lab-check.bash" | bash

# Iterate over each lab directory
foreach ($dir in Get-ChildItem -Directory -Path "lab/lab_*") {
    Write-Host "Processing directory: $dir"

    # Change to the directory
    Set-Location $dir.FullName

    # Check if all required files are present
    $requiredFiles = @("Answer.java", "Lab.java", "Main.java")
    $missingFiles = @()

    foreach ($file in $requiredFiles) {
        if (!(Test-Path $file)) {
            $missingFiles += $file
        }
    }

    if ($missingFiles.Count -gt 0) {
        Write-Host "Missing required files in directory $($dir.Name): $($missingFiles -join ', '). Skipping..."
        Set-Location .. # Return to the parent directory
        continue
    }

    # Compile the Java files
    $Env:JAVA_TOOL_OPTIONS="-Dfile.encoding=UTF8"
    $compileResult = javac Answer.java, Lab.java, Main.java 2>&1
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Compilation failed for Answer.java, Lab.java, Main.java in $($dir.Name). Skipping..."
        Write-Host "Error: $compileResult"
        Set-Location .. # Return to the parent directory
        continue
    }

    # Run the program and display the last 8 lines of output
    Write-Host "Output from $($dir.Name):"
    java Main 2>&1 | Select-Object -Last 8

    # Return to the parent directory
    Set-Location ..
}
