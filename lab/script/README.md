---
revision:
    "2025-01-06": "(A, mos) First release."
---
Verify and execute your labs
===========================

This is documentation and how-to for scripts that aids in verifying the status of the labs.

[[_TOC_]]

<!--
TODO

* 
-->



The checker script
---------------------------

The lab checker script will verify that you have saved the labs in the recommended structure and it will then execute each lab and show the status of it.



### Visually check your directory structure

The first thing you should do is to go to the directory where you saved the labs and visually check that you have the correct directory structure.

The proposed directory structure is like this.

```
lab/
    lab_01/
    lab_02/
    lab_03/
    lab_04/
```

You can verify your directory structure, visually, like this.

```
# Go to your directory where lab/ is

# Show the content of the lab/ directory.
ls lab
```



### Run the checker script

Now you can run the lab checker script, depending on your platform.



<details>
<summary>Windows PowerShell: Run lab-check.ps1.</summary>

The source code for the script can be [reviewed online](./lab-check.ps1).

Download and execute the script like this.

```powershell
# Execute the script locally, download script from url
Invoke-Expression (Invoke-WebRequest -Uri "https://gitlab.com/mikael-roos/java/-/raw/main/lab/script/lab-check.ps1" -UseBasicParsing).Content
```

The output from the execution can something look like this.

```
Processing directory: C:\Users\mos\course\java\lab\lab_01
Output from lab_01:
 --------------------------------------------------------------------
| Total: 42, Passed ✅: 0, Failed ❌: 42
|
| Points needed to PASS=15, PASS WITH HONOUR=19, TOTAL=21
|  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  😁  ⦾  ⦾  ⦾  😍  ⦾  🙌
|
| You have 0 points. Try to earn 1 point to get started... 😏
 --------------------------------------------------------------------
```

</details>



<details>
<summary>Linux, Mac, Windows with bash terminal: Run lab-check.bash.</summary>

The source code for the script can be [reviewed online](./lab-check.bash).

Download and execute the script like this by either using curl or wget.

Opt to use curl.

```bash
# Using curl
curl -s "https://gitlab.com/mikael-roos/java/-/raw/main/lab/script/lab-check.bash" | bash
```

Or opt to use wget.

```bash
# Using wget
wget -qO- "https://gitlab.com/mikael-roos/java/-/raw/main/lab/script/lab-check.bash" | bash
```

The output from the execution can something look like this.

```
Processing directory: lab/lab_01/                                         
Output from lab/lab_01/:                                                  
 --------------------------------------------------------------------     
| Total: 42, Passed ✅: 0, Failed ❌: 42                                  
|                                                                         
| Points needed to PASS=15, PASS WITH HONOUR=19, TOTAL=21                 
|  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  ⦾  😁  ⦾  ⦾  ⦾  😍  ⦾  🙌       
|                                                                         
| You have 0 points. Try to earn 1 point to get started... 😏             
 --------------------------------------------------------------------     
```

</details>
