#!/bin/bash

# Run the script by downloading the script from a URL
# curl -s "https://gitlab.com/mikael-roos/java/-/raw/main/lab/script/lab-check.bash" | bash

# Iterate over each lab directory
for dir in lab/lab_*/; do
    echo "Processing directory: $dir"
    cd "$dir" || continue

    # Check if all required files are present
    if [[ ! -f "Answer.java" || ! -f "Lab.java" || ! -f "Main.java" ]]; then
        echo "Missing required files in $dir. Skipping..."
        cd - > /dev/null
        continue
    fi

    # Compile the Java files
    if ! javac {Answer,Lab,Main}.java; then
        echo "Compilation failed for {Answer,Lab,Main}.java in $dir. Skipping..."
        cd - > /dev/null
        continue
    fi

    # Run the program and display the last 8 lines of output
    echo "Output from $dir:"
    java Main | tail -n 8

    # Return to the parent directory
    cd - > /dev/null
done
