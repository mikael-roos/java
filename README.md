![What now?](exercise/hello_world/img/hello_world.png)

# Learn programming with Java

[![pipeline status](https://gitlab.com/mikael-roos/java/badges/main/pipeline.svg)](https://gitlab.com/mikael-roos/java/-/commits/main)

Course material for various courses with the focus of learning programming in Java.
