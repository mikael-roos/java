import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Example getting values from the commandline, using try and creating another
 * static method and using List.
 * https://docs.oracle.com/javase/tutorial/essential/environment/cmdLineArgs.html
 *
 * Compile and run as:
 *  javac Main.java && java Main 1 2 3
 */
public class Main {
    
    /**
     * Read input from the commandline, convert to integer and sum them.
     * 
     * @param args
     */
    public static void main(String[] args) {
        final List<Integer> values = new ArrayList<Integer>();
        int value;

        for (int i = 0; i < args.length; i++) {
            try {
                value = Integer.parseInt(args[i]);
                values.add(value);
            }   
            catch (Exception ex) {
                System.out.println(
                    "ERROR. Could not convert the value '"
                    + args[i]
                    + "' to an integer. "
                    + ex
                );
                System.exit(1);
            } 
        }

        int sum = sum(values);
        System.out.println("The sum is: " + sum);
        System.out.println("The values are: " + Arrays.toString(values.toArray()));
    }

    public static int sum(List<Integer> values) {
        int sum = 0;

        for (Integer value : values) {
            sum += value;
        }

        return sum;
    }
}
