public class Main {
    
    public static void main(String[] args) {
        int[] data = {0};
        data[0] = 0x1F916;
        System.out.println( new String(data, 0, 1) );

        String robot = new String(new int[]{0x1F916}, 0, 1);
        System.out.println("This is a robot: " + robot);

        robot = "This is yet another robot looking like this 🤖";
        System.out.println(robot);
    }
}
