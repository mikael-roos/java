/**
 * Example with creating and using an ArrayList with integers.
 * https://docs.oracle.com/en/java/javase/20/docs/api/java.base/java/util/ArrayList.html
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */

import java.util.ArrayList;
import java.util.List;

public class Main {
    
    public static void main(String[] args) {
        List<String> collection = new ArrayList<>();
        
        System.out.println("Adding A");
        collection.add("A");
        collection.add("B");
        collection.add("C");
        collection.add(3, "D");
        System.out.println(collection.size());
        System.out.println(collection.get(0));

        collection.add("A");
        collection.add("A");

        collection.set(4, "F");
        collection.set(collection.size() - 1, "G");

        collection.remove(0);

        for (String s: collection) {
            System.out.print(s);
        }
        System.out.println();

        for (int i = 0; i < collection.size(); i++) {
            System.out.print(collection.get(i));
        }
        System.out.println();

    }
}
