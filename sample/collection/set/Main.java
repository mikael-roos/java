/**
 * Example with creating and using an ArrayList with integers.
 * https://docs.oracle.com/en/java/javase/20/docs/api/java.base/java/util/ArrayList.html
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */

import java.util.HashSet;
import java.util.Set;

public class Main {
    
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        
        // Adding elements to the Set
        set.add("Java");
        set.add("Python");
        set.add("JavaScript");
        System.out.println("After adding elements: " + set);

        // Trying to add a duplicate element
        boolean added = set.add("Java");
        System.out.println("Trying to add duplicate 'Java': " + added); // Output: false
        System.out.println("Set after attempting to add duplicate: " + set);

        // Removing an element
        boolean removed = set.remove("Python");
        System.out.println("Removed 'Python': " + removed); // Output: true
        System.out.println("Set after removal: " + set);

        // Removing a non-existent element
        removed = set.remove("Ruby");
        System.out.println("Trying to remove non-existent 'Ruby': " + removed); // Output: false
        System.out.println("Set after attempting to remove 'Ruby': " + set);

        // Checking if an element exists
        boolean contains = set.contains("JavaScript");
        System.out.println("Set contains 'JavaScript': " + contains); // Output: true

        // Clearing the Set
        set.clear();
        System.out.println("Set after clearing: " + set); // Output: []
    }
}
