/**
 * Example with creating and using an Deque.
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;


public class Main {
    
    public static void main(String[] args) {
        example1();
        example2();
    }

    private static void example1 () {
        Deque<String> collection = new LinkedList<>();

        collection.addFirst("A");
        collection.addFirst("B");
        collection.addLast("Z");
        collection.addLast("Y");

        collection.removeFirst();
        collection.removeLast();

        for (String s: collection) {
            System.out.print(s);
        }
        System.out.println();
    }

    private static void example2 () {
        Deque<String> deque = new ArrayDeque<>();

        // Adding elements
        deque.addFirst("Front");
        deque.addLast("Rear");
        System.out.println("Deque after adding: " + deque);

        // Inspecting elements
        System.out.println("First element: " + deque.peekFirst());
        System.out.println("Last element: " + deque.peekLast());

        // Removing elements
        deque.removeFirst();
        System.out.println("Deque after removing front: " + deque);
        deque.removeLast();
        System.out.println("Deque after removing rear: " + deque);
    }
}
