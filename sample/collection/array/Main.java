package collection.array;
/**
 * Example with creating and using an array with strings.
 * https://docs.oracle.com/javase/specs/jls/se18/html/jls-10.html
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */

import java.util.Arrays;

public class Main {
    
    public static void main(String[] args) {
        ArrayString();
        ArrayInteger();
    }

    private static void ArrayString () {
        String[] alphabet = {"A", "B", "C"};
        
        System.out.println("The alphabet starts with: " + alphabet[0]);

        System.out.println("The alphabet contains " + alphabet.length + " characters.");

        System.out.print("Alphabet: ");
        for (int i=0; i < alphabet.length; i++) {
            System.out.print(alphabet[i] + " ");
        }
        System.out.println();

        System.out.print("Alphabet: ");
        for (String element : alphabet) {
            System.out.print(element);
        }

        System.out.println("\nLets print it once more...");
        System.out.println(Arrays.toString(alphabet));
    }

    private static void ArrayInteger () {
        int[] arr = {1, 2, 3};

        // int[] arr1 = new int[5];
        // int[] arr2 = {1, 2, 3};
        
        // arr2.length             // 3
        // arr2[0]                 // 1
        // arr2[arr2.length() -1]   // 3
        
        System.out.println(arr.length);
        System.out.println(arr[0]);
        System.out.println(arr[arr.length -1]);

    }
}

