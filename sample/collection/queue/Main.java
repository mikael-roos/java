/**
 * Example with creating and using an Que.
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;


public class Main {
    
    public static void main(String[] args) {
        example1();
        example2();
    }

    private static void example1 () {
        Queue<String> collection = new LinkedList<>();

        collection.add("A");
        collection.add("B");
        collection.add("C");

        for (String s: collection) {
            System.out.print(s);
        }
        System.out.println();

        Iterator<String> iterator = collection.iterator();

        while (iterator.hasNext()) {
            String s = collection.poll();
            System.out.println("popped= " + s);
        }
    }

    private static void example2 () {
        Queue<String> queue = new LinkedList<>();

        // Adding elements
        queue.offer("First");
        queue.offer("Second");
        queue.offer("Third");

        // Inspecting the head
        System.out.println("Head of the queue: " + queue.peek()); // Output: First

        // Removing elements
        System.out.println("Removed: " + queue.poll()); // Output: First
        System.out.println("Head after removal: " + queue.peek()); // Output: Second
    }
}
