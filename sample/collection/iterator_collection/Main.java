/**
 * Iterate over a collection.
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Main {
    
    public static void main(String[] args) {
        ArrayList<String> collection = new ArrayList<>();

        collection.add("A");
        collection.add("B");
        collection.add("C");
        
        iterateUsingIteratorWhile(collection);
        iterateUsingIteratorFor(collection);
        iterateUsingForEach(collection);
        iterateUsingFor(collection);
        iterateUsingStream(collection);
        
        System.out.println();
    }

    public static void iterateUsingIteratorWhile(Collection<String> collection) {
        Iterator<String> iterator = collection.iterator();

        while (iterator.hasNext()) {
            System.out.println("value= " + iterator.next());
        }
    }

    public static void iterateUsingIteratorFor(Collection<String> collection) {
        Iterator<String> iterator = collection.iterator();

        for (; iterator.hasNext();) {
            System.out.println("value= " + iterator.next());
        }
    }

    public static void iterateUsingForEach(Collection<String> collection) {
        for (String s: collection) {
            System.out.println("value= " + s);
        }
    }

    public static void iterateUsingFor(ArrayList<String> collection) {
        for (int i = 0; i < collection.size(); i++) {
            System.out.println("value= " + collection.get(i));
        }
    }

    public static void iterateUsingStream(Collection<String> collection) {
        collection.stream()
            .filter(e -> e == "B")
            .map(e -> e += "BB")
            .forEach(e -> System.out.print(e));
    }
}

