/**
 * Example with creating and using an Map.
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class Main {
    
    public static void main(String[] args) {
        example1();
        example2();
    }

    private static void example1 () {
        Map<String, Integer> map = new HashMap<>();

        map.put("Magic", 42);
        map.put("Lucky", 7);
        map.put("Unlucky", 13);

        for (int value : map.values()) {
            System.out.println(value);
        }

        for (String key : map.keySet()) {
            System.out.println(key + ": " + map.get(key));
        }
    }

    private static void example2 () {
        // LinkedList as a Deque
        LinkedList<String> linkedListDeque = new LinkedList<>();
        linkedListDeque.addFirst("Front");
        linkedListDeque.addLast("Rear");
        System.out.println("LinkedList: " + linkedListDeque);

        // ArrayDeque as a Deque
        ArrayDeque<String> arrayDeque = new ArrayDeque<>();
        arrayDeque.addFirst("Front");
        arrayDeque.addLast("Rear");
        System.out.println("ArrayDeque: " + arrayDeque);
    }
}
