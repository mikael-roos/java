/*
 * Class to show off how unit tests works.
 */
package junit5;

public class Calculator {
    int add (int a, int b) {
        return a + b;
    } 

    int subtract (int a, int b) {
        return a - b;
    } 

    double division (double a, double b) {
        return a / b;
    } 

    double divisionWithException (double a, double b) {
        if (b == 0) {
            throw new java.lang.ArithmeticException("Division by zero not allowed.");
        }
        return a / b;
    } 
}
