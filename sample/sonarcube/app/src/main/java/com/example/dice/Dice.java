/**
 * Classic dice with values between 1 and 6.
 */
package com.example.dice;

public interface Dice {

    /**
     * This implies a six sided dice.
     */
    final int MIN_SIDES = 1;
    final int MAX_SIDES = 6;

   /**
     * Roll the dice and return/save its results.
     * @return integer
     */
    public int roll ();

    /**
     * Get the value of the last roll, returns 0 if the die is not yet rolled.
     * @return integer
     */
    public int value ();

    /**
     * May set the value of the last roll, nice for cheating.
     * @param int value the dice value to set 
     * @return void
     */
    default void set (int value) throws DiceException {
        if (value < MIN_SIDES || value > MAX_SIDES) {
            throw new DiceException("Setting die value " + value + " out of bound.");
        }
    }

    /**
     * Print a string representation of the object.
     * @return String
     */
    default String print () {
        return Integer.toString(value());
    }
}
