/**
 * Graphical six sided dice.
 */
package com.example.dice;

public interface DiceGraphic extends Dice {

   /**
    * A graphic representation of a dice.
    */
    final String[] graphicDie = {"-", "⚀", "⚁", "⚂", "⚃", "⚄", "⚅"};

    /**
     * Print a string representation of the object.
     * @return String
     */
    @Override
    default String print () {
        return graphicDie[value()];
    }
}
