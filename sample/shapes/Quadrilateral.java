abstract class Quadrilateral extends AbstractShape {
    protected double width = 1;
    protected double height = 1;
    protected String name;

    public double area() {
        return width * height;
    }
    public double circumference() {
        return width * 2 + height * 2;
    }
    public String toString() {
        return name + " " + pos + " w:" + width + " h:" + height;
    }
}
