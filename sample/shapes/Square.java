class Square extends Quadrilateral {
    Square(double width) {
        this.name = "Square";
        this.width = width;
        this.height = width;
    }
}
