public class Main {
    public static void main(String[] args) {
        System.out.println("# Shapes");

        OneDimension point = new Point();
        System.out.println(point);

        OneDimension line = new Line();
        System.out.println(line);
 
        Quadrilateral rect = new Rectangle(5, 5);
        System.out.println(rect);
 
        Quadrilateral square = new Square(5);
        System.out.println(square);
    }
}
