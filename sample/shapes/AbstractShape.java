abstract class AbstractShape implements Shape, Moveable {
    protected Position pos = new Position();
    private boolean moveable = false;

    public boolean isMoveable() {
        return moveable;
    }
    public void moveAbsolute(Position pos) {
        this.pos = pos;
    }
    public void moveRelative(Position pos) {
        this.pos.add(pos);
    }

    //abstract public double area();
    //abstract public double circumference();
}
