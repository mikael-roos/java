abstract class OneDimension extends AbstractShape {
    protected String name;

    public double area() {
        return Double.NaN;
    }
    public double circumference() {
        return Double.NaN;
    }
    public String toString() {
        return name + " " + pos;
    }
}
