interface Moveable {
    public boolean isMoveable();
    public void moveAbsolute(Position pos);
    public void moveRelative(Position pos);
}


