class Position {
    public int x = 0;
    public int y = 0;

    public Position() {
        this.x = 0;
        this.y = 0;
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position add(Position pos) {
        this.x += pos.x;
        this.y += pos.y;
        return pos; 
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
