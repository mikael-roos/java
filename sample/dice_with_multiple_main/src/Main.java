/**
 * Example with main and extra classes in separate files, without package
 * naming.
 *
 * Compile and run as:
 *  1. Go to the directory sample/dice/src
 *  2. javac com/example/dice/*.java
 *  3. javac Main.java
 *  4. java Main
 */
import com.example.dice.Dice;

public class Main {
    
    public static void main(String[] args) {
        Dice die = new Dice();

        int roll;
        roll = die.roll();
        System.out.println("The rolled die was: " + roll);
        System.out.println(die);

        Dice die2 = die;
        System.out.println("The rolled die was: " + die2.getValue());
        System.out.println(die2);

        System.out.println("Hashcode die:  " + die.hashCode());
        System.out.println("Hashcode die2: " + die2.hashCode());

        printDie(die2);
    }

    protected static void printDie(Dice die) {
        System.out.println("The rolled die was: " + die.getValue());
        System.out.println("Hashcode die:  " + die.hashCode());
    }
}
