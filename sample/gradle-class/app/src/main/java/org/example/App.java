/*
 * This source file was generated by the Gradle 'init' task
 */
package org.example;

import java.util.Scanner;

public class App {
    public String getGreeting() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        System.out.println(new App().getGreeting());
        read();
    }

    /**
     * Verify that application can read from terminal.
     */
    public static void read () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name:");
        String name = scanner.nextLine();
        System.out.println("Hello, " + name + "!");
        scanner.close();
    }

}
