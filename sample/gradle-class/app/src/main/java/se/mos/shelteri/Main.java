package se.mos.shelter;

public class Main {
    public static void main(String[] args) {
        Shelter shelter = new Shelter();

        Dog pluto = new Dog("Pluto");
        Cat misse = new Cat("Misse");

        shelter.add(pluto);
        shelter.add(misse);

        System.out.println(shelter.play());
    }
}
