package se.mos.shelteri;

public class Cat extends AbstractAnimal {

    Cat (String desc) {
        super(desc);
    }

    @Override
    public String noice () {
        return description + " makes a cattish noce, mjau mjau.";
    }
}
