package se.mos.shelter;

public class Dog extends Animal {
    
    Dog (String desc) {
        super(desc);
    }

    @Override
    String noice () {
        return description + " makes a doggy noice, bark bark";
    }
}
