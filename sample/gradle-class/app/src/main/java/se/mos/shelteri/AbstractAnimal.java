package se.mos.shelteri;

public abstract class AbstractAnimal implements Animal {
    protected String description;

    AbstractAnimal (String desc) {
        description = desc;
    }

    public String noice () {
        return description + " makes a animal noice.";
    }
}
