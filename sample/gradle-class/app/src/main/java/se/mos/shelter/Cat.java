package se.mos.shelter;

public class Cat extends Animal {

    Cat (String desc) {
        super(desc);
    }

    @Override
    String noice () {
        return description + " makes a cattish noce, mjau mjau.";
    }
}
