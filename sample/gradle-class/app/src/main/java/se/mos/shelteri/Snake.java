package se.mos.shelteri;

public class Snake extends AbstractAnimal implements Moveable {

    Snake (String description) {
        super(description);
    } 

    @Override
    public int getPosition () {
        return 0;
    }

    @Override
    public String noice () {
        return description + " makes a snakey noice, zzz zzz";
    }
}
