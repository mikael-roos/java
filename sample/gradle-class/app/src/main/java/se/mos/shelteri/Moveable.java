package se.mos.shelteri;

public interface Moveable {
    int getPosition();
}
