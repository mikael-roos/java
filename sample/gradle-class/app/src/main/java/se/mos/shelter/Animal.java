package se.mos.shelter;

public class Animal {
    protected String description;

    Animal (String desc) {
        description = desc;
    }

    String noice () {
        return description + " makes a animal noice.";
    }
}
