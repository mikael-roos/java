package se.mos.shelteri;

public class Dog extends AbstractAnimal {
    
    Dog (String desc) {
        super(desc);
    }

    @Override
    public String noice () {
        return description + " makes a doggy noice, bark bark";
    }
}
