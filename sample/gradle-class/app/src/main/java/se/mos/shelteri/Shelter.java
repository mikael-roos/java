package se.mos.shelter;

import java.util.Collection;
import java.util.ArrayList;

public class Shelter {
    private Collection<Animal> animals = new ArrayList<>();

    public void add (Animal animal) {
        animals.add(animal);
    }

    public String play () {
        StringBuilder res = new StringBuilder();
        for (Animal animal: animals) {
            res.append(animal.noice() + "\n");
        }
        return res.toString();
    }
}
