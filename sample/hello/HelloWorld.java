/**
 * The HelloWorldApp class implements an application that
 * simply prints "Hello World!" to standard output.
 */
public class HelloWorld {
    
    public static void main(String[] args) {
        System.out.println("Hello, World!");

        Dice die = new Dice();

        int roll;
        roll = die.roll();
        System.out.println("The rolled die was: " + roll);

        // byte b = 127;
        // short s = 32767;
        // int i = (int) (Math.pow(2, 31) - 1);
        // long a = (long) (Math.pow(2, 63) - 1);

        // System.out.println(b);
        // System.out.println(s);
        // System.out.println(i);
        // System.out.println(a);

        float f = (float) Math.PI;
        double d = Math.PI;
        boolean b = true;
        char c = 'A';

        System.out.println(f);
        System.out.println(d);
        System.out.println(b);
        System.out.println("" + c + " " + ((int) c));

        String s = "Hello there";
        System.out.println(s + " (" + s.length() + ")");
    }
}


/**
 * Classic dice with values between 1 and 6.
 */
class Dice {

    private int lastRoll = 0;

    public int roll () {
        lastRoll = (int) (Math.random() * 6 + 1);
        return lastRoll;
    }
}
