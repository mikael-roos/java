import java.util.Scanner;

/**
 * A hello world program in Java.
 */
public class Hello {
    public static void main(String[] args) {
        System.out.println("Hello, World!");

        Scanner in = new Scanner(System.in);
        String choice;
        choice = in.nextLine();
        System.out.println("You entered: " + choice);
    }
}
