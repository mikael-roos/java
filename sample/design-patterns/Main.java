
public class Main {
    public static void main(String[] args) {
        
        // Get access to Singleton class
        ServiceContainer sc = ServiceContainer.getInstance();
        System.out.println(sc);
        sc = ServiceContainer.getInstance();
        System.out.println(sc);

        // Create objects through factory method
        Dice die;

        try {
            die = DiceFactory.create("SixSided");
            System.out.println(die.roll());

            die = DiceFactory.create("Winner");
            System.out.println(die.roll());

            die = DiceFactory.create("Looser");
            System.out.println(die.roll());
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
