/**
 * Example on using the singleton design pattern.
 */
class ServiceContainer {
    private static ServiceContainer instance = null;

    public static ServiceContainer getInstance() {
        if (instance == null) {
            instance = new ServiceContainer();
        }
        return instance;
    }
}