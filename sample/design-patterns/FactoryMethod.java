/**
 * Example on using the factory method design pattern.
 */
class DiceFactory {
    public static Dice create(String type) throws Exception {
        switch (type) {
            case "SixSided":
                return new DiceSixSided();
            case "Winner":
                return new DiceWinner();
            case "Looser":
                return new DiceLooser();
            default:
                throw new Exception("Not supported Dice type");
        }
    }
}

interface Dice {
    public int roll();
}

class DiceSixSided implements Dice {
    public int roll() {
        return (int) (Math.random() * 6 + 1);
    };
}

class DiceLooser implements Dice {
    public int roll() {
        return 1;
    };
}

class DiceWinner implements Dice {
    public int roll() {
        return 6;
    };
}

