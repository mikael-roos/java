import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Example with creating and using an array with strings.
 * https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/time/package-summary.html
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */
public class Main {
    
    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        
        System.out.println("Current date and time (default formatting): ");
        System.out.println(now);
        System.out.println(now.getYear());
        System.out.println(now.getMonth());
        System.out.println(now.getDayOfMonth());
        System.out.println(now.getDayOfWeek());
        
        // Formatting a date and time
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        String formattedDate = now.format(myFormatObj);
        System.out.println("After formatting:\n" + formattedDate);
    }
}
