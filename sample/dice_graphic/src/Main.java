/**
 * Example a DiceGraphic extending the Dice.
 *
 * Compile and run as:
 *  1. javac com/example/dice/Dice*.java Main.java
 *  2. java Main
 */
import com.example.dice.Dice;
import com.example.dice.DiceGraphic;

public class Main {
    
    public static void main(String[] args) {
        Dice die = new Dice();
        DiceGraphic dieGraphic = new DiceGraphic();

        die.roll();
        System.out.println("The rolled die was: " + die);

        dieGraphic.roll();
        System.out.println("The rolled die was: " + dieGraphic);
    }
}
