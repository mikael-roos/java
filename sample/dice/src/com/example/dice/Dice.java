/**
 * Classic dice with values between 1 and 6.
 */
package com.example.dice;

import java.lang.Math;

public class Dice {

    protected int lastRoll = 0;

    private static final int MAX_SIDES = 6;

    public Dice () {
        this.lastRoll = 0;
    }

    public final int roll () {
        this.lastRoll = (int) (Math.random() * MAX_SIDES + 1);
        return this.lastRoll;
    }

    public int getValue () {
        return lastRoll;
    }

    public void setValue (int value) {
        lastRoll = value;
    }

    @Override
    public String toString() {
        return "The last roll of the dice was " + lastRoll + ".";
    }
}
