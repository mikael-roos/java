/**
 * Classic dice with values between 1 and 6.
 */
package com.example.dice;

import java.lang.Math;

public class Dice {

    protected int lastRoll = 0;

    private static final int MAX_SIDES = 6;

    /**
     * Roll the dice and return/save its results.
     * @return integer
     */
    public int roll () {
        lastRoll = (int) (Math.random() * MAX_SIDES + 1);
        return lastRoll;
    }

    /**
     * Get the value of the last roll.
     * @return integer
     */
    public int value () {
        return lastRoll;
    }

    /**
     * Provide a string representation of the object.
     */
    public String toString () {
        return Integer.toString(lastRoll);
    }
}
