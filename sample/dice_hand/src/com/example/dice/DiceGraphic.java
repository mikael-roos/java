/**
 * Dice with a more graphic representation.
 */
package com.example.dice;

public class DiceGraphic extends Dice {

    private String[] representation = {" ", "⚀", "⚁", "⚂", "⚃", "⚄", "⚅"};

    /**
     * Provide a string representation of the object.
     */
    public String toString () {
        return representation[lastRoll];
    }
}
