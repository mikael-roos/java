/**
 * Example with a DiceHand holding Dices.
 *
 * Compile and run as:
 *  1. javac com/example/dice/Dice*.java Main.java
 *  2. java Main
 */
// import com.example.dice.*;
import com.example.dice.Dice;
import com.example.dice.DiceGraphic;
import com.example.dice.DiceHand;

public class Main {
    
    public static void main(String[] args) {
        // Hard code what dices to use
        Dice die1 = new Dice();
        DiceHand hand1 = new DiceHand(5);

        hand1.roll();
       //System.out.println("The hand1 contains: " + hand1);

        // Inject the dices of various types
        DiceHand hand2 = new DiceHand();

        Dice die2 = new Dice();
        
        hand2.add(die2);

        hand2.add(new Dice());
        for (int i = 0; i < 4; i++) {
            hand2.add(new DiceGraphic());
        }
        hand2.roll();
        System.out.println("The hand2 contains: " + hand2);

        // More variants:
        // DiceHand<Dice/DiceGraphic> generic type
        // Use an interface
        // Use reflection with getClass
        // Perhaps clone
    }
}
