/**
 * Example with main and extra classes in the same file, without package naming.
 *
 * Compile and run as:
 *  javac Main.java && java Main 
 */
public class Main {
    
    public static void main(String[] args) {
        Dice die = new Dice();

        int roll;
        roll = die.roll();
        System.out.println("The rolled die was: " + roll);
        System.out.println("The last rolled die was: " + die.value());
        System.out.println(die);
    }
}

/**
 * Classic dice with values between 1 and 6.
 */
class Dice {

    private int lastRoll = 0;

    public Dice () {
        System.out.println("The constructor was called, but it did nothing special");
    }

    public int roll () {
        lastRoll = (int) (Math.random() * 6 + 1);
        return lastRoll;
    }

    public int value () {
        return lastRoll;
    }

    @Override
    public String toString() {
        return "The last roll of the dice was " + lastRoll + ".";
    }
}
