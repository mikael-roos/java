/**
 * Example with main and extra classes in separate files, without package
 * naming.
 *
 * Compile and run as:
 *  javac Main.java Dice.java && java Main 
 */
public class Main {
    
    public static void main(String[] args) {
        Dice die = new Dice();

        int roll;
        roll = die.roll();
        System.out.println("The rolled die was: " + roll);
        System.out.println("The last rolled die was: " + die.value());
        System.out.println(die);
    }
}
