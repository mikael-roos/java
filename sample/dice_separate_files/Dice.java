/**
 * Classic dice with values between 1 and 6.
 */
class Dice {

    private int lastRoll = 0;

    public int roll () {
        lastRoll = (int) (Math.random() * 6 + 1);
        return lastRoll;
    }

    public int value () {
        return lastRoll;
    }

    @Override
    public String toString() {
        return "The last roll of the dice was " + lastRoll + ".";
    }
}
