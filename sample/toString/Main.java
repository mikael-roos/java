/**
 * Show how to implement use toString(), one class has implemented toString(), the other 
 * has not and then uses the built-in Object.toString()
 * 
 * Compile & run: javac Main.java && java Main
 */
public class Main {
    public static void main(String[] args) {
        Person1 personOne = new Person1();
        System.out.println("# Output from class Person1");
        System.out.println(personOne);

        Person2 personTwo = new Person2();
        System.out.println("\n# Output from class Person2");
        System.out.println(personTwo);
    }
}

class Person1 {
    private String name = "John/Jane Doe";
}

class Person2 {
    private String name = "John/Jane Doe";

    @Override
    public String toString() {
        return String.format("My name is: %s", name); 
    }
}