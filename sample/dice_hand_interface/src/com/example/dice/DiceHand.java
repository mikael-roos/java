/**
 * DiceHand that holds any type of "dices".
 */
package com.example.dice;

import java.util.ArrayList;

public class DiceHand {

    private ArrayList<Dice> hand = new ArrayList<Dice>();

    /**
     * Default constructor.
     */
    public DiceHand () {
    }

    /**
     * Add dice to the hand.
     */
    public void add (Dice die) {
        hand.add(die);
    }

    /**
     * Roll all dices.
     * @return void
     */
    public void roll () {
        for (Dice die: hand) {
            die.roll();
        }
    }

    /**
     * Provide a string representation of the object.
     * @return String
     */
    public String toString () {
        String result = "";
        Dice die;

        for (int i = 0; i < hand.size(); i++) {
            die = hand.get(i);
            result += "[" + i + "] " + die + " ";
        }

        return result;
    }
}
