---
author: mos
revision: 
    2023-08-09: "(A, mos) first version"
---
Example code - DiceHand with interfaces
====================

This example program shows how interfaces can be used in Java. The code is build on a class DiceHand that holds different types of Dice objects.

The example also shows how to implement your own exception class in DiceException.



Build and run
------------------------

You can build and run the example program like this.

Build it.

```
cd src
javac Main.java com/example/dice/Dice*.java
```

Then run it.

```
java Main
```



UML class diagram
------------------------

Here is the UML class diagram for the code.

![class diagram](.img/class-diagram.png)

Consider if there is another way to create the class diagram for this example.
