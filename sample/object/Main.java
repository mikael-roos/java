import java.lang.reflect.*;

class Main {
    public static void main(String[] args) {
        Object o = new Object();

        System.out.println(o);
        Reflection.DumpMethods(o);
    }
}



class Reflection {
    public static void DumpMethods(Object o)
    {
        try {
        //Class c = Class.forName(o);
        Class c = o.getClass();
        Method m[] = c.getDeclaredMethods();
        for (int i = 0; i < m.length; i++)
            System.out.println(m[i].toString());
        }
        catch (Throwable e) {
        System.err.println(e);
        }
    }
}
