/**
 * Testcases for a calculator, to show off how unit tests works.
 */
package junit5;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

class CalculatorTest {

    private final Calculator calculator = new Calculator();

    @Test
    void addition() {
        int res, exp;

        res = calculator.add(1, 1);
        exp = 2;
        assertEquals(exp, res);

        res = calculator.add(2, 2);
        exp = 4;
        assertEquals(exp, res);
    }

    @Test
    void division() {
        double res, exp;

        res = calculator.division(1, 1);
        exp = 1;
        assertEquals(exp, res);

        res = calculator.division(1, 2);
        exp = 0.5;
        assertEquals(exp, res);

        res = calculator.division(0, 1);
        exp = 0;
        assertEquals(exp, res);

        res = calculator.division(1, 0);
        exp = Double.POSITIVE_INFINITY;
        assertEquals(exp, res);
    }

    @Test
    void divisionWithException() {
        Exception exception;
        
        exception = assertThrows(
            ArithmeticException.class,
            () -> calculator.divisionWithException(1, 0)
        );
    
        Boolean check = exception.getMessage().contains("zero");
        assertTrue(check);
    }
}
