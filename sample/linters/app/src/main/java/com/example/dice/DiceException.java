/**
 * Exception to use with Dice.
 */
package com.example.dice;

public class DiceException extends Exception {

  public DiceException(String errorMessage) {
    super(errorMessage);
  }
}
