/**
 * Classic dice with values between 1 and 6.
 */
package com.example.dice;

import java.lang.Math;

public class DiceSixSided implements Dice {

    protected int lastRoll = 0;

    /**
     * Roll the dice and return/save its results.
     * @return integer
     */
    public int roll () {
        lastRoll = (int) (Math.random() * 6 + 1);
        return lastRoll;
    }

    /**
     * Get the value of the last roll, returns 0 if the die is not yet rolled.
     * @return integer
     */
    public int value () {
        return lastRoll;
    }

    /**
     * Set the value of the last roll, nice for cheating.
     * @param int value the dice value to set 
     * @return void
     */
    @Override
    public void set (int value) throws DiceException {
        Dice.super.set(value);
        lastRoll = value;
    }

    /**
     * Print a string representation of the object.
     * @return String
     */
    public String toString () {
        return print();
    }
}
