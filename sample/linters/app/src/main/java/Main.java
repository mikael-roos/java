/**
 * Example with a DiceHand holding Dices.
 *
 * Compile and run as:
 *  1. javac com/example/dice/Dice*.java Main.java
 *  2. java Main
 */
import com.example.dice.Dice;
import com.example.dice.DiceGraphic;
import com.example.dice.DiceHand;
import com.example.dice.DiceSixSided;
import com.example.dice.DiceSixSidedGraphic;
import com.example.dice.DiceException;

public class Main {
    
    public static void main(String[] args) {
        // Try a dice
        DiceSixSided die1 = new DiceSixSided();
        die1.roll();
        System.out.println("The dice has the value: " + die1);

        // Try the graphic dice
        DiceSixSidedGraphic die2 = new DiceSixSidedGraphic();
        die2.roll();
        System.out.println("The dice has the value: " + die2);

        try {
            die2.set(6);
            System.out.println("Cheating...  the value: " + die2);
        }
        catch (DiceException exc) {
            System.out.println(exc);
        }

        // Use a dicehand
        DiceHand hand = new DiceHand();

        hand.add(new DiceSixSided());
        for (int i = 0; i < 4; i++) {
            hand.add(new DiceSixSidedGraphic());
        }
        hand.roll();
        System.out.println("The hand contains: " + hand);
    }
}
