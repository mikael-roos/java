package json_csv;

class Person {
    public String firstName;
    public String lastName;
    public int age;

    Person () {

    }

    Person (String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String toString() {
        return this.firstName + " " + this.lastName + " (" + age + ")";
    }
}