/**
 * Example with a menu driven application.
 */
import java.util.Scanner;
import java.time.LocalDate;

public class Main {
    
    public static void main(String[] args) {
        Menu menu = new Menu();

        menu.menuLoop();
    }
}

/**
 * A menu class to show a menu and call methods depending on the selected
 * option.
 */
class Menu {

    public void menuLoop () {
        Scanner in = new Scanner(System.in);
        String choice;
        String result;

        outer: do {
            System.out.print(menu());
            choice = in.nextLine();

            switch (choice) {
                case "1":
                    result = getQuote();
                    System.out.println(result);
                    break;

                case "2":
                    result = getRandomNumber1To100();
                    System.out.println(result);
                    break;

                case "3":
                    result = getDateOfToday();
                    System.out.println(result);
                    break;

                case "q":
                case "Q":
                    System.out.println("Bye bye!");
                    break outer;

                default:
                    System.out.println("No such choice, try again!");
                    break;
            }

            System.out.println("Press \"ENTER\" to continue...");
            in.nextLine();
        } while (true);
    }

    public String menu () {
        return """
            Menu:
            1) Get a quote
            2) Get a random number between 1 and 100
            3) Get date of today
            qQ) Quit

            Your choice:""";
        }

    public String getQuote () {
        return """
            Get busy working
            or
            get busy coding.
            --Unknown
            """;
    }

    public String getRandomNumber1To100 () {
        int num = (int) (Math.random() * 100 + 1);
        String res = """
            Your number is '%d'.
            """;
        
        return res.formatted(num);
    }

    public String getDateOfToday () {
        Scanner in = new Scanner(System.in);

        LocalDate today = LocalDate.now();
        return "Today is " + today;
    }
}
