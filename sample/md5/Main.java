import java.security.MessageDigest;
import java.util.Base64;

public class Main {
    public static void main(String[] args) {
        String firstName = "Mikael";
        String lastName = "Roos";
        int birthYear = 55;

        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            String baseText = firstName + lastName + birthYear;
            byte[] md5Hash = m.digest(baseText.getBytes());
            System.out.println(baseText);
            System.out.println(md5Hash);

            // make byte[] as base64 encoded string
            String base64Str = Base64.getEncoder().encodeToString(md5Hash);
            System.out.println(base64Str);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
