/**
 * Showing how to use static properties and methos from the outside.
 *
 * Compile and run as:
 *  javac Main.java && java Main
 */
public class Main {
    
    public static void main(String[] args) {
        System.out.println(MyMath.PI);
        System.out.println(MyMath.random());

        MyMath obj = new MyMath();
        System.out.println(obj.PI);
        System.out.println(obj.random());
    }
}

class MyMath {

    public static double PI = 3.14;

    public static int random () {
        return 42;
    }
}