/**
 * Dice with a more graphic representation.
 */
package com.example.dice;

public class DiceGraphic extends Dice {

    private String[] graphicDie = {" ", "⚀", "⚁", "⚂", "⚃", "⚄", "⚅"};

    /**
     * Provide a string representation of the object.
     */
    public String toString () {
        return graphicDie[lastRoll];
    }
}
