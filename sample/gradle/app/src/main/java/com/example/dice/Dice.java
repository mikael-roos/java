/**
 * Classic dice with values between 1 and 6.
 */
package com.example.dice;

import java.lang.Math;

public class Dice {

    protected int lastRoll = 0;

    /**
     * Roll the dice and return/save its results.
     * @return integer
     */
    public int roll () {
        lastRoll = (int) (Math.random() * 6 + 1);
        return lastRoll;
    }

    /**
     * Get the value of the last roll.
     * @return integer
     */
    public int value () {
        return lastRoll;
    }

    /**
     * Provide a string representation of the object.
     */
    public String toString () {
        return Integer.toString(lastRoll);
    }
}
