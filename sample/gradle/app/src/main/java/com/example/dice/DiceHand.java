/**
 * DiceHand that holds Dices.
 */
package com.example.dice;

import java.util.ArrayList;

public class DiceHand {

    private ArrayList<Dice> hand = new ArrayList<Dice>();

    /**
     * Default constructor.
     */
    public DiceHand () {
    }

    /**
     * Constructor to create num dices.
     */
    public DiceHand (int num) {
        for (int i = 0; i < num; i++) {
            hand.add(new Dice());
            //hand.add(new DiceGraphic());
        }
    }

    /**
     * Add dice to the hand.
     */
    public void add (Dice die) {
        hand.add(die);
    }

    /**
     * Roll all dices.
     */
    public void roll () {
        for (Dice die: hand) {
            die.roll();
        }
    }

    /**
     * Provide a string representation of the object.
     * @return String
     */
    public String toString () {
        String result = "";
        Dice die;
        int value;

        for (int i = 0; i < hand.size(); i++) {
            die = hand.get(i);
            result += "[" + i + "] " + die + " ";
        }

        return result;
    }
}
