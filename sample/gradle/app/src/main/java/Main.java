/**
 * Example with a DiceHand holding Dices.
 *
 * Compile and run as:
 *  1. ./gradlew run
 *  2. ./gradlew javadoc
 */
import java.util.Scanner;

import com.example.dice.Dice;
import com.example.dice.DiceGraphic;
import com.example.dice.DiceHand;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main {
    
    public static void main(String[] args) {
        System.out.println("Try that input works");

        try (Scanner in = new Scanner(System.in)) {
            String choice = in.nextLine();
            System.out.println(choice);
        }
        //in.close();

        // Hard code what dices to use
        Dice die1 = new Dice();
        DiceHand hand1 = new DiceHand(5);

        hand1.roll();
        System.out.println("The hand1 contains: " + hand1);

        // Inject the dices of various types
        DiceHand hand2 = new DiceHand();

        hand2.add(new Dice());
        for (int i = 0; i < 4; i++) {
            hand2.add(new DiceGraphic());
        }
        hand2.roll();
        System.out.println("The hand2 contains: " + hand2);

        // Use the external module for converting objects to JSON
        //Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json;
        
        json = gson.toJson(die1);
        System.out.println("JSON: " + json);

        json = gson.toJson(hand2);
        System.out.println("JSON: " + json);
    }
}
