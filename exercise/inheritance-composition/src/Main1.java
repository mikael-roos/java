/**
 * Example with main and extra classes in separate files, without package
 * naming.
 *
 * Compile and run as:
 *  1. Goto this directory
 *  2. javac se/mos/dice/*.java
 *  3. javac Main.java
 *  4. java Main
 * or in one line
 *  1. javac se/mos/dice/*.java Main.java && java Main
 */
import java.lang.Math;
import java.util.Random;

import static java.lang.System.out;

public class Main1 {
    
    public static void main(String[] args) {
        Random rand = new Random();

        int num = rand.nextInt(6) + 1;
        double val = Math.random();
        out.println(val);
        out.println(num);
    }
}
