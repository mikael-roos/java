package se.mos.dicehand;

//import se.mos.diceinherit.*;
import se.mos.diceinherit.DiceSixSide;
import se.mos.diceinherit.DiceGraphic;
import se.mos.diceinherit.DiceCheat;

public class DiceHand {
    DiceSixSide die;
    DiceSixSide graphic;
    DiceSixSide cheat;

    public DiceHand () {
        die = new DiceSixSide();
        graphic = new DiceGraphic();
        cheat = new DiceCheat();
    }

    public void roll () {
        die.roll();
        graphic.roll();
        cheat.roll();
    }

    @Override
    public String toString () {
        return String.format("%s %s %s", die, graphic, cheat);
    }
}