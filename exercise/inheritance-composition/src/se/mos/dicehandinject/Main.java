/**
 * Example with dicehand holding dices.
 *
 * Compile and run as:
 *  1. javac se/mos/dicehandinject/*.java Main.java && java se.mos.dicehandinject.Main
 */
package se.mos.dicehandinject;

import se.mos.diceinherit.DiceSixSide;
import se.mos.diceinherit.DiceGraphic;
import se.mos.diceinherit.DiceCheat;

public class Main {
    
    public static void main(String[] args) {
        DiceSixSide die = new DiceSixSide();
        DiceSixSide graphic = new DiceGraphic();
        DiceSixSide cheat = new DiceCheat();
    
        DiceHand hand = new DiceHand();
        hand.add(die);
        hand.add(graphic);
        hand.add(cheat);

        hand.roll();
        System.out.println(hand);
    }
}
