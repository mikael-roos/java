/**
 * javac se/mos/inheritance/*.java Main.java; java se.mos.inheritance.Main
 */
package se.mos.inheritance;

public class Main {
    public static void main(String[] args) {
        Parent parent = new Parent("I am the parent class");
        Child child = new Child("I am the child class");

        System.out.println(parent);
        System.out.println(child);
    }
}
