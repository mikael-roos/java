package se.mos.inheritance;

public class Child extends Parent {

    Child (String desc) {
        super(desc);
    }

    @Override
    public String toString () {
        return "Child: " + description;
    }
}
