/**
 * Example with separate dices, no inheritance.
 *
 * Compile and run as:
 *  1. javac se/mos/diceInherit/*.java Main.java && java se.mos.diceInherit.Main
 */
package se.mos.diceinherit;

public class Main {
    
    public static void main(String[] args) {
        DiceSixSide die = new DiceSixSide();
        DiceSixSide graphic = new DiceGraphic();
        DiceSixSide cheat = new DiceCheat();

        die.roll();
        graphic.roll();
        cheat.roll();
        System.out.print(die + " ");
        System.out.print(graphic + " ");
        System.out.println(cheat);
    }
}
