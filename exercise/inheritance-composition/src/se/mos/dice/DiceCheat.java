/**
 * A cheating dice always rolling 6.
 */
package se.mos.dice;

import java.lang.Math;

public class DiceCheat {

    private int lastRoll = 6;

    public final int roll () {
        this.lastRoll = 6;
        return this.lastRoll;
    }

    public final int getLastRoll () {
        return lastRoll;
    }

    public void setLastRoll (int value) {
        lastRoll = 6;
    }

    @Override
    public String toString() {
        return "[" + lastRoll + "]";
    }
}
