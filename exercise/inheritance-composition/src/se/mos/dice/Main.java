/**
 * Example with separate dices, no inheritance.
 *
 * Compile and run as:
 *  1. javac se/mos/dice/*.java Main.java && java se.mos.dice.Main
 */
package se.mos.dice;

public class Main {
    
    public static void main(String[] args) {
        DiceSixSide die = new DiceSixSide();
        DiceGraphic graphic = new DiceGraphic();
        DiceCheat cheat = new DiceCheat();

        die.roll();
        graphic.roll();
        cheat.roll();
        System.out.print(die + " ");
        System.out.print(graphic + " ");
        System.out.println(cheat);
    }
}
