---
revision:
    "2024-11-25": "(PA1, mos) For autumn 2024."
---
Java programming: Inheritance and composition
===========================

This is example code for reviewing how inheritance and composition works when programming Java.

[[_TOC_]]



About inheritance
---------------------------

Inheritance is a fundamental concept in Java that allows one class (called the subclass or child class) to inherit the fields and methods of another class (called the superclass or parent class). It promotes code reuse and helps in organizing related classes into a hierarchy.



About composition
---------------------------

Composition is a design principle in Java where one class contains an instance of another class as a field. Instead of inheriting from a class (as in inheritance), composition models a "has-a" relationship, meaning one object is made up of or uses other objects.



Inheritance
---------------------------

```java
/**
 * javac se/mos/inheritance/*.java Main.java; java se.mos.inheritance.Main
 */
package se.mos.inheritance;

public class Main {
    public static void main(String[] args) {
        Parent parent = new Parent("I am the parent class");
        Child child = new Child("I am the child class");

        System.out.println(parent);
        System.out.println(child);
    }
}
```

```java
package se.mos.inheritance;

public class Parent {
    protected String description;

    Parent (String desc) {
        description = desc;
    }

    @Override
    public String toString () {
        return description;
    }
}
```

```java
package se.mos.inheritance;

public class Child extends Parent {

    Child (String desc) {
        super(desc);
    }

    @Override
    public String toString () {
        return description;
    }
}
```

UML diagram.



Composition
---------------------------

```java
/**
 * javac se/mos/composition/*.java Main.java; java se.mos.composition.Main
 */
package se.mos.composition;

public class Main {
    public static void main(String[] args) {
        Component component = new Component("I am the complete component");
        Part part1 = new Part("I am standalone part 1");
        Part part2 = new Part("I am standalone part 2");

        System.out.println(part1);
        System.out.println(part2);
        System.out.println(component);
    }
}
```

```java
package se.mos.composition;

public class Component {
    private String description;
    Part partX;
    Part partY;

    Component (String desc) {
        description = desc;
        partX = new Part("Component owns partX");
        partY = new Part("Component owns partY");
    }

    @Override
    public String toString () {
        return String.format("%s %s %s.", description, partX, partY);
    }
}
```

```java
package se.mos.composition;

public class Part {
    String description;

    Part (String desc) {
        description = desc;
    }

    @Override
    public String toString () {
        return String.format("[%s]", description);
    }
}
```

UML diagram.




Summary
---------------------------

