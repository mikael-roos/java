---
revision:
    "2024-11-17": "(C, mos) Rewritten in new structure and new code."
    "2023-08-01": "(B, mos) Worked through, minor edits."
    "2022-08-26": "(A, mos) For autumn 2022."
---
![uml diagram](img/class-diagram.png)

Create a menu driven application
===========================

This exercise will guide you into creating a menu driven application in Java and run it in the terminal.

[[_TOC_]]



Precondition
--------------------------

You have understanding of the basic programming constructs in Java like data types, conditions and iterations. You can create classes and instantiate objects. You can work with input and output with the terminal.

Do not forget to look up the builtin methods through [Java docs online](https://docs.oracle.com/en/java/javase/).



Prepare
--------------------------

Create a working directory where you can save all your files. 

```bash
cd java

mkdir menu
cd menu
ls
```

Open your vscode to the directory.

```bash
code .
```

Now you are ready to work with this exercise.



Create the main program and the Menu class
--------------------------

I will show you how to create the main program an you should be able to create the Menu class on your own. There will be a suggestion for a solution, if you need it.

First we create the main program in `Main.java`, it will be the classic main template. Within it we instantiate an object of the Menu class and we hand over the control to the class to execute the application.

```java
public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.run();
    }
}
```

The idea is to have a really small main program and put all code behind the menu class.

You should now create the file `Menu.java` and create the smallest amount of code that makes it possible to compile and execute the code.

<details>
<summary>Compile and execute in Windows PowerShell terminal</summary>


If you are on an older version of PowerShell (pre 7), then you need to compile and execute like this.

```bash
javac *.java; java Main
```

On newer versions of PowerShell you can compile all Java files `*.java` and if it is successful execute the main program.

```bash
javac *.java && java Main
```

</details>

<details>
<summary>Compile and execute in Bash terminal</summary>

Compile all Java files `*.java` and if it is successful execute the main program.

```bash
javac *.java && java Main
```

</details>

<details>
<summary>Solution class Menu</summary>

This is how the class Dice can look with a method `run()` having an empty body.

Lets add a print statement in the empty method, to verify that it is called.

```java
public class Menu {
    public void run() {
        System.out.println("Menu is ready");
    }
}
```

</details>



Print out the menu
--------------------------

Now create the outline for the menu and print it out. Put the output of the menu into its own method and name it `printMenu()`.

It could like this when you execute the program and prints the menu.

```
 -----------------
| 1) Menu choice 1
| 2) Menu choice 2
| 3) Menu choice 3
| m) Print menu
| qQ) Quit
 -----------------
```

<details>
<summary>Solution printing the menu</summary>

This is how you can implement one method that prints the menu and how to call it. We make the method private since it is only used within the class.

```java
public class Menu {
    public void run() {
        printMenu();
    }

    private void printMenu() {
        String menuText = """
             -----------------
            | 1) Menu choice 1
            | 2) Menu choice 2
            | 3) Menu choice 3
            | m) Print menu
            | qQ) Quit
             -----------------""";

        System.out.println(menuText);
    }
}
```

</details>



Read the choice from the user
--------------------------

Now create code to read the choice from the user. Lets put this code into its own method and name it `getChoice()`, the method should return a stringre presenting the choice the user entered through the terminal.

It could look like this when you are done.

```
 -----------------
| 1) Menu choice 1
| 2) Menu choice 2
| 3) Menu choice 3
| m) Print menu
| qQ) Quit
 -----------------
Enter your choice: 1
You entered: 1
```

<details>
<summary>Solution read the menu choice</summary>

This is how you can implement one method that reads a string from the terminal and returns it to the caller. We put the scanner as a private member and we instantiate it in the constructor.

```java
import java.util.Scanner;

public class Menu {
    private Scanner scanner;

    public Menu () {
        this.scanner = new Scanner(System.in);
    }

    public void run() {
        String choice; 

        printMenu();
        choice = getChoice();

        System.out.println("You entered: " + choice);
    }

    private String getChoice() {
        System.out.print("Enter your choice: ");
        return scanner.nextLine();
    }
}
```

</details>

Remember that you should not copy the solution, you need to readm understand and then write the code by yourself.



Create the menu loop
--------------------------

Now we add a loop around it to be able to make several choices and the use `q` or `Q` to quit the program. The loop happens within the `run()` method. Feel free to use any loop construction you feel is suitable.

It can look like this when you execute the code.

```
 -----------------
| 1) Menu choice 1
| 2) Menu choice 2
| 3) Menu choice 3
| m) Print menu
| qQ) Quit
 -----------------
Enter your choice: 1
Enter your choice: 2
Enter your choice: q
```

<details>
<summary>Solution menu loop</summary>

This is how you can implement the menu loop using a do-while loop that continues until the choice is q or Q.

```java
    public void run() {
        String choice; 

        printMenu();
        do {
            choice = getChoice();
        } while (!(choice.equals("q") || choice.equals("Q")));
    }
```

</details>



Implement the menu choice to print the menu
--------------------------

Now we start to implement the switch-case to enable the menu choices. The first one is to select `m` to print the menu.

It can look like this when you first enter `m` and the enter `q`.

```
 -----------------
| 1) Menu choice 1
| 2) Menu choice 2
| 3) Menu choice 3
| m) Print menu
| qQ) Quit
 -----------------
Enter your choice: m
 -----------------
| 1) Menu choice 1
| 2) Menu choice 2
| 3) Menu choice 3
| m) Print menu
| qQ) Quit
 -----------------
Enter your choice: q
```

<details>
<summary>Solution switch-case and menu item `m`</summary>

This is how you can implement the switch-case with the menu item `m` where you call the method to print the menu. Notice that we also add a case for `q` and `Q` since they are valid menu choices but nothing should happen.

```java
    public void run() {
        String choice; 

        printMenu();
        do {
            choice = getChoice();
            switch (choice) {
                case "m": 
                    printMenu();
                    break;
                case "q":
                case "Q":
                    break;
                default:
                    System.err.println("Bad menu choice, use 'm' to get the menu.");
            }
        } while (!(choice.equals("q") || choice.equals("Q")));
    }
```

</details>



Create menu choice "1) Print my avatar"
--------------------------

Now we create the first real menu choice to be "1) Print my avatar" and it should print ascii art from your own selection and it should say some greeting.

Put all the code within its own class in the file `Avatar.java`. The method should be called `print()` and it should be a static method so it can be called like `Avatar.print()`.

The code in the menu looks like this.

```java
        do {
            choice = getChoice();
            switch (choice) {
                case "1":
                    Avatar.print();
                    break;
                case "m": 
                    printMenu();
                    break;
                case "q":
                case "Q":
                    break;
                default:
                    System.err.println("Bad menu choice, use 'm' to get the menu.");
            }
        } while (!(choice.equals("q") || choice.equals("Q")));
```

It could look like this when you execute the program.

```
 -----------------
| 1) Print my avatar
| 2) Menu choice 2
| 3) Menu choice 3
| m) Print menu
| qQ) Quit
 -----------------
Enter your choice: 1
                      ,     ,
                     (\____/)
Hello, I am Marvin!   (_oo_)
                        (O)
What can I do         __||__    \/
 you for?          []/______\[] /
                   / \______/ \/
                  /    /__\
                 /\   /____\ 

Enter your choice: q
```

You may get into trouble if you are using a complex ascii art image where the characters needs to be escaped. Choose a rather simple ascii art to avoid issues (google ascii art to find an image).

Or look at my solution where I store the ascii art in a text file to avoid problems. You may use my ascii art if you do not want to find your own, but do change some parts to "make it your own".

<details>
<summary>Solution menu choice "1) Print my avatar"</summary>

Put all the code within its own class. I had severe issues with my ascii art since it contains characters like `\` that needs to be escaped like `\\` to print as a backslash. I then opted to save the complete ascii art in a text file that I load and print. You can look at the text file in [`avatar.txt`](./avatar.txt).

```java
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.IOException;

class Marvin {
    public static void printMarvin() {
        Path path = Path.of("avatar.txt");
        try {
            String asciiArt = Files.readString(path);
            System.out.println(asciiArt);
        } catch (IOException e) {
            System.err.println("Error reading the file " + path);
        }
    }
}
```

You can also see how I wrap the reading of the file within a try-catch block. I need to do this since the method `Files.readString()` throws an expception if something goes wrong and I do not want to declare this method to throw an exception.

I set the method to be static so the user does not need to create an object to call it, the method can be called using the class only `Avatar.print()`.

</details>



Create menu choice "2) Print today date/time"
--------------------------

Add a new menu item that prints the current date and time. 

Put all the code for this menu choice in the file `Today.java` and create a method `Today.print()` prints the details.

The updated part of the menu loop can look like this.

```java
        do {
            choice = getChoice();
            switch (choice) {
                case "1":
                    Avatar.print();
                    break;
                case "2":
                    Today.print();
                    break;
```

Java has built-in methods for this and you can for example use [`LocalDateTime.now()`](https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/time/LocalDateTime.html#now()) to get the date and the time. You can then use [`DateTimeFormatter.ofPattern()`](https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/time/format/DateTimeFormatter.html#ofPattern(java.lang.String)) to format the output of the string.

When you enter this choice it could look like this.

```
 -----------------
| 1) Print my avatar
| 2) Print today date/time
| 3) Menu choice 3
| m) Print menu
| qQ) Quit
 -----------------
Enter your choice: 2
Not formatted date/time: 2024-11-16T18:15:16.135607046
Formatted date and time: 2024-11-16 18:15:16
Enter your choice: 
```

<details>
<summary>Solution menu choice "2) Print today date/time"</summary>

The code below uses the builtin classes to get the current date and time and then print it out. It then uses a date formatter where we can format the date as any string format.

```java
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Today {
    public static void print() {
        LocalDateTime now = LocalDateTime.now();

        System.out.println("Not formatted date/time: " + date);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = now.format(formatter);
        System.out.println("Formatted date and time: " + formattedDateTime);
    }
}
```

You can read more on how date and time works in Java in the tutorial "[The Java™ Tutorials: Date and Time Classes](https://docs.oracle.com/javase/tutorial/datetime/iso/datetime.html)".

</details>



Class diagram
--------------------------

We can create an UML diagram to represent the classes.

![uml diagram](img/class-diagram.png)

_Figure. An UML diagram of the classes involved in this menu program._

Can you review the UML diagram and see all parts that you have implemented in this exercise?



Summary
---------------------------

You have created a menu driven application with Java in the terminal and you have learned to use some of the builtin methods in the Java API.
