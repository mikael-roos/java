import java.util.Scanner;

public class Menu {
    private Scanner scanner;

    public Menu () {
        this.scanner = new Scanner(System.in);
    }

    public void run() {
        String choice; 

        printMenu();
        do {
            choice = getChoice();
            switch (choice) {
                case "1":
                    Avatar.print();
                    break;
                case "2":
                    Today.print();
                    break;
                case "m": 
                    printMenu();
                    break;
                case "q":
                case "Q":
                    break;
                default:
                    System.err.println("Bad menu choice, use 'm' to get the menu.");
            }
        } while (!(choice.equals("q") || choice.equals("Q")));
    }

    private String getChoice() {
        System.out.print("Enter your choice: ");
        return scanner.nextLine();
    }

    private void printMenu() {
        String menuText = """
             -----------------
            | 1) Print my avatar
            | 2) Print today date/time
            | 3) Menu choice 3
            | m) Print menu
            | qQ) Quit
             -----------------""";

        System.out.println(menuText);
    }
}
