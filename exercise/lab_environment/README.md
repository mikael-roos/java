---
revision:
    "2023-08-01": "(B, mos) Worked through it, minor edits."
    "2022-08-26": "(A, mos) For autumn 2022."
---
Lab environment
===========================

This is the lab environment (to start with) that is recommended for this course.

[[_TOC_]]



Git
---------------------------

We will use [Git](https://git-scm.com/) & GitLab, so you should install it locally.

Before you install it you might want to check that you have it installed or not.

```
git --version
```

You may want to view the video "[Install Git from the installation program on Windows 10 and configure it](https://www.youtube.com/watch?v=02u7ao7uK5k&list=PLEtyhUSKTK3iTFcdLANJq0TkKo246XAlv&index=1)".



Bash terminal
---------------------------

Get a bash terminal to work in.

* Linux - you already know where you have it
* Mac - you have it, start "Terminal"
* Windows - choose a terminal to work with
    * Git bash (the terminal is included with the [Git installation](https://git-scm.com/))
    * [WSL/WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
    * [Cygwin](https://www.cygwin.com/)

You may want to view the video "[Use various bash terminals on Windows (Git Bash, Cygwin, Debian/WSL2)](https://www.youtube.com/watch?v=kialYZs6Oyc&list=PLEtyhUSKTK3gHj087mUjPfXyqMvSy2Rwz&index=1)".

If you are a Windows user you "might" be able to work in the Windows terminals CMD or PowerShell. It is however recommended to use a Bash terminal since some of the code samples might use bash command in their examples.



Visual Studio Code
---------------------------

You need a texteditor and the suggestion is "[Visual Studio Code](https://code.visualstudio.com/)" which has good [support for Java](https://code.visualstudio.com/docs/languages/java).

You may choose any another texteditor or IDE.



Java
---------------------------

Read the whole section before you choose how to install.

The latest long-term support (LTS) release of Java is 21, as per 2024. Earlier (and later) versions might work during the course but try to get the latest available LTS version.



### Install with Visual Studio Code

You may [install Java by using Visual Studio Code](https://code.visualstudio.com/docs/languages/java#_install-visual-studio-code-for-java). It will/should detect you are using Java and automatically ask you to install the extension pack for Java. This procedure should work fow Windows and Mac and it installs two extensions to Visual Studio Code.

1. Coding pack for Java.
1. Extension pack for Java.



### Install through Oracle installation program

You can [download and install the latest version of Java SE Development Kit](https://www.oracle.com/java/technologies/downloads/) from the Oracle website, this works for Windows, Mac and Linux.

Choose an installation programs that matches your system and execute it directly.



### Open source version OpenJDK

You may choose the open source variant and install Java through [OpenJDK](https://openjdk.org/), using the package manager for your system.

Here follows how to do that for Mac and Linux.



#### OpenJDK on Mac using brew

This is how to install using the packet manager `brew` on Mac.

```
# Install the default (might be an older version)
brew install openjdk@

# Install a specific/latest version
brew install openjdk@21
```



#### OpenJDK on Linux using apt

This is how to install using the packet manager `apt` on Linux like Ubuntu.

```
# Install the default (might be an older version)
apt install default-jdk

# Install a specific/latest version
apt install openjdk-21-jdk
```



Check that it works
---------------------------

When you are done, open the terminal (both in Visual Studio Code and the terminal application) and check what version you have.

```
java --version
javac --version
```

You should see a matchin version number of the package you choosed to install.



Where to go from here?
---------------------------

Here are a few things you could do to learn a bit about your development environment.



### Learn more on Git

If you are new to Git, you might want to work through the document "[Work with Git](https://mikael-roos.gitlab.io/doc/work-with-git/)" to gain some basic knowledge on how to work with Git and Git repos.

There is a video playlist showing you how to work with the initial steps in Git.

* [Git, GitHub and GitLab - Learn and practice](https://www.youtube.com/playlist?list=PLEtyhUSKTK3iTFcdLANJq0TkKo246XAlv)



### Try out some commands in the terminal

You could open the terminal and [try out some Unix commands](https://mikael-roos.gitlab.io/doc/unix-commands/) to get acquainted with the bash and unix terminal.



### Start visual code from the terminal

You should be able (most likely) to open a terminal and start vscode from within the terminal using the current working directory `.` (the dot) as the entrypoint.

```
code .
```

That is a good way to use the terminal together with the editor.



### Try out Java in vscode

Now is a good idea to create your own Hello World program with vscode and execute it. Here is an article "[Getting Started with Java in VS Code](https://code.visualstudio.com/docs/java/java-tutorial)" showing how to do it.
