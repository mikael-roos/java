---
revision:
    "2024-11-12": "(D, mos) State the recommended way to install through install program."
    "2024-11-03": "(C, mos) Remove git and support powershell."
    "2023-08-01": "(B, mos) Worked through it, minor edits."
    "2022-08-26": "(A, mos) For autumn 2022."
---
Lab environment
===========================

This is the lab environment (to start with) that is recommended for this course.

[[_TOC_]]



Terminal
---------------------------

You need to have a terminal that you could work in on your computer.

* Linux - you already know where you have it
* Mac - you have it, start "Terminal"
* Windows - choose a terminal to work with
    * PowerShell (or cmd.exe) (the Windows terminal)
    * Git bash (the terminal is included with the [Git installation](https://git-scm.com/))
    * Install Ubuntu with a bash-terminal using [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install).



Visual Studio Code
---------------------------

You need a texteditor and the suggestion is "[Visual Studio Code](https://code.visualstudio.com/)". 

You may choose any another texteditor or IDE.



Java
---------------------------

Read the whole section before you choose how to install.

The latest long-term support (LTS) release of Java is 21, as per 2024. Earlier (and later) versions might work during the course but try to get the latest available LTS version.



### Install through Oracle installation program

You can [download and install the latest version of Java SE Development Kit](https://www.oracle.com/java/technologies/downloads/) from the Oracle website, this works for Windows, Mac and Linux.

Choose an installation programs that matches your system and execute it directly.

This is the recommended way for users on Windows and Mac.



### Install with Visual Studio Code

You may [install Java by using Visual Studio Code](https://code.visualstudio.com/docs/languages/java#_install-visual-studio-code-for-java). It will/should detect you are using Java and automatically ask you to install the extension pack for Java. This procedure should work fow Windows and Mac and it installs two extensions to Visual Studio Code.

1. Coding pack for Java.
1. Extension pack for Java.



### Open source version OpenJDK

You may choose the open source variant and install Java through [OpenJDK](https://openjdk.org/), using the package manager for your system.

Here follows how to do that for Mac and Linux.



#### OpenJDK on Mac using brew

This is how to install using the packet manager `brew` on Mac.

```
# Install the default (might be an older version)
brew install openjdk@

# Install a specific/latest version
brew install openjdk@21
```



#### OpenJDK on Linux using apt

This is how to install using the packet manager `apt` on Linux like Ubuntu.

```
# Install the default (might be an older version)
apt install default-jdk

# Install a specific/latest version
apt install openjdk-21-jdk
```



Check that it works
---------------------------

When you are done, open the terminal (both in Visual Studio Code and the terminal application) and check what version you have.

```
java --version
javac --version
```

You should see a matching version number of the package you choosed to install.



Visual Studio Code extensions for Java (OPTIONAL)
---------------------------

The editor vscode has good support for Java. There exists several extensions that you could use to aid when coding Java.

Sometimes you might think that the extensions are bloating your editor, if so you should avoid to install any extensions and wait a bit until you get more comfortable.

Here is a longer article [Java in Visual Studio Code](https://code.visualstudio.com/docs/languages/java) showing the various ways of install Java with vscode together with some extensions. Read it through, before you choose to install anything.



Where to go from here?
---------------------------

Here are a few things you could do to learn a bit about your development environment.



### Try out Java in vscode

Now is a good idea to create your own Hello World program with vscode and execute it. Here is an article "[Getting Started with Java in VS Code](https://code.visualstudio.com/docs/java/java-tutorial#_creating-a-source-code-file)" showing how to do it.
