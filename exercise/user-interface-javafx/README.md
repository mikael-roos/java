---
revision:
    "2024-10-13": "(B, mos) Spelling and change to version 22."
    "2023-10-06": "(A, mos) For autumn 2023."
---
User interface with JavaFX
==========================

This is to learn the basics on how to create a graphical user interface with JavaFX within a Gradle project.

[[_TOC_]]


<!--
TODO

* REbuild the hello world to MVMV design pattern.
* Finish up the example on FXML to Hello world in gradle
    * With or without the scenebuilder
* Next exercise could be a multiwindow application, for example for CRUD 
* How about mobile platforms?

-->

About
--------------------------

The content in this exercise is based on material from the website on [JavaFX](https://openjfx.io/).



JavaFX
--------------------------

JavaFX is an open source client application platform for desktop, mobile and embedded systems built on Java. You can use it to create graphical user interfaces to your application that can run on several platforms.

Review the [roadmap and various versions on JavaFX](https://gluonhq.com/products/javafx/). JavaFX is available in the [Maven repository as OpenJFX](https://mvnrepository.com/artifact/org.openjfx/javafx). 



### JavaFX versus Java Swing

Java Swing is an alternate way of building GUI with Java. Java Swing was developed before Java FX and has a broad user base. You might want to consider both JavaFX and Java Swing when building user interface for your application.

Java Swing is available in the package [`javax.swing`](https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/package-summary.html) and there is a tutorial "[Creating a GUI With Swing](https://docs.oracle.com/javase/tutorial/uiswing/)" on how to get going with it.



Prepare with Gradle
--------------------------

There are several ways to include JavaFX into your application and this exercise will use Gradle as a base. To get going with this exercise you should init a new gradle project, something like this.

```
mkdir javafx
cd javafx
gradle init
```

It can look like this when you are done.

```
$ tree .
.
├── app
│   ├── build.gradle
│   └── src
│       ├── main
│       │   ├── java
│       │   │   └── javafx
│       │   │       └── App.java
│       │   └── resources
│       └── test
│           ├── java
│           │   └── javafx
│           │       └── AppTest.java
│           └── resources
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
└── settings.gradle

12 directories, 8 files
```



Add JavaFX to Gradle
--------------------------

The guide "[Getting started with JavaFX](https://openjfx.io/openjfx-docs/#gradle)" shows how to do this. You might want to check the latest version in the docs.

We add JavaFX intro Gradle by editing the configuration file `build.gradle`.

```
plugins {
  id 'application'
  id 'org.openjfx.javafxplugin' version '0.1.0'
}
```

Next you add each module you require. To start with we only add the module `javafx.controls`.

```
javafx {
    version = "22"
    modules = [ 'javafx.controls' ]
}
```


Hello World
--------------------------

Lets see how we create a "Hello World" with JavaFX.

![JavaFX Hello World](img/javafx-helloworld.png)

_Figure: This is how the "Hello World" looks like._

To create the first JavaFX application we use the following code and place it in the file `app/src/main/java/javafx/App.java`. 

```java
package ui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage stage) {
        String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        Label l = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        Scene scene = new Scene(new StackPane(l), 640, 480);
        stage.setScene(scene);
        stage.show();
    }
    public static void main(String[] args) {
        launch();
    }
}
```

Do also comment out the test cases in `app/src/test/java/javafx/AppTest.java`.

Then we can run and build the program and see that the windows is opened showing a "Hello "world" message.

![Run hello world](img/run-helloworld.png)

_Figure: The "Hello World" is built and run._



JavaFX and Visual Studio Code
--------------------------

When you are done you can reload your project by right-clicking on the file `settings.gradle` and execute "Reload projects" or "Update projects". This makes the dependencies being downloaded locally and the Visual Studio Code will recognize the JavaFX packages.

It should look something like this.

![Source is downloaded](img/integrated-with-vscode.png)

_Figure: The external dependencies are downloaded and the JavaFX packages are recognized._



Walk through the "Hello World" program
--------------------------

Lets review the example program and see what what parts in the documentation if maps to. The [API for JavaFX](https://openjfx.io/javadoc/21/) is available as JavaDoc.

The main class [`javafx.application.Application`](https://openjfx.io/javadoc/21/javafx.graphics/javafx/application/Application.html) is extended and it contains the method [`launch()`](https://openjfx.io/javadoc/21/javafx.graphics/javafx/application/Application.html#launch(java.lang.String...)) that launches the application.

```java
import javafx.application.Application;

public class App extends Application {

    public static void main(String[] args) {
        launch();
    }
}
```

To create the application we override the method [`start()`](https://openjfx.io/javadoc/21/javafx.graphics/javafx/application/Application.html#start(javafx.stage.Stage)) and within it we create the parts of the gui. This method is the main entry point for a JavaFX applications.

Then there is the code that creates the window and its controls.

```java
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage stage) {
        String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        Label l = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        Scene scene = new Scene(new StackPane(l), 640, 480);
        stage.setScene(scene);
        stage.show();
    }
}
```

In JavaFX the Stage ([javafx.stage.Stage](https://openjfx.io/javadoc/21/javafx.graphics/javafx/stage/Stage.html)) is the representation of the native OS Window object. A stage can contain a single Scene ([javafx.scene.Scene](https://openjfx.io/javadoc/21/javafx.graphics/javafx/scene/Scene.html)).

A scene contains Node(s) ([javafx.scene.Node](https://openjfx.io/javadoc/21/javafx.graphics/javafx/scene/Node.html)) in a tree-like structure where a node can have children and one parent.

![Scene graph](img/scene_graph.jpg)

_Picture: An example of Stage, Scene and Node(s) (from [JavaFX Documentation Project](https://fxdocs.github.io/docs/html5/#_scene_graph))_

Looking at the code from "Hello World" we see that the Label ([javafx.scene.control.Label](https://openjfx.io/javadoc/21/javafx.controls/javafx/scene/control/Label.html)) is a Node.

```java
Label l = new Label("Hello, JavaFX ");
```

The node is then added to the scene through the layout of a StackPane ([javafx.scene.layout.StackPane](https://openjfx.io/javadoc/21/javafx.graphics/javafx/scene/layout/StackPane.html)).

```java
Scene scene = new Scene(new StackPane(l), 640, 480);
```

The StackPane layout its Node children in a back-to-front stack where the z-order of the children decides the final result.

When the Scene is created it can be added to a Stage and the stage can be shown.

```java
stage.setScene(scene);
stage.show();
```

Those are some basic building stones on how to create a user interface with JavaFX.



Guessing game with a GUI
--------------------------

Review the example code for the "[Guessing Game](../../sample/javafx-guess/)" that is a slighly larger example program showing how you can layout more controls on the window and how you can work with events and application code.

![Guessing game](img/guess-game.png)

_Figure. The example application "Guessing game"._

All the code is within a single class file [`app/src/main/java/ui/App.java`](../../sample/javafx-guess/app/src/main/java/ui/App.java) to make it easier to see all the code at once. This off course makes it harder to see what is ui code and what is application code and how it interact, but lets leave that discussion on code structure for just a minute.



### Layout

There are many ways to layout the controls and ui elements onto a scene. There are layout classes to support for this. The Guessing game uses a VBox (javafx.scene.layout.VBox) as the root layout element to layout children of ui elements that is organised within StackPane (javafx.scene.layout.StackPane), HBox (javafx.scene.layout.HBox) and more VBox. 

The header is layed out using a StackPane stacking the objects on top of eachother.

```java
StackPane header = new StackPane(
        new Rectangle(200, 100, Color.BLACK),
        new Circle(40, Color.RED),
        new Button("Guess my number!")
);
```

Here is the code that adds the buttons onto a HBox for a horisontal layout.

```java
Button btnGuess = new Button("Guess");
Button btnCheat = new Button("Cheat");
Button btnRestart = new Button("Restart");

HBox buttons = new HBox();
buttons.getChildren().addAll(
    btnGuess,
    btnCheat,
    btnRestart
);
```

At the end the scene is created by adding all items to the root element being a VBox.

```java
vbox.getChildren().addAll(
    header,
    message,
    inputs,
    buttons,
    feedback,
    footer
);
Scene scene = new Scene(vbox, 640, 480);
```



### Style

The style and layout can be affected by margins, padding, spaces and code looking like CSS (Cascading Style Sheets) rules.

Set color and padding using CSS rules on the header.

```java
header.setStyle(
    "-fx-background-color: #00FF00;" +
    "-fx-padding: 10px;"
);
```

Set spacing between the buttons to create som empty space.

```java
buttons.setSpacing(10);
```

Add margins to the header area.

```java
VBox.setMargin(header, new Insets(10.0d));
```



### Events

Each element can have event handlers attached to it. In the game there is event handlers for when the buttons are clicked and when the enter key is pressed within a textfield. These handlers will be called when the event occur.

This is how to add a handler as a method to a specific event. In this case the event is an ActionEvent (javafx.event.ActionEvent).

```java
btnGuess.setOnAction(event -> actionGuess());
btnCheat.setOnAction(event -> actionCheat());
btnRestart.setOnAction(event -> actionRestart());
```

Adding an even to on key press in a textfield looks like this.

```java
TextField input = new TextField();
input.setOnKeyPressed(event -> {
    if (event.getCode() == KeyCode.ENTER) {
        actionGuess();
    }
});
```

The event handler checks if it was the enter key that was pressed and calls a method if that is.

The class Event (javafx.event.Event) is the base class for all events.



### Update ui

The ui scenes contains the visual state of the application. This state can be read from the ui elements. It can also be stored as variables in the class and then update the ui components when the state is changed.

In the game the actual game state is stored as member variables like this.

```java
private final int numberOfGuesses = 5;
private int guessesMade = 0;
private int theNumber;
```

Those are the state variables needed to represent the game state internally.

Some parts of the ui is then updated during the game and these ui elements might also be needed to store a reference to. The game implemented it like this.

```java
private TextField inputGuess;
private Text feedbackMessage;
private Text guessesStatus;
```

The game then interacts and updates the ui elements when needed, here is an example on updating the guesses made. It is pure application code that updates the ui element `guessesStatus` that is a Text element.

```java
private void updateGuesses() {
    guessesStatus.setText(
        "You have made " + guessesMade + 
        " out of " + numberOfGuesses + 
        " guesses possible."
    );
}
```

The application can also read the state from the ui element like this when parsing the new guess that is made in the TextField.

```java
private int parseGuess() {
    String s = inputGuess.getText().trim();

    if (s.isEmpty()) {
        feedbackMessage.setText("You need to enter a guess in the field above!");
        return 0;
    }
    // more code parsing the value to see it is a integer
}
```

How to interact between the ordinary application code and the ui elements is a challenging task that requires some thought on the application structure.



MVVM pattern
--------------------------

The [MVVM pattern](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) is an architectural pattern to separate GUI code from application code. MVVM stands for Model - View - ViewModel.

![MVVM](img/MVVMPattern.png)

_Figure. The Model - View - ViewModel (MVVM) arhitectural pattern ([from Wikipedia](https://en.wikipedia.org/wiki/File:MVVMPattern.png))._

The pattern can be explained like this.

* Model is your own application code, without interference of the ui elements.
* View is the ui, or a scen in the ui, where the user interacts with the application.
* View-Model is the glue between the View and the Model, it connects application code with ui code.

**Model** If we would rewrite the "Guessing Game" in this pattern we could start with the application code, the domain objects, that implement tha actual game. This is the Model containing the game logic and state. It does not contain any reference to the ui.

**View** The we create the View(s) that is the ui scenes where the user interacts with the game. This is pure ui code with elements, layouts, scenes and adding event handlers (that delegates to the ViewModel).

**ViewModel** Then there is the issue of connecting the Model with the View, but still keeping the code structure clear. A ViewModel can be a callback handler for an event that retrives the state of the game from the Model and updates the View.

The view knows about the ViewModel and uses JavaFX data bindings to synchronize the elements in the View with the data in the ViewModel. When data in the ViewModel is updated, the data bindings assure that the View elements is synchronized. When the data is changed in the View, the ViewModel is synchronized with the udpated data.

Even when you create small applications, it will be a good recommendation to follow a code structure like this.

You might have heard of the [architectural pattern MVC (Model, View, Controller)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller). It is commonly used in GUI applications, web applications and client server applications. A simple comparisom with the MVVM pattern would be that the ViewModel is doing the work of the Controller - the glue between the View and the Model.



FXML
--------------------------

FXML is a way to organise your GUI using XML structures. The scenes in a GUI can be organised into a treelike structure, a graph, and FXML can do this in an XML structure. It is an alternative when you create the user interface of your JavaFX application, an alternative to create the scenes using pure Java code.

Read more on FXML in the "[Introduction to FXML](https://openjfx.io/javadoc/21/javafx.fxml/javafx/fxml/doc-files/introduction_to_fxml.html)".



Scene Builder
--------------------------

The [Scene Builder](https://gluonhq.com/products/scene-builder/) is an application that helps you creating your JavaFX UI by using drag and drop and configure each scene with its layout and ui elements. The tool generates FXML files.

![Scene builder](img/scene-builder.png)

_Figure. Creating a Hello World application using Scene Builder._

You can review the [FXML code for the example in Hello-World.fxml](../../sample/scene-builder-hello-world/Hello-World.fxml).



Summary
--------------------------

You have worked through an introduction on how to work with JavaFX to create graphical user interfaces for Java.

Review the documentation on the JavaFX homepage for more resources to learn JavaFX.
