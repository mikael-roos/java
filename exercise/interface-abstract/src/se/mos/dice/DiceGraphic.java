/**
 * A graphical dice.
 */
package se.mos.dice;

import java.lang.Math;

public class DiceGraphic {

    private int lastRoll = 0;
    private static final int MAX_SIDES = 6;
    private String[] graphics = {"⚀", "⚁", "⚂", "⚃", "⚄", "⚅"};

    public DiceGraphic () {
        this.lastRoll = 0;
    }

    public final int roll () {
        this.lastRoll = (int) (Math.random() * MAX_SIDES + 1);
        return this.lastRoll;
    }

    public final int getLastRoll () {
        return lastRoll;
    }

    public void setLastRoll (int value) {
        lastRoll = value;
    }

    @Override
    public String toString() {
        return graphics[lastRoll - 1] + "[" + lastRoll + "]";
    }
}
