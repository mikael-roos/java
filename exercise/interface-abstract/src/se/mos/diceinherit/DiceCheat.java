/**
 * A cheating dice always rolling 6.
 */
package se.mos.diceinherit;

public class DiceCheat extends DiceGraphic{

    public DiceCheat () {
        super();
    }

    public final int roll () {
        lastRoll = 6;
        return lastRoll;
    }

    public final int getLastRoll () {
        return 6;
    }

    public void setLastRoll (int value) {
        lastRoll = 6;
    }
}
