/**
 * A graphical dice extending the six sided dice.
 */
package se.mos.diceinherit;

public class DiceGraphic extends DiceSixSide {

    private String[] graphics = {"⚀", "⚁", "⚂", "⚃", "⚄", "⚅"};

    public DiceGraphic () {
        super();
    }

    @Override
    public String toString() {
        return graphics[getLastRoll() - 1] + "[" + getLastRoll() + "]";
    }
}
