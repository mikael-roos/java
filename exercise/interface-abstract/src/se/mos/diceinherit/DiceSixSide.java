/**
 * Classic dice with values between 1 and 6.
 */
package se.mos.diceinherit;

import java.lang.Math;

public class DiceSixSide {

    protected int lastRoll = 0;

    private static final int MAX_SIDES = 6;

    public DiceSixSide () {
        lastRoll = 0;
    }

    public int roll () {
        lastRoll = (int) (Math.random() * MAX_SIDES + 1);
        return lastRoll;
    }

    public int getLastRoll () {
        return lastRoll;
    }

    public void setLastRoll (int value) {
        lastRoll = value;
    }

    @Override
    public String toString() {
        return "[" + lastRoll + "]";
    }
}
