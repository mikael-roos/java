package se.mos.inheritance;

public class Parent {
    protected String description;

    Parent (String desc) {
        description = desc;
    }

    @Override
    public String toString () {
        return description;
    }
}
