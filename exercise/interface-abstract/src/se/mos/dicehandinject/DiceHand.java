package se.mos.dicehandinject;

import java.util.ArrayList;
import se.mos.diceinherit.DiceSixSide;

public class DiceHand {
    private ArrayList<DiceSixSide> hand = new ArrayList<DiceSixSide>();

    public void add (DiceSixSide die) {
        hand.add(die);
    }

    public void roll () {
        for (DiceSixSide die: hand) {
            die.roll();
        }
    }
    
    @Override
    public String toString () {
        StringBuilder str = new StringBuilder();
        for (DiceSixSide die: hand) {
            str.append(die + " ");
        }
        return str.toString().trim();
    }
}