/**
 * Example with dicehand holding dices.
 *
 * Compile and run as:
 *  1. javac se/mos/dicehand/*.java Main.java && java se.mos.dicehand.Main
 */
package se.mos.dicehand;

public class Main {
    
    public static void main(String[] args) {
        DiceHand hand = new DiceHand();

        hand.roll();
        System.out.println(hand);
    }
}
