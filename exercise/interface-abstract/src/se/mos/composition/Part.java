package se.mos.composition;

public class Part {
    String description;

    Part (String desc) {
        description = desc;
    }

    @Override
    public String toString () {
        return String.format("[%s]", description);
    }
}
