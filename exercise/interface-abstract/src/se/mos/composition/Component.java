package se.mos.composition;

public class Component {
    private String description;
    Part partX;
    Part partY;

    Component (String desc) {
        description = desc;
        partX = new Part("Component owns partX");
        partY = new Part("Component owns partY");
    }

    @Override
    public String toString () {
        return String.format("%s %s %s.", description, partX, partY);
    }
}
