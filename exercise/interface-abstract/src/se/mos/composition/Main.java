/**
 * javac se/mos/composition/*.java Main.java; java se.mos.composition.Main
 */
package se.mos.composition;

public class Main {
    public static void main(String[] args) {
        Component component = new Component("I am the complete component");
        Part part1 = new Part("I am standalone part 1");
        Part part2 = new Part("I am standalone part 2");

        System.out.println(part1);
        System.out.println(part2);
        System.out.println(component);
    }
}
