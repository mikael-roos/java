---
revision:
    "2024-12-09": "(A, mos) For autumn 2024."
---
Java programming: Implement interface and abstract class
===========================

This is example code for reviewing how to implement an interface and work with abstract classes when programming Java.

[[_TOC_]]



About interface
---------------------------

In Java, an interface is a reference type that defines a set of methods that a class must implement. It is like a contract that specifies what a class should do but not how it should do it. Interfaces are used to achieve abstraction. A class implements an interface and a class can implement several interfaces. A interface should be small and only do one thing.

Here is a code example on using interface.

```java
// Define an interface
interface Animal {
    void eat(); // abstract method
    void sleep(); // abstract method
}

// Implement the interface in a class
class Dog implements Animal {
    public void eat() {
        System.out.println("Dog eats food.");
    }
    public void sleep() {
        System.out.println("Dog sleeps.");
    }
}

// Main class
public class Main {
    public static void main(String[] args) {
        Animal myDog = new Dog(); // Polymorphism
        myDog.eat();
        myDog.sleep();
    }
}
```

Why should you use interfaces?

* To define common behavior across unrelated classes.
* To achieve loose coupling in applications.
* (To enable multiple inheritance in Java.)



About abstract class
---------------------------

In Java, an abstract class is a class that is declared with the abstract keyword. It serves as a blueprint for other classes and cannot be instantiated on its own. Abstract classes are used when you want to provide partial implementation for a group of related classes.

Here is a code example on using abstract classes.

```java
// Define an abstract class
abstract class Animal {
    // Abstract method
    abstract void makeSound();
    
    // Concrete method
    void sleep() {
        System.out.println("Sleeping...");
    }
}

// Subclass that extends the abstract class
class Dog extends Animal {
    // Provide implementation for the abstract method
    void makeSound() {
        System.out.println("Bark");
    }
}

// Main class
public class Main {
    public static void main(String[] args) {
        Animal myDog = new Dog(); // Polymorphism
        myDog.makeSound();
        myDog.sleep();
    }
}
```

Why should you use abstract classes?

* To define a common base class with shared behavior.
* To enforce a contract (abstract methods) while allowing some flexibility (concrete methods).
* When you need to share code across related classes but still allow customization.



Use interface with abstrac class
---------------------------

Here is an example that uses en interface to create the basis for an `Animal`. 

The `Mammal` is an abstract class providing a default implementation for an animal of the type mammal. The Mammal implements Animal.

The concrete class `Dog` then extends Mammal to act like an actual dog.

We could say that the Dog is a Mammal and it implements the interface of an Animal.

```java
// Define an interface
interface Animal {
    void eat(); // abstract method
}

// Define an abstract class
abstract class Mammal implements Animal {
    // Concrete method
    void breathe() {
        System.out.println("Breathing...");
    }

    // Abstract method
    abstract void makeSound();
}

// Define a concrete class that extends the abstract class and implements the interface
class Dog extends Mammal {
    // Implement the interface's method
    public void eat() {
        System.out.println("Dog eats food.");
    }

    // Provide implementation for the abstract class's method
    void makeSound() {
        System.out.println("Dog barks.");
    }
}

// Main class
public class Main {
    public static void main(String[] args) {
        Mammal myDog = new Dog(); // Polymorphism
        myDog.eat();              // Interface method
        myDog.breathe();          // Concrete method from abstract class
        myDog.makeSound();        // Abstract class method implementation
    }
}
```



UML diagram
---------------------------

We can draw an UML class diagram of the above code, and we can include more concrete animals. It could then look like this.

![class diagram](img/interface-abstract-class-diagram.png)

_Figure. A class diagram with Animal, Mammal, Dog and Cat._

When building a structure like this you should always consider how the final code looks like. The code structure should help you create code that is readable and easy to maintain and helps you to reuse code sections.

Sometimes using interface and abstract classes improves your final code structure. Sometimes it is overdoing the solution. Learn about interface and abstract so you know when they improve your code and when they do not.

<!--
@startuml
skin rose

title Animal as interface and Mammal as abstract class


interface Animal {
  +eat () : void
}

abstract class Mammal implements Animal {
  +breathe () : void
  +makeSound () : void
}

class Dog extends Mammal {
  +eat () : void
  +breathe () : void
  +makeSound () : void
}

class Cat extends Mammal {
  +eat () : void
  +breathe () : void
  +makeSound () : void
}

@enduml
-->



Larger code example on shapes
---------------------------

There exists a code sample on  a hierarchy of shapes, including inheritance, composition, interface and abstract classes.

The class diagram for the exercise looks like this.

![class diagram shapes](img/shape-full-class-diagram.png)

_Figure. Class diagram of the Shape example._

Here is the [source code for the example](../../sample/shapes/) if you want to dig into how the structure is built up.

The example can be compiled and executed and the produces the following output.

```java
$ javac *.java && java Main     
# Shapes                      
Point (0, 0)                  
Line (0, 0)-(0, 0)            
Rectangle (0, 0) w:5.0 h:5.0  
Square (0, 0) w:5.0 h:5.0     
```



Summary
---------------------------

This was a short introduction with code samples on how to work with implementing interface and on abstract classes.
