---
revision:
    "2022-10-05": "(A, mos) First release."
---
UML class diagrams and tools
===========================

Here follows a set of sample UML class diagrams related to the labs.

[[_TOC_]]



UML tools
---------------------------

When you draw UML diagrams you can draw them on a paper with a pen. That is fine. You can also use a diagram editor to draw them. Here follows two tools that you could use, both are free.

* [Dia Diagram Editor](http://dia-installer.de/) desktop
* [Draw.io](https://app.diagrams.net/) cloud

Dia is a desktop tool that you install on your computer and saves the drawings locally. Draw.io is a cloud service where you can share and cooperate on the diagram and save it in the cloud like Dropbox, Google drive or One drive.



Lab 2 as UML class diagram
---------------------------

In the file [`dice_inheritance.dia`](dice_inheritance.dia), which can be opened using Dia, you will find one (out of many) possible solution to the lab 2.



Lab 3 as UML class diagram
---------------------------

In the file [`dice_interface.dia`](dice_interface.dia), which can be opened using Dia, you will find one (out of many) possible solution to the lab 3.

