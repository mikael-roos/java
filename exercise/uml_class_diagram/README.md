---
revision:
    "2023-05-05": "(B, mos) Worked through and improved."
    "2022-10-05": "(A, mos) First release."
---

![logo](.img/uml-class-diagram.png)

UML class diagrams and tools
===========================

This exercise contains material on how to draw a class diagram using UML with classes, composition, inheritance and interfaces.

[[_TOC_]]

<!--
TODO

* Add PlantUML and include vscode extension

-->

UML tools
---------------------------

When you draw UML diagrams you can draw them on a paper with a pen. That is fine. You can also use a diagram editor to draw them. Here follows two tools that you could use, both are free and works on "all" platforms.

* [Dia Diagram Editor](http://dia-installer.de/) desktop
* [Draw.io](https://app.diagrams.net/) cloud
* [PlantText - The expert's design tool](https://www.planttext.com/)

Dia is a desktop tool that you install on your computer and saves the drawings locally. Draw.io is a cloud service where you can share and cooperate on the diagram and save it in the cloud like Dropbox, Google drive or One drive. PlantText is a tool where you write text that is converted into diagrams.



UML diagrams
---------------------------

UML contains a large set of various diagrams to model and represent a program or system architecture.

![UML overview](.img/UML_diagrams_overview.svg)

*Image from [Wikipedia on UML](https://en.wikipedia.org/wiki/Unified_Modeling_Language)*

The class diagram is a diagram showing the structure of the system.

You might need more or less diagrams to represent the system, it depends on the size and complexity of the system. A class diagram is usually a good place to start.



Notation for drawing UML diagrams
---------------------------

The notation for drawing the diagrams might be different depending on the choice of tool. Here follows some basic constructs showing how the different parts of the diagram can be drawn. You can read more in details in [Wikipedia on UML class diagram](https://en.wikipedia.org/wiki/Class_diagram).

We start with a class that has properties and methods. The `+` and `-` states the visibility of the member.

![class](.img/class.png)

An inheritance can look like this, with an arrow pointing from the extending class to the base class.

![inheritance](.img/inheritance.png)

When a class is containing objects from another class, that is called "uses" or composition where a class is composed, or contains, other classes.

A blank romb says "aggregation" and a filled romb says "composition". The difference is that aggregation is between classes that can exists on their own and composition is when then objects can not exist on their own.

* Aggregation (blank romb), `Dice` can exists even when they are not in the `DiceHand`.
* composition (filled romb), the `Room` of a `House` can not exists on their own, they need a relation with the house.

The following example says that the `Dice` can exists on their own, but the `DiceHand` can only exists when it is in a `Player`. It is up to the modeller to decide what is right and wrong.

![composition](.img/composition.png)

A interface provides a contract for what needs to be implemented, without saying how, to the class that implements the interface.

![interface](.img/interface.png)

A class can depend or temporary use another class, this can be modelled as a dependency.

![dependency](.img/dependency.png)

<!--
You can see the [details from above example](.img) using the drawio application.
-->



Draw UML for Yatzy
---------------------------

The idea is to model a Yatzy game where a `Player` can hold several `Dice` in the `Hand` and roll them like a round of Yatzy. There are different `Dice` like  a `DiceGraphic` and a `DiceCheating`. The player can also be a `PlayerComputer` which holds some level of `Intelligence` for playing the game.

It would be nice to know all the rules of Yatzy before proceeding, but it is enough to now the structure of the objects needed to carry out the game. A class diagram is a static diagram showing the classes and interfaces with their relations for implement (interface), extend (inheritance) and uses (composition).

This is the basics to draw.

* Dice class, roll it and show its value.
* DiceGraphic class, roll it and show its value with a graphic representation. The DiceGraphic shall extend Dice.
* DiceGraphicCheating class that has a method setRoll(int value) that can be used to set the value of the dice.
* DiceHand that can hold dices of any type of Dice.
* DicePlayer that holds a DiceHand with 5 dices where at least one dice should a a cheating dice.
* DiceGame where the DicePlayer can roll the dices three times. The player can select which dices to save and which dices to re-roll.
* Extend the DicePlayer to a DicePlayerComputer that automatically rolls a three set round, including the ability to have one cheating dice.
    * The DicePlayerComputer should have some level of Intelligence while playing the game.
* Extend the DicePlayer to a DicePlayerMonkey that plays the game totally random without any intelligence.



Draw the UML diagram without using interfaces
---------------------------

Draw the first UML class diagram without using interfaces.

In the file [`dice_inheritance.dia`](dice_inheritance.dia), which can be opened using Dia, you will find one (out of many) possible solution.

You can view a [similair solution](https://drive.google.com/file/d/1e8wCLzzyouMZVDntOMA6C36nOKZ_tPpP/view?usp=sharing) in the web tool drawio. 

You can see an [image representation of the same class diagram](.img/uml-with-inheritance-and-composition.png).



Draw the UML diagram using interfaces
---------------------------

Draw another UML class diagram using interfaces, or just update your first diagram with the concept of interfaces.

In the file [`dice_interface.dia`](dice_interface.dia), which can be opened using Dia, you will find one (out of many) possible solutions.

You can view a [similair solution](https://drive.google.com/file/d/1zvBgfXzR9QTLcmHGPL_XnraYwmJzWl4N/view?usp=sharing) in the web tool drawio. 

You can see an [image representation of the same class diagram](.img/uml-with-interface.png).



References
---------------------------

Read more to learn more.

Wikipedia

* [Unified Modeling Language](https://en.wikipedia.org/wiki/Unified_Modeling_Language)
* [Class diagram](https://en.wikipedia.org/wiki/Class_diagram)

Tools

* [Dia Diagram Editor](http://dia-installer.de/) desktop
* [Draw.io](https://app.diagrams.net/) cloud
