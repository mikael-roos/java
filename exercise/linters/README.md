---
revision:
    "2024-09-30": "(PB1, mos) Rework and update, as an alternative for SonarCube exercise."
    "2022-11-01": "(A, mos) First release."
---
Static code analysis integrated with Gradle
===========================

This is how to get going with static code analysis and integrate tools with Gradle. The tools we will work with are a selection of various linters, mess detectors and tools for code formatting.

* Checkstyle
* PMD
* SonarLint
* Prettier

[[_TOC_]]

<!--
TODO

* Add Sonarcybe/lint both as linter with gradle and as plugin vscode
* Add checkstyle as plugin vscode
* Add PMD as plugin vscode
* Add Prettier

* Mention tools like SonarCube and SonarCloud.

-->


Read up on the tools
---------------------------

[Checkstyle](https://checkstyle.sourceforge.io/) is a tool helping you to follow a code standard in Java. You can configure what code standard to follow and the tool can both check and partly fix your code accordingly. Checkstyle is supported out of the box with Gradle.

[PMD](https://pmd.github.io/) is a cross language static code analyser which can find troublesome parts in your code. It is a "mess detector" following a ruleset and warns when your code does not obey the ruleset. It also includes a "copy-paste-detector" (CPD) which warns you when it finds code that is duplicated.

Using [Gradle](https://docs.gradle.org/) we can intergrate both Checkstyle and PMD as tools to build a quality assurance suite for our code. Togeterh with the unit tests (and other tools) we can use this to show other that our code is adhearing to common best practice.



Example code saved in `sample/linters`
---------------------------

There is a Gradle project used in this exercise in [the repository in the directory `sample/linters`](https://gitlab.com/mikael-roos/java/-/tree/main/sample/linters). The code builds upon the [Sonarcube code sample](https://gitlab.com/mikael-roos/java/-/tree/main/sample/sonarcube).

Use the code sample to verify how the Gradle configuration file can look like and verify how it looks like when the tools execute.



Checkstyle
---------------------------

Gradle comes with a [Checkstyle plugin](https://docs.gradle.org/current/userguide/checkstyle_plugin.html) and all you need to do is to activate it like this in your `build.gradle`.

```json
plugins {
    id 'checkstyle'
}
```

You can now configure what version of Checkstyle to use. YOu might want to check what latest versions are available for [Checkstyle in the Maven package repository](https://mvnrepository.com/artifact/com.puppycrawl.tools/checkstyle).

```json
checkstyle {
  toolVersion = '10.4'
}
```

You will need a configuration file that tells Checkstyle what code to check and how to check it. It is a large set of rules. Here is a way to get the Checkstyle configuration that checks the [Google coding conventions from Google Java Style](https://google.github.io/styleguide/javaguide.html).

```
# Go to the root of the project dir
install -d config/checkstyle
curl -L --silent https://github.com/checkstyle/checkstyle/raw/master/src/main/resources/google_checks.xml > config/checkstyle/checkstyle.xml
ls -l config/checkstyle/checkstyle.xml
```

You can now execute the task. Any Checkstyle software needed will first be downloaded.

This will check the style for the project.

```
# Check all
./gradlew check

# Check only Main
./gradlew checkstyleMain

# Check only Test
./gradlew checkstyleTest
```

After the first run the result is cached and you need to clean the cache to rerun the checks.

<!-- Could this clean be done in a better manner? -->

```
./gradlew clean checkstyleMain
```

Once the report is generated it is available as HTML in the files located at `app/build/reports/checkstyle`, open that directory in your web browser to analyse the report.

![Checkstyle](img/checkstyle.png)



PMD
---------------------------

There is a [Gradle PHD Plugin](https://docs.gradle.org/current/userguide/pmd_plugin.html) that we can enable in the Gradle configuration like this.

```json
plugins {
    id 'pmd'
}
```

We can configure how we want the task to execute, what rules to use and what version to use like this.

```
pmd {
    //consoleOutput = true
    //toolVersion = "6.51.0"
    //rulesMinimumPriority = 5
    //ruleSets = ["category/java/errorprone.xml", "category/java/bestpractices.xml"]
}
```

All the above settings are commented out so I will procede without any specific configurations to the task. I will use the default settings.

The task is part of the `check` task and it can also be run as a separate task.

```
./gradlew pmdMain
./gradlew pmdTest
```

Once the report is generated it is available as HTML in the files located at `app/build/reports/pmd`, open that directory in your web browser to analyse the report.

![PMD](img/pmd.png)



Check
---------------------------

The task "check" now includes both Checkstyle and PMD. However, when PMD breaks it prevents the other tasks from executing so using `--continue` enables for all tasks to execute.

This is how you can make a clean run of the check task, even it some part fails.

```
./gradlew clean check --continue
```



Summary
---------------------------

These types of linters will help you perform quality assurance to your code and they can easily be integrated into a flow of continous integration and continous deployment to alwas check your code on any new commit pushed to the Git repo.
