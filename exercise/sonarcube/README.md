---
revision:
    "2022-09-29": "(A, mos) First release."
---
Static code analysis and software metrics with SonarCube
===========================

This is how to get going with Sonarcube to perform static code analysis and work with software metrics.

[[_TOC_]]

<!--
TODO
* xml jacoco integrate
* sonarlint vscode?
* Prova sonarcube hosted mot privat gitlab
* https://docs.sonarqube.org/latest/user-guide/metric-definitions/
* Bygg om övningen så att den enbart körs via docker, behöver inte visa hur man gör utan docker
-->


Read up on Sonarcube
---------------------------

[Sonarcube](https://www.sonarqube.org/) is a software/service that provides static code analysis of software and provide software metrics that will help you understand and work with your code.

The [SonarQube Documentation](https://docs.sonarqube.org/latest/) provides insights into the software metrics generated and helps you understand what to do with the information.

We are going to use the [SonarScanner for Gradle](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-gradle/) to integrate the generation of data with our gradle project setup.

We are going to use the free community version of Sonarcube and it can be [downloaded and installed from the Sonarcube website](https://www.sonarqube.org/downloads/).



Example code saved in `sample/sonarcube`
---------------------------

The Java code and the Gradle project used in this exercise is [stored in the repository in the directory `sample/sonarcube`](https://gitlab.com/mikael-roos/java/-/tree/main/sample/sonarcube).

Use it to compare your own code generated in this exercise.

You can also use the sample project to generate your first report in Sonarcube.



Get going
---------------------------

This is a two phase installation procedure, first we install the Sonarcube software locally which will be run by a webserver, then we configure gradle to generate data that will be consumed by the Sonarcube software.



### Installing SonarQube from the Docker image

If you are familiar with docker, then this alternative might be the easiest way to try out Sonarcube on your local machine.

Read on how to do it in the article "[Installing SonarQube from the Docker image](https://docs.sonarsource.com/sonarqube/10.5/setup-and-upgrade/install-the-server/installing-sonarqube-from-docker/)".



### Install Sonarcube software

Download the [community edition of Sonarcube](https://www.sonarqube.org/downloads/).

There is a [documentation on how to install it](https://docs.sonarqube.org/latest/setup/get-started-2-minutes/). There is a zipfile with the Sonarcube software, unzip it and start the server with the script depending on your operating system.

It can look like this in the terminal.

```
# Download the file
# Move it to a working directory of your choice
# Unzip the file
# Start the server

# On Windows
C:\sonarqube\bin\windows-x86-64\StartSonar.bat

# On Linux/Mac/Windows with WSL/Linux
sonarqube/bin/[OS]/sonar.sh console
```

Open a web browser to `http://localhost:9000` and login using admin:admin (then change the password to admin1 so you remember it).

When you have the Sonarcube server up and running you can create your first project.

Click "Manually" to work with a project you have locally on your own computer.

Set the project name to your choice, for example "Sonarcube".

![Create project](img/create-project.png)

You need to create a token which enable tha data from your gradle build to be analysed by the Sonarcube server.

![Provide token](img/provide-token.png)

Then you have the final details to be able to run your analysis.

![Run analysis](img/run-analysis.png)

Do save the section with how you are supposed to execute the gradle task, you will need it later. It looks like this.

```
./gradlew sonarqube \
  -Dsonar.projectKey=Sonarcube \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=sqp_56b9ebc458496c2be3d0c7c1b6121824e05e3b9a
```

The next section will guide you through to run the first analysis.



### Configure the Gradle project with Sonarcube

To get going you can use an existing Gradle project where you have a solid code base together with unit tests and code coverage enabled. Feel free to use the `sample/sonarcube` before you integrate this with your own project.

You start by adding Sonarcube to the Gradle configuration file `app/build.gradle`.

```
plugins {
  id "org.sonarqube" version "3.4.0.2513"
}
```

You can then run the Sonarcube task, with the gradle command, to generate the analysis that can be displayed by the Sonarcube software. The exact command will be shown in the Sonarcube server, it could look like this.

```
./gradlew sonarqube \
  -Dsonar.projectKey=Sonarcube \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=sqp_0c915f923e0d6d78a3dd9237937301cbe6cdf2c2
```

The task will run, together with all unit tests and the code coverage report.

Reload the sonarcube web application to view the results. It could look something like this.

![Sonarcube results](img/first-run.png)



Integrate with your own project
---------------------------

You can now create another project in Sonarcube and this time you integrate it with your own code from your latest gradle project.

In the Sonarcube web application, find the part where you "Create new project -> Manually".

Add the plugin from above section into the `build.gradle` and save the gradlew command (so you do not forget it).

The run your first analyse of your own code and watch the result in Sonarcube.



Enable to analyse code coverage
---------------------------

The following needs to been updated in the configuration file `app/build.gradle` to enable integration with Sonarcube.

We have data from code coverage that is generated by Jacoco. We must however ensure that a XML report is generated.

```
jacocoTestReport {
    // tests are required to run before generating the report
    dependsOn test

    reports {
        xml.enabled true
    }
}
```

We should also define that the code coverage is always generated before for the sonarcube task is run.

```
project.tasks["sonarqube"].dependsOn "jacocoTestReport"
```

Both settings are made in the `build.gradle` file.

Try to make these edits in your own project, then Run the report and reload the results, it can look like this.

![code coverage report](img/code-coverage.png)

If you need to troubleshoot you can clean upp your build with `./gradlew clean` before you run the Sonarcube taks, all data including the code coverage should be possible to view in Sonarcube. 



Walk through the data
---------------------------

Now you have a large set of static code analyse and software metrics of your project, use the Sonarcube web application to review your code and see what Sonarcube thinks about it.

To learn more on each metric you should rely on the [Sonarqube documentation](https://docs.sonarqube.org/latest/) which provides insights into the software metrics generated and helps you understand what to do with the information.



Note on other tools for static code analysis
---------------------------

Sonarcube provides a friendly user interface to static code analysis. There are also som other tools that might be useful when working with Hava code.

[PMD](https://pmd.github.io/) PMD is a source code analyzer. It finds common programming flaws like unused variables, empty catch blocks, unnecessary object creation, and so forth. [Gradle has a PMD plugin](https://docs.gradle.org/current/userguide/pmd_plugin.html).

[Checkstyle](https://checkstyle.org/checks.html) is a development tool to help programmers write Java code that adheres to a coding standard. It automates the process of checking Java code to spare humans of this boring (but important) task. This makes it ideal for projects that want to enforce a coding standard.. [Gradle has a Checkstyle plugin](https://docs.gradle.org/current/userguide/checkstyle_plugin.html).

Both these tools can be integrated with gradle tasks to be run as a quality assurance and help tools to aid in writing clean code.



Note on automatic testing
---------------------------

The workflow can then be enhanced by integrating a pipeline in GitLab (or GitHub actions), or using an external build system, that automatically builds and analyses your code each time you push new commits to the Git repository.



Summary
---------------------------

That was all for now, you need to learn a bit on what Sonarcube tries to tell you and do try to add some fixes that Sonarcube suggests.

