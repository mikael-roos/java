---
revision:
    "2024-12-16": "(A, mos) First version."
---
Java programming: Create, print and save a JSON data structure from an object
==========================

This is an example on how to work with JSON data format to convert a Java object to JSON and how to print it and how to save it to file.

To work with JSON we use the external library `com.google.code.gson`.

[[_TOC_]]


<!--
TODO

* Add UML class diagram
* Read JSON from file and convert into java objects
* How to read a JSON structure and populate Java objects.
    * json read through constructor
    * Json read through beans?

-->

Precondition
--------------------------

You should have worked through the exercise "[Java programming: File and stream management](./README.md)".

You should be aware of the basics of the [JSON data format](https://www.json.org/j).

You should be aware of the basics of the [Maven repository and the GSON library](https://mvnrepository.com/artifact/com.google.code.gson/gson).



Example code
--------------------------

The example code for this exercise is available in [`src-json/`](./src-json/).

The example code lives in a gradle project in the namespace `se.company.team`.



Gradle project
--------------------------

The example code lives in a Gradle project.

The configuration file `app/build.gradle` is updated to contain the following.

First to allow input from the terminal.

```java
tasks.named('run') {
    standardInput = System.in
}
```

Then to include the library GSON.

```java
dependencies {
    // https://mvnrepository.com/artifact/com.google.code.gson/gson
    implementation 'com.google.code.gson:gson:2.11.0'
}
```



Example: Team and Person
--------------------------

In this example we have a Team that consists of Person objects. We can add persons to the team and print them out. We can also print the team as a JSON data structure and we can save the team as a JSON data structure to a file.

Lets take the example step by step.



Class Team
--------------------------

First we have the Team class that holds all persons in a collection.

```java
package se.company.team;

import java.util.ArrayList;
import java.util.List;

/**
 * A team of persons.
 */
public class Team {
    private List<Person> members;

    public Team() {
        members = new ArrayList<>();
    }

    public void addMember(Person person) {
        members.add(person);
    }

    public List<Person> getMembers() {
        return members;
    }

    @Override
    public String toString() {
        return "Team:\n  " + members;
    }
}
```

The class is an ordinary class that manages a team.



Class Person
--------------------------

Then we look at the Person class that keeps data of a person.

```java
package se.company.team;

/**
 * A Person with details.
 */
public class Person {
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + '}';
    }
}
```

Like the Team class, this is an ordinary class holding details on a Person.



Main program with menu
--------------------------

To tigh together the code the example program contains a Main with a menu looking like this.

```
$ ./gradlew run --console plain -q
 -----------------
| 1) Create an empty team
| 2) Add a random person to the team
| 3) Print the team
| 4) Print team as JSON
| 5) Save team as JSON
| m) Print menu
| q) Quit
 -----------------
Enter your choice: 
```

The idea is to add an empty team, then add some persons and check that the team can be printed. It might look like this when you execute the example program.

```
 -----------------
| 1) Create an empty team
| 2) Add a random person to the team
| 3) Print the team
 -----------------
Enter your choice: 1
Enter your choice: 2
Adding person Person{name='Dave', age=41} to the team.
Enter your choice: 2
Adding person Person{name='Bob', age=33} to the team.
Enter your choice: 3
Team:
  [Person{name='Dave', age=41}, Person{name='Bob', age=33}]
```

Now it is time to print the team as JSON.



Print object as JSON data structure
--------------------------

To convert an object to a JSON data structure we will use the GSON library and code looking like this.

```java
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Print team as JSON data structure.
 */
private static void printTeamAsJson () {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String json;

    json = gson.toJson(team);
    System.out.println("JSON: " + json);
}
```

The code uses a `GsonBuilder` with options to create a `gson` object that can convert a java object to a JSON data structure.

It is the statement `json = gson.toJson(team)` that performs the conversion.

The `team` variable is a private member of the class, so it is not visible in this code example, but it is an instance of the Team class.

When printing the resulting string it might look like this.

```json
 -----------------
| 3) Print the team
| 4) Print team as JSON
 -----------------
Enter your choice: 3
Team:
  [Person{name='Dave', age=41}, Person{name='Bob', age=33}]
Enter your choice: 4
JSON: {
  "members": [
    {
      "name": "Dave",
      "age": 41
    },
    {
      "name": "Bob",
      "age": 33
    }
  ]
}
```

You can review the output above to see how the JSON object is formatted where `[]` is an array and `{}` is a key-value object.



Save JSON data structure to file
--------------------------

A JSON data structure is just a string so it is rather easy to save it to a file.

```java
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Save team as JSON data structure to file.
 */
private static void saveTeamAsJson () {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String json = gson.toJson(team);
    Path path = Path.of("team.json");

    try {
        Files.writeString(path, json);
        System.out.println("JSON data is written to 'team.json'.");
    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

We create the JSON string representation of the team object, just like before. Then we save it to the file `team.json`.

The `team` variable is a private member of the class, so it is not visible in this code example, but it is an instance of the Team class.

To save the file we first create a path to the file using `Path path = Path.of("team.json")` and the we write the JSON string to a file on that path using `Files.writeString(path, json)`.

We can read the content from the file, or open it in a text editor, and it can look like this.

```json
{
  "members": [
    {
      "name": "Cora",
      "age": 34
    },
    {
      "name": "Bob",
      "age": 24
    }
  ]
}
```



Read JSON data structure from file
--------------------------

We can also read the JSON data from file and use it to create a Java object from it. But let leave that for another exercise.

The JSON data format is often used for configuration files where you can edit the file by using an ordinary text editor and it is easy to read into an application.

The JSON data format is also used in machine to machine commuication where two applications exchange data.



Summary
--------------------------

You have worked through an example where you work with JSON data format and how it relates to a Java object.
