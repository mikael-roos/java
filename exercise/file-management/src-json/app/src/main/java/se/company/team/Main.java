package se.company.team;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Example Main with menut to show how to serialize objects to and from files.
 */
public class Main {
    private static Scanner scanner;
    private static String menuText = """
        -----------------
       | 1) Create an empty team
       | 2) Add a random person to the team
       | 3) Print the team
       | 4) Print team as JSON
       | 5) Save team as JSON
       | m) Print menu
       | q) Quit
        -----------------""";
        private static Team team;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        String choice; 

        System.out.println(menuText);
        do {
            choice = getChoice();
            switch (choice) {
                case "1": team = new Team(); break;
                case "2": createPersonAndAddToTeam(); break;
                case "3": printTeam(); break;
                case "4": printTeamAsJson(); break;
                case "5": saveTeamAsJson(); break;
                case "m": System.out.println(menuText); break;
                case "q": break;
                default:
                    System.err.println("Bad menu choice, use 'm' to get the menu.");
            }
        } while (!choice.equals("q"));
    }

    /**
     * Get the menu choice from the user input.
     * @return String as user input.
     */
    private static String getChoice() {
        System.out.print("Enter your choice: ");
        return scanner.nextLine();
    }

    /**
     * Add a Person to the Team.
     */
    private static void createPersonAndAddToTeam () {
        String[] names = {"Alice", "Bob", "Cora", "Dave"};
        Person person = new Person();
        int rand = (int) (Math.random() * names.length);
        
        person.setName(names[rand]);
        person.setAge(20 + (int) (Math.random() * 40));

        System.out.println("Adding person " + person + " to the team.");
        team.addMember(person);
    }

    /**
     * Print all persons in the team.
     */
    private static void printTeam () {
        System.out.println(team);
    }

    /**
     * Print team as JSON data structure.
     */
    private static void printTeamAsJson () {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json;
    
        json = gson.toJson(team);
        System.out.println("JSON: " + json);
    }

    /**
     * Save team as JSON data structure to file.
     */
    private static void saveTeamAsJson () {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(team);
        Path path = Path.of("team.json");
    
        try {
            Files.writeString(path, json);
            System.out.println("JSON data is written to 'team.json'.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
