package se.company.team;

import java.util.ArrayList;
import java.util.List;

/**
 * A team of persons.
 */
public class Team {
    private List<Person> members;

    public Team() {
        members = new ArrayList<>();
    }

    public void addMember(Person person) {
        members.add(person);
    }

    public List<Person> getMembers() {
        return members;
    }

    @Override
    public String toString() {
        return "Team:\n  " + members;
    }
}
