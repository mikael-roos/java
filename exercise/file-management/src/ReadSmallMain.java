/**
 * Show how a path can be constructed to point out a file resources.
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadSmallMain {
    
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("data/file.txt");
        System.out.println(path);

        // Read file content
        List<String> rows = Files.readAllLines(path);
        System.out.println("The file contains " + rows.size() + " rows:");
        rows.forEach(row -> System.out.println(row));

        // Write file content
        Path path1 = Paths.get("data/file_backup.txt");
        Files.write(path1, rows);
        System.out.println("---\nWrote a backup file as: " + path1);
    }
}
