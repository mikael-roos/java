/**
 * Show how a path can be constructed to point out a file resources.
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CsvMain {
    
    public static void main(String[] args) 
        throws FileNotFoundException, IOException
    {
        Path path = Paths.get("data", "persons.csv");
        List<Person> persons = new ArrayList<>();

        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");
                persons.add(new Person(values[0], values[1], Integer.parseInt(values[2])));
            }

            persons.forEach(person -> System.out.println(person));
        }

        // Write to a csv file
        Path path1 = Paths.get("data", "persons2.csv");
        persons.add(new Person("New", "Person", 1337));
        try (BufferedWriter writer = Files.newBufferedWriter(path1)) {
            for (Person person: persons) {
                writer.write(person.asCsv());
            }
        }
    }
}


class Person {
    public String firstName;
    public String lastName;
    public int age;

    Person (String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String asCsv() {
        return firstName + "," + lastName + "," + age + "\n";
    }

    public String toString() {
        return firstName + " " + lastName + " (" + age + ")";
    }
}