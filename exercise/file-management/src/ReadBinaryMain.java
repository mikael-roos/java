/**
 * Show how a path can be constructed to point out a file resources.
 */
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReadBinaryMain {
    
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("data/glider.png");
        System.out.println(path);

        // Read file content
        byte[] buf =  Files.readAllBytes(path);
        for (int i = 0; i < 10; i++) {
            System.out.print(String.format("%02d ", i));
            System.out.print(String.format("%02x ", buf[i]));
            if (isPrintableChar((char) buf[i])) {
                System.out.println((char) buf[i]);
            } else {
                System.out.println();
            }
        }

        // try (
        //     InputStream in = Files.newInputStream(path);
        //     BufferedReader reader = new BufferedReader(new InputStreamReader(in))
        // ) {
        //     byte[] buf = new byte[10];
        //     reader.read(buf, 0, 10);
        //     for (int i = 0; i < 10; i++) {
        //         System.out.print(String.format("%02d ", i));
        //         System.out.print(String.format("%02x ", buf[i]));
        //         if (isPrintableChar((char) buf[i])) {
        //             //System.out.println(buf[i]);
        //             System.out.println();
        //         } else {
        //             System.out.println();
        //         }
        //     }
        // } catch (IOException ex) {
        //     System.err.println(ex);
        // }

        
        // List<String> rows = Files.readAllLines(path);
        // System.out.println("The file contains " + rows.size() + " rows:");
        // rows.forEach(row -> System.out.println(row));

        // // Write file content
        // Path path1 = Paths.get("data/file_backup.txt");
        // Files.write(path1, rows);
        // System.out.println("---\nWrote a backup file as: " + path1);
    }

    static public boolean isPrintableChar( char c ) {
        Character.UnicodeBlock block = Character.UnicodeBlock.of( c );
        return (!Character.isISOControl(c)) &&
                c != KeyEvent.CHAR_UNDEFINED &&
                block != null &&
                block != Character.UnicodeBlock.SPECIALS;
    }
}
