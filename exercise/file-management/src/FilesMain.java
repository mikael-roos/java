/**
 * Show how a path can be constructed to point out a file resources.
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FilesMain {
    
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Add an argument to a single file or a dir.");
            System.exit(2);
        }

        Path path = Paths.get(args[0]);
        System.out.println(path);

        if (Files.isRegularFile(path)) {
            System.out.println("This is a regular file.");
        } else if (Files.isDirectory(path)) {
            System.out.println("This is a directory.");
        }

        if (Files.isReadable(path)) {
            System.out.println("The file/dir is readable.");
        }
        if (Files.isWritable(path)) {
            System.out.println("The file/dir is writable.");
        }
        if (Files.isExecutable(path)) {
            System.out.println("The file/dir is executable.");
        }

        // Iterate over items in a directory
        if (Files.isDirectory(path)) {
            try (var paths = Files.newDirectoryStream(path)) {
                paths.forEach(entry -> System.out.println(entry));
            }
        }
    }
}
