/**
 * Show how a path can be constructed to point out a file resources.
 */
import java.io.Console;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathMain {
    
    public static void main(String[] args) {
        // Windows paths
        //Path abs = Paths.get("C:\\files\\file.txt");
        //Path rel = Paths.get("data\\file.txt");

        // Unix, mac paths
        Path abs = Paths.get("/home/mos/files/file.txt");
        Path rel = Paths.get("data/file.txt");

        // Get the current relative path and convert to an absolute path
        Path cur = Paths.get("").toAbsolutePath();

        Path[] paths = {abs, rel, cur};
        for (Path path : paths) {
            System.out.format("toString: %s%n", path.toString());
            System.out.format("getFileName: %s%n", path.getFileName());
            System.out.format("getName(0): %s%n", path.getName(0));
            System.out.format("getNameCount: %d%n", path.getNameCount());
            System.out.format("subpath(0,2): %s%n", path.subpath(0,2));
            System.out.format("getParent: %s%n", path.getParent());
            System.out.format("getRoot: %s%n", path.getRoot());
        }

        Path path = Paths.get("data", "file.txt");
        System.out.println(File.separator);
        System.out.println(path);

        // Combine the current base path with a relative path
        Path file = Paths.get("data", "file.txt").toAbsolutePath();
        System.out.println(file);
    }
}
