/**
 * Show how a path can be constructed to point out a file resources.
 */
import java.util.Scanner;

public class ScannerMain {
    
    public static void main(String[] args) {
        Scanner in1 = new Scanner(System.in);
        String choice1 = in1.nextLine();

        System.out.println("You entered: " + choice1);
        //in1.close();

        try (
            Scanner in = new Scanner(System.in)
        ) {
            String choice = in.nextLine();
            System.out.println("You entered: " + choice);
        }
    }
}
