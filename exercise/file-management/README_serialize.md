---
revision:
    "2024-12-16": "(A, mos) First version."
---
Java programming: Save and read a serialized object to file
==========================

This is an example on how to work with serialized objects and how to write and read a serialized object to and from a file.

[[_TOC_]]


<!--
TODO

* Add UML class diagram

-->

Precondition
--------------------------

You should have worked through the exercise "[Java programming: File and stream management](./README.md)".



Example code
--------------------------

The example code for this exercise is available in [`src-serialize/`](./src-serialize/).



Example: Team and Person
--------------------------

In this example we have a Team that consists of Person objects. We can add persons to the team and print them out. We can also save the whole team to file as a serialized object. Then we can read the serialized object in the file and populate the team with the data from the file.

Lets take the example step by step.



Class Team
--------------------------

First we have the Team class that holds all persons in a collection.

```java
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A team of persons and the team can be serialized into a file.
 */
public class Team implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<Person> members;

    public Team() {
        members = new ArrayList<>();
    }

    public void addMember(Person person) {
        members.add(person);
    }

    public List<Person> getMembers() {
        return members;
    }

    @Override
    public String toString() {
        return "Team:\n  " + members;
    }
}
```

To make this class serializable we make it `implement Serializable` to "mark it" as a class that can be serialized. That interface is a "marker interface" and it does not contain any methods that we need to implement. It just states that this class is possible to serialize.

The property `serialVersionUID` is a unique identifier used during Java object serialization and deserialization to ensure compatibility between the sender's and receiver's classes. It is not strictly needed, but it helps keeping different versions of classes and serialized objects so they match.

Besides these, the class is an ordinary class that manages a team.



Class Person
--------------------------

Then we look at the Person class that keeps data of a person.

```java
import java.io.Serializable;

/**
 * A Person that is serializable.
 */
public class Person implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + '}';
    }
}
```

Like the Team class, we have the same setup with the Person class.

```java
import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 1L;
```

Besides this we do not need to add any other to make the classes serializable.



Main program with menu
--------------------------

To tigh together the code the example program contains a Main with a menu looking like this.

```
$ javac *.java && java Main
 -----------------
| 1) Create an empty team
| 2) Add a random person to the team
| 3) Print the team
| 4) Save team to serialised file
| 5) Load team from serialised file
| m) Print menu
| q) Quit
 -----------------
Enter your choice: 
```

The idea is to add an empty team, then add some persons and check that the team can be printed. It might look like this when you execute the example program.

```
 -----------------
| 1) Create an empty team
| 2) Add a random person to the team
| 3) Print the team
 -----------------
Enter your choice: 1
Enter your choice: 2
Adding person Person{name='Alice', age=54} to the team.
Enter your choice: 2
Adding person Person{name='Bob', age=55} to the team.
Enter your choice: 3
Team:
  [Person{name='Alice', age=54}, Person{name='Bob', age=55}]
```

Now it is time to save the team to file.



Save serialized team to file
--------------------------

To actually save the team, with its persons, to file, we need code looking like this.

```java
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Save team to a file using serialisation.
 */
private static void saveTeam () {
    try (ObjectOutputStream out = new ObjectOutputStream(
        new FileOutputStream("team.ser")
    )) {
        out.writeObject(team);
        System.out.println("Team is written to file 'team.ser'.");
    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

The code creates a `out` stream that is an `ObjectOutputStream` that uses a `FileOutputStream` to write the serialized object to a file.

The `team` variable is a private member of the class, so it is not visible in this code example, but it is an instance of the Team class.

The method `out.writeObject(team)` first serializes the object and then writes the serialized data to the file `team.ser`.

The file `team.ser` is a binary file so we can not fully view it in a text editor, but it looks something like this.

```
��srTeamLmemberstLjava/util/List;xpsrjava.util.ArrayListx����a�IsizexpwsrPersonIageLnametLjava/lang/String;xptBobsq~qsq~tDavesq~!qsq~8tAlicex
```

It is the full content of the team object, including the collection of person objects.



Read serialized team from file
--------------------------

We can now read the serialized file and populate an empty team object to contain the persons we previously stored in the file. To do so we need code looking like this.

```java
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Load team from a file using serialisation.
 */
private static void loadTeam () {
    try (ObjectInputStream in = new ObjectInputStream(
        new FileInputStream("team.ser")
    )) {
        team = (Team) in.readObject();
        System.out.println("Read team from the file 'team.ser'.");
    } catch (IOException | ClassNotFoundException e) {
        e.printStackTrace();
    }
}
```

We create a strem `in` to read from and the stram is an instance of `ObjectInputStream` that uses a `FileInputStream` to read from file.

The `team` variable is a private member of the class, so it is not visible in this code example, but it is an instance of the Team class.

The statement `team = (Team) in.readObject()` reads the serialized file and then tries to create an object from it. The result is casted to a `(Team)` so it gets the correct class.

Now you got a complete new team with persons, loaded from the saved file.



Summary
--------------------------

You have worked through a complete example where you save a serialized object to file and then read it to construct on object from it.
