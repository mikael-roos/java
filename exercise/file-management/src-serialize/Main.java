import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Scanner;

/**
 * Example Main with menut to show how to serialize objects to and from files.
 */
public class Main {
    private static Scanner scanner;
    private static String menuText = """
        -----------------
       | 1) Create an empty team
       | 2) Add a random person to the team
       | 3) Print the team
       | 4) Save team to serialised file
       | 5) Load team from serialised file 
       | m) Print menu
       | q) Quit
        -----------------""";
        private static Team team;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        String choice; 

        System.out.println(menuText);
        do {
            choice = getChoice();
            switch (choice) {
                case "1": team = new Team(); break;
                case "2": createPersonAndAddToTeam(); break;
                case "3": printTeam(); break;
                case "4": saveTeam(); break;
                case "5": loadTeam(); break;
                case "m": System.out.println(menuText); break;
                case "q": break;
                default:
                    System.err.println("Bad menu choice, use 'm' to get the menu.");
            }
        } while (!choice.equals("q"));
    }

    /**
     * Get the menu choice from the user input.
     * @return String as user input.
     */
    private static String getChoice() {
        System.out.print("Enter your choice: ");
        return scanner.nextLine();
    }

    /**
     * Add a Person to the Team.
     */
    private static void createPersonAndAddToTeam () {
        String[] names = {"Alice", "Bob", "Cora", "Dave"};
        Person person = new Person();
        int rand = (int) (Math.random() * names.length);
        
        person.setName(names[rand]);
        person.setAge(20 + (int) (Math.random() * 40));

        System.out.println("Adding person " + person + " to the team.");
        team.addMember(person);
    }

    /**
     * Print all persons in the team.
     */
    private static void printTeam () {
        System.out.println(team);
    }

    /**
     * Save team to a file using serialisation.
     */
    private static void saveTeam () {
        try (ObjectOutputStream out = new ObjectOutputStream(
            new FileOutputStream("team.ser")
        )) {
            out.writeObject(team);
            System.out.println("Team is written to file 'team.ser'.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load team from a file using serialisation.
     */
    private static void loadTeam () {
        try (ObjectInputStream in = new ObjectInputStream(
            new FileInputStream("team.ser")
        )) {
            team = (Team) in.readObject();
            System.out.println("Read team from the file 'team.ser'.");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
