import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A team of persons and the team can be serialized into a file.
 */
public class Team implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<Person> members;

    public Team() {
        members = new ArrayList<>();
    }

    public void addMember(Person person) {
        members.add(person);
    }

    public List<Person> getMembers() {
        return members;
    }

    @Override
    public String toString() {
        return "Team:\n  " + members;
    }
}
