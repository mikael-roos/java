---
revision:
    "2024-12-16": "(B, mos) Minor updates."
    "2023-09-21": "(A, mos) For autumn 2023."
---
Java programming: File and stream management
==========================

This is to learn the basics on how to read and write to files and streams using Java. You can learn from examples, study them and copy the examples and run them on your local machine.

A file is a physical storage place on the host system. A stream can be explained as a "stream of bytes" or a pipeline where the data flows through.

In Java we will see that file management is implemented (mainly) through `java.io` and `java.nio.file`.

[[_TOC_]]


<!--
TODO

* Example on working with CSV and external module.

-->

About
--------------------------

The content in this exercise is based on the [The Java™ Tutorials Lesson: Basic I/O](https://docs.oracle.com/javase/tutorial/essential/io/) and you should use that article as a reference.



Example code
--------------------------

The example code for this exercise is available in [`src/`](./src/).



Reading and writing to the terminal
--------------------------

Reading from the terminal and writing to the terminal is actually an example on working with files and streams. The terminal uses the input stream `System.in` and the output stream `System.out`.

```java
Scanner in = new Scanner(System.in);
String choice = in.nextLine();

System.out.println("You entered: " + choice);
in.close();
```

The scanner opens the in stream and close it at the end of the program. If you forget to close an open resource you will usually get a warning.

An alternate way would be to use the `try` statement that ensures that the opened resource is closed even it something goes wrong.

```java
try (
    Scanner in = new Scanner(System.in)
) {
    String choice = in.nextLine();
    System.out.println("You entered: " + choice);
}
```

Using the try statement would be good practice when working with files and streams. The statement is also called try-with-resources.

The example program [`ScannerMain.java`](src/ScannerMain.java) shows how it can look like.



Path
--------------------------

To open a file you first need to have the path to the file. The path is the position where the file is stored in the file system. The path can be relative from the current working directory (where you started your program) or absolute starting with a `/`.

Here is a way to construct absolute and relative paths.

```java
// Windows paths
Path abs = Paths.get("C:\\files\\file.txt");
Path rel = Paths.get("data\\file.txt");

// Unix, mac paths
Path abs = Paths.get("/home/mos/files/file.txt");
Path rel = Paths.get("data/file.txt");
```

Sometimes you need to know the current path to where the program was started.

```java
// Get the current relative path
Path cur = Paths.get("");

// Convert it to an absolute path
cur.toAbsolutePath();

// Get the current relative path and convert to an absolute path
Path cur = Paths.get("").toAbsolutePath();
```

Each path can be dissected into its part using methods in the Path class.

```java
System.out.format("toString: %s%n", path.toString());
System.out.format("getFileName: %s%n", path.getFileName());
System.out.format("getName(0): %s%n", path.getName(0));
System.out.format("getNameCount: %d%n", path.getNameCount());
System.out.format("subpath(0,2): %s%n", path.subpath(0,2));
System.out.format("getParent: %s%n", path.getParent());
System.out.format("getRoot: %s%n", path.getRoot());
```

The output of a program displaying details of the path `/home/mos/files/file.txt` could look like this.

```text
toString: /home/mos/files/file.txt      
getFileName: file.txt                   
getName(0): home                        
getNameCount: 4                         
subpath(0,2): home/mos                  
getParent: /home/mos/files              
getRoot: /                              
```

If you are creating programs that run on different filesystems you could let Java decide what path separator to use. It is available like this.

```java
String separator = File.separator;
```

You can create a filepath in a compatible way like this.

```java
Path abs = Paths.get(File.separator, "home", "mos", "files", "file.txt");
Path rel = Paths.get("data", "file.txt");
```

Java will use the current filesystem separator to combine the path to a string.

Sometimes you want to have the current path and then add some relative path to it and even construct it as an absolute path. This is how that can be done.

```java
// Combine the current base path with a relative path
Path file = Paths.get("data", "file.txt").toAbsolutePath();
```

The example program [`PathMain.java`](src/PathMain.java) shows this example in full.



Files and directories
--------------------------

The Files class offers static methods for reading, writing, and manipulating files and directories. The methods operates on the file structure of the host system. It can be directories, files or links. The class consists of static methods that in general delegates the operation to the host system and the specific file system.

As the first example program we can create our own version of the `ls` or `dir` command that lists the content of the directory.

The program should take an argument and if it is a directory it should print out all files and directories in it, showing their names with details if the file or subdirectory is readable, writable or executable.

It can look like ths when the program is executed.

First with a file `FilesMain.java` as argument.

```bash
$ java FilesMain FilesMain.java
FilesMain.java                 
This is a regular file.        
The file/dir is readable.      
The file/dir is writable.      
```

Then with the current directory `.` as argument.

```bash
$ java FilesMain .            
.                             
This is a directory.          
The file/dir is readable.     
The file/dir is writable.     
The file/dir is executable.   
./PathMain.class              
./ScannerMain.class           
./PathMain.java               
./FilesMain.java              
./FilesMain.class             
./ScannerMain.java            
```

This is how the file operations can look like in the program.

First take the incoming argument and create a Path of it. Then check if it is a file or a directory.

```java
Path path = Paths.get(args[0]);
System.out.println(path);

if (Files.isRegularFile(path)) {
    System.out.println("This is a regular file.");
} else if (Files.isDirectory(path)) {
    System.out.println("This is a directory.");
}
```

Then check the file or directory if it is readable, writable or exacutable.

```java
if (Files.isReadable(path)) {
    System.out.println("The file/dir is readable.");
}
if (Files.isWritable(path)) {
    System.out.println("The file/dir is writable.");
}
if (Files.isExecutable(path)) {
    System.out.println("The file/dir is executable.");
}
```

Finally iterate over all items in the directory, if it is a directory.

```java
// Iterate over items in a directory
if (Files.isDirectory(path)) {
    try (var paths = Files.newDirectoryStream(path)) {
        paths.forEach(entry -> System.out.println(entry));
    }
}
```

The example program [`FilesMain.java`](src/FilesMain.java) shows how it can look like.

There are also methods to move, copy and remove files. You can review [what methods are available in the Files class in the javadoc](https://docs.oracle.com/en/java/javase/23/docs/api/java.base/java/nio/file/Files.html).



Reading and writing small files
--------------------------

When the file is small its content can be read from the file into the program without disturbance with a single method.

Here we use the method `Files.readAllLines()`.

```java
Path path = Paths.get("data", "file.txt");
System.out.println(path);

// Read file content as a list of strings
List<String> rows = Files.readAllLines(path);
System.out.println("The file contains " + rows.size() + " rows:");
rows.forEach(row -> System.out.println(row));
```

The file content is read into a list of strings. The method `readAllLines()` uses UTF8 as default character set when interpretating the characters. If you do not need character encoding, then you can read the file content byte by byte using `readAllBytes()`.

This is how to write the list of strings into a new file.

```java
// Write file content
Path path1 = Paths.get("data", "file_backup.txt");
Files.write(path1, rows);
```

The above code is working with text files, files that you can open and read visually in a text editor. If you have binary files you can use the same method but write byte buffers instead, byte buffers having bytes and not printable characters.

You need to deal with `IOException` when things go wrong. You can also check if the path is readable and writable before doing the action.

The example program [`ReadSmallMain.java`](src/ReadSmallMain.java) shows how it can look like.



Reading and writing larger files, piece by piece
--------------------------

When reading and writing larger files you usually want to read and write the file piece by piece to avoid that the process blocks or hangs without control. This can be done using streams of buffers. Here is an example on how to use buffers to read a larger file, buffer by buffer. This is also called channel I/O.

```java
Path path = Paths.get("data", "file.txt");

try (BufferedReader reader = Files.newBufferedReader(path)) {
    String line = null;
    while ((line = reader.readLine()) != null) {
        System.out.println(line);
    }
} catch (IOException x) {
    System.err.format("IOException: %s%n", x);
}
```

The `BufferedReader` reads text from a character-input stream, buffering characters to provide for the efficient reading of characters, arrays, and lines.

Here is an example on how to write to a text file using a `BufferedWriter` and channel I/O. The code writes one line at a time.

```java
// Write file content
Path path1 = Paths.get("data", "file_backup_again.txt");
try (BufferedWriter writer = Files.newBufferedWriter(path1)) {
    for (String row: rows) {
        writer.write(row, 0, row.length());
        writer.write("\n", 0, 1);
    }
} catch (IOException x) {
    System.err.format("IOException: %s%n", x);
}
```

In above example, the file content is stored as an ArrayList and each line is written and a `\n` is added to each line end.

The example program [`ReadBufferedMain.java`](src/ReadBufferedMain.java) shows how it can look like.



Read a binary file
--------------------------

A text file contains text that can be displayed to the user with characters and a character encoding. Binary files contains data that can not be displayed in that way. A binary file contains bytes. Images and videos are example of binary files.

When working with pure data, a binary file might be more suitable to work with.

The [PNG file format (image)](https://en.wikipedia.org/wiki/PNG) is an example of a binary file format. The first 8 bytes of the file contain data on the rest of the file. 

It can look like this when inspecting the file in the terminal.

```text
$ hexdump -C data/glider.png | head -1
00000000  89 50 4e 47 0d 0a 1a 0a  00 00 00 0d 49 48 44 52  |.PNG........IHDR| 
```

Lets write a java program that reads the first 10 bytes from the same PNG image. To keep it simple we read the whole file into memory using `Files.readAllBytes()` and then we print the first 10 bytes in the file.

```java
Path path = Paths.get("data", "glider.png");
System.out.println(path);

// Read file content
byte[] buf =  Files.readAllBytes(path);
for (int i = 0; i < 10; i++) {
    System.out.print(String.format("%02d ", i));
    System.out.print(String.format("%02x ", buf[i]));
    if (isPrintableChar((char) buf[i])) {
        System.out.println((char) buf[i]);
    } else {
        System.out.println();
    }
}
```

The function `isPrintableChar()` is a hand coded function to detect if the character is printable.

Running the program, it might look like this.

```text
$ java ReadBinaryMain 
data/glider.png                                    
00 89 ﾉ                                            
01 50 P                                            
02 4e N                                            
03 47 G                                            
04 0d                                              
05 0a                                              
06 1a                                              
07 0a                                              
08 00                                              
09 00                                              
```

We see that the command hexdump and our own program shows the same content from the file.

The example program [`ReadBinaryMain.java`](src/ReadBinaryMain.java) shows how it can look like.



Creating files
--------------------------

Writing to a file that does not exists also means that the file is created. There are also methods to create an empty file with no content.

```java
Path file = Paths.get("data", "create.txt");
try {
    // Create the empty file with default permissions, etc.
    Files.createFile(file);
} catch (FileAlreadyExistsException x) {
    System.err.format("file named %s" +
        " already exists%n", file);
} catch (IOException x) {
    // Some other sort of failure, such as permissions.
    System.err.format("createFile error: %s%n", x);
}
```

Sometimes it can be useful to create a temporary file that is unique, perhaps for intermediate storage or for some locking purpose, and then remove the file again.

```java
try {
    Path tempFile = Files.createTempFile(null, ".myapp");
    System.out.format("The temporary file" +
        " has been created: %s%n", tempFile);
    Files.delete(tempFile);
} catch (IOException x) {
    System.err.format("IOException: %s%n", x);
}
```

The place to store and naming of the temp file is depending on the host system.

The example program [`CreateMain.java`](src/CreateMain.java) shows how it can look like.



Read a CSV file
--------------------------

Excel and equal tools, including databases, can import and export the data using comma delimited text files, also known as the CSV file format (`data.csv`).

Imagen a CSV file containing the following data about persons.

```text
Mikael,Roos,42
John,Doe,35
Jane,Doe,35
Mumin,Troll,13
```

Lets create a java program that reads the file and instantiates Person objects and adds those into a list.

It can look like this.

```java
Path path = Paths.get("data", "persons.csv");
List<Person> persons = new ArrayList<>();

try (BufferedReader reader = Files.newBufferedReader(path)) {
    String line;
    while ((line = reader.readLine()) != null) {
        String[] values = line.split(",");
        persons.add(new Person(values[0], values[1], Integer.parseInt(values[2])));
    }

    persons.forEach(person -> System.out.println(person));
}
```

You can see this as a way to read data from a database or datasheets.

Writing a CSV file can be done in a similair manner. Consider the following code that adds a person and writes to a new csv file.

```java
Path path1 = Paths.get("data", "persons2.csv");
persons.add(new Person("New", "Person", 1337));
try (BufferedWriter writer = Files.newBufferedWriter(path1)) {
    for (Person person: persons) {
        writer.write(person.asCsv());
    }
}
```

This is how the method `Person.asCsv()` can be implemented in a rudimentary way.

```java
class Person {
    public String asCsv() {
        return firstName + "," + lastName + "," + age + "\n";
    }
}
```

The example program [`CsvMain.java`](src/CsvMain.java) shows how it can look like.

The implementation of CSV above is not foolproof, so you might consider using an external library like "[OpenCSV](https://mvnrepository.com/artifact/com.opencsv/opencsv)" to actually do this.

<!--
The example program [`json_csv/App.java`](../../sample/json_csv/app/src/main/java/json_csv/App.java) shows how it can look like when using the external module.
-->



Read a JSON file
--------------------------

Another way to work with data in text files is the JSON format. There is an external module [Gson](https://mvnrepository.com/artifact/com.google.code.gson/gson) which is a good alternative to work with JSON.

An advantage with textfiles like CSV and JSON is that they can be generated from another system, or edited by hand, and the read into a java program into a datastructure.

An example where this an be useful is when you have a configuration file that your program reads. The configuration file could be saved in JSON making it easy to edit by hand. When the java program starts up it can read the configuration file and create an object from it and use it in the program.

Another example would be to implement the CSV Person example using JSON files instead.
 
Here are the basics on how to write to write a collection of objects as a JSON string.

```java
Person[] persons = {
    new Person("Mikael", "Roos", 42),
    new Person("John/Jane", "Doe", 35),
    new Person("Mumin", "Troll", 13)
};

Gson gson = new GsonBuilder().setPrettyPrinting().create();
String json = gson.toJson(persons);

System.out.println("JSON: " + json);
```

The output from above code can look like this.

```json
JSON: [
  {
    "firstName": "Mikael",
    "lastName": "Roos",
    "age": 42
  },
  {
    "firstName": "John/Jane",
    "lastName": "Doe",
    "age": 35
  },
  {
    "firstName": "Mumin",
    "lastName": "Troll",
    "age": 13
  }
]
```

<!--
How to read a JSON structure and populate Java objects.
-->

The example program [`json_csv/App.java`](../../sample/json_csv/app/src/main/java/json_csv/App.java) shows how it can look like when using the external JSON module.



Serialize an object
--------------------------

An object that is serializable can take all its data, the values of its properties, and make them into a file that can be saved in a file or sent over the network. It is then a byte representation of the object.

Such representations can then be deserialized to populate an object again. This is away to store objects persistently, first serialize, then save to file. To recreate the object the file is read and the string is deserialised into an object again.

To make a class serializable it implements the interface.

```java
class Person implements Serializable {

}
```

Then it is all a matter of serializing and writing to a file using the following process.

```java
Path path = Paths.get("data", "serialize.data");
Person mik = new Person("Mikael", "Roos", 42);

// Serialize and write to file
try (
    FileOutputStream file = new FileOutputStream(path.toString());
    ObjectOutputStream out = new ObjectOutputStream(file);
) {  
    // Method for serialization of object into a file
    out.writeObject(mik);
    System.out.println("Object has been serialized in " + path);
} catch (IOException ex) {
    System.out.println("IOException is caught " + ex);
}
```

Review the code and you can find that the method call `out.writeObject(mik)` is where it all happens.

We can review the file content to see how the serialised object looks like.

```bash
$ hexdump data/serialize.data -C                                                
00000000  ac ed 00 05 73 72 00 06  50 65 72 73 6f 6e dc 80  |....sr..Person..|  
00000010  a4 67 82 96 ba d4 02 00  03 49 00 03 61 67 65 4c  |.g.......I..ageL|  
00000020  00 09 66 69 72 73 74 4e  61 6d 65 74 00 12 4c 6a  |..firstNamet..Lj|  
00000030  61 76 61 2f 6c 61 6e 67  2f 53 74 72 69 6e 67 3b  |ava/lang/String;|  
00000040  4c 00 08 6c 61 73 74 4e  61 6d 65 71 00 7e 00 01  |L..lastNameq.~..|  
00000050  78 70 00 00 00 2a 74 00  06 4d 69 6b 61 65 6c 74  |xp...*t..Mikaelt|  
00000060  00 04 52 6f 6f 73                                 |..Roos|            
00000066                                                                        
```

You might see a few details that you expect in the printout.

To deserialize it is a matter of open the file and assigning it to an object. The actual process of deserialization is happening behind the scenes.

```java
// Deserialization
Person doe = new Person();
try (
    FileInputStream file = new FileInputStream(path.toString());
    ObjectInputStream in = new ObjectInputStream(file);
) {
    // Method for deserialization of object from file
    doe = (Person) in.readObject();
    System.out.println("Object has been deserialized ");
    System.out.println(doe);
} catch(IOException ex) {
    System.out.println("IOException is caught " + ex);
} catch(ClassNotFoundException ex) {
    System.out.println("ClassNotFoundException is caught");
}
```

You might see that the magic happens in this line of code.

```java
doe = (Person) in.readObject();
```

Working with serialized objects is rather easy.

The example program [`SerializeMain.java`](src/SerializeMain.java) shows how it can look like.



Summary
--------------------------

You have worked through an introduction on how to work with files and streams.
