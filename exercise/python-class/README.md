---
revision:
    "2024-11-15": "(A, mos) Enhancing an existing example program."
---
A dice class in Python
==========================

This is a short code example that shows how to create a class in Python and how to use it.

[[_TOC_]]



Precondition
--------------------------

You have installed Python and you can execute Python programs.



Prepare
--------------------------

Create a working directory where you can save all your files. 

```bash
mkdir java
cd java

mkdir python-class
cd python-class
ls
```

Open your vscode to the directory.

```bash
code .
```

Now you are ready to work with this exercise.



Create the files and execute them in your terminal
--------------------------

The example consists of two files [`main.py`](./main.py) and [`dice.py`](./dice.py). Create those two files and add the source code into them.

Execute the program using your python intepretator in the terminal.

```bash
python3 main.py 
```

It will look like this when you execute the program.

```
Rolling the dice, it was a 2
Rolling the dice, it was a 4
You have rolled the dice 2 times.
The sum of all rolls where 6
The faces of the die are now: 32
Rolling the dice, it was a 1 using faces=32
```

Now you should study the source code to explore its parts to see if you can understand how the Dice class and the Dice object is used by the main program to crate the output.



Summary
--------------------------

You have now built and run your first Java program.
