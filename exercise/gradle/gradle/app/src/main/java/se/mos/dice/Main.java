/**
 * Example with main and extra classes in separate files, without package
 * naming.
 *
 */
package se.mos.dice;

public class Main {
    
    public static void main(String[] args) {
        Dice die = new Dice();

        int roll;
        roll = die.roll();
        System.out.println("The rolled die was: " + roll);
        System.out.println(die);
    }
}
