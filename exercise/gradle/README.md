---
revision:
    "2024-11-30": "(C, mos) Reworked and enhanced."
    "2023-08-09": "(B, mos) Walkthrough with minor edits."
    "2022-09-13": "(A, mos) First release."
---
Build your Java project with Gradle
===========================

This shows how to install and get going with Gradle to setup a new Java project structure and how to build the executable.

[[_TOC_]]

<!--
TODO

* F1 java clean.

-->



About Gradle
---------------------------

Gradle is a build and automation tool that we are going to use to compile and build our Java projects. We will also use the conventions and structures that Gradle impose on us. You can quickly scan through the article on "[What is Gradle?](https://docs.gradle.org/current/userguide/what_is_gradle.html)" to learn what Gradle is and does for our Java project.

We are going to use Gradle for "[Building Java & JVM projects](https://docs.gradle.org/current/userguide/building_java_projects.html#building_java_projects)".

In this guide we are going to create, build and run a Java project using Gradle and we will follow the steps from the article "[Building Java Applications Sample](https://docs.gradle.org/current/samples/sample_building_java_applications.html)". Ensure that you have that article available if you get into trouble or if you want to get more details on what files Gradle generates.



Install Gradle
---------------------------

You need to install Gradle and you can follow the instructions in "[Install Gradle](https://docs.gradle.org/current/userguide/installation.html)".

When you are done you should be able to execute the command like this.

```
gradle --version
```



Install Gradle extension to Visual Studio Code
---------------------------

[Visual Studio Code supports Gradle](https://code.visualstudio.com/docs/java/java-build#_gradle) through an extension. Do install the extension "[Gradle for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-gradle)". It will provide you with an overview of the different Gradle taks you may execute.



Create a Java project directory
---------------------------

We start by creating a new directory for our project and we move into it. For this exercise we label the directory `gradle`.

```
mkdir gradle
cd gradle
```

We can now use Gradle to initiate the directory structure.

```
gradle init
```

You will now get some choices, choose the following.

* Select type of build to generate: "Application"
* Select implementation language: "Java"
* Select Java version: (use default)
* Project name: (use default)
* Select application structure: "Single..."
* Select build script DSL: "Groovy"
* Select test framework: "JUnit Jupiter"
* For the rest use the default proposed value

Gradle will generate a directory structure looking like this.

```
.                                          
├── app                                    
│   ├── build.gradle                       
│   └── src                                
│       ├── main                           
│       │   ├── java                       
│       │   │   └── org                    
│       │   │       └── example            
│       │   │           └── App.java       
│       │   └── resources                  
│       └── test                           
│           ├── java                       
│           │   └── org                    
│           │       └── example            
│           │           └── AppTest.java   
│           └── resources                  
├── gradle                                 
│   ├── libs.versions.toml                 
│   └── wrapper                            
│       ├── gradle-wrapper.jar             
│       └── gradle-wrapper.properties      
├── gradle.properties                      
├── gradlew                                
├── gradlew.bat                            
└── settings.gradle                        
                                           
15 directories, 10 files                   
```

The Java file where we can start to write our code in is in `app/src/main/` and the "Hello World" is in the subdirectory `java/proj/App.java`. Feel free to edit it.

It is a rather deep directory structure, but it is such to support more complex projects.

Unit tests can be written into `app/src/test`.

Everything named "gradle" is part of the Gradle tool.

You can quickly review the following gradle files.

* `settings.gradle` provides details on what directories to include in the build.
* `app/build.gradle` a sample file to build a Java projects.

These two files are the ones most likely that you will need to modify.



Build and run the project
---------------------------

We can now build and run the program with the following command.

```
./gradlew run
```

It can look like this when it is done.

![Gradle](img/gradle.png)

_Figure. Using gradle in the command line to execute the application._

Try to run the command without an argument to learn more on the command and what it can do.

```
./gradlew
```

You can also execute the application though the Gradle extension in vscode, it can look like this.

![Gradle](img/gradle-vscode.png)

_Figure. Using gradle extension in vscode to execute the application._

Review the source code of the application in `App.java`, do some minor edit and build and run it again to verify that your changes where active.

Try running the application using the following command.

```
./gradlew run --console plain -q
```

You will then get less verbose output and it will look like this.

```
$ ./gradlew run --console plain -q
Hello World!
```



Clean the builds
---------------------------

You can clean up the build directory, or remove it, with the follwoing command.

```
./gradlew clean
```

That can be useful if you for some reason want to make a clean and fresh build of your application.



Configure project for System.in
---------------------------

If your project should read from System.in in the terminal, then you need to configure that in the `app/build.gradle` using this setting.

```
tasks.named('run') {
    standardInput = System.in
}
```

Add the following code to the `App.java` to verify that it works to read user input from the terminal.

```java
/**
 * Verify that application can read from terminal.
 */
public static void read () {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter your name:");
    String name = scanner.nextLine();
    System.out.println("Hello, " + name + "!");
    scanner.close();
}
```

Update the main method to call the above method and verify that it works.

It can look like this.

![Read system.in](img/gradle-system-in.png)

_Figure. Now it works to read from System.in in the terminal._



Generate Java Doc
---------------------------

Javadoc is the tool/system that helps you generate documentation for your Java code. Javadoc is included in your Gradle setup and you can generate it like this.

```
./gradlew javadoc
```

You might get som warnings saying that the code is not propely documented. We can ignore those for now, but try to have no warnings whan you use javadoc to document your own code.

You can now open a web browser and point it to the generated documentation in `app/build/docs/javadoc/index.html`. Traverse the documentation to see how the code is documented.

It can look like this.

![javadoc](img/javadoc-start.png)

Now you can generate your own javadoc for your application.

<!--
You will see that the links leading to Oracle javadoc does not work. You can fix that by adding following to the configuration file `build.gradle` and then rebuild your documentation.

```
javadoc {
    options {
        links 'https://docs.oracle.com/en/java/javase/23/docs/api/'
    }
}
```

Change the version 18 in the link to your choice, for example 8, 11 or 17.

-->



Example of javadoc comments
---------------------------

Here follows an example on how a class should look like when it is documented using javadoc comments.

```java
/**
 * A class that represents a car.
 * It includes properties like make, model, and speed, as well as methods to manipulate them.
 */
public class Car {

    private String make;
    private String model;
    private int speed;

    /**
     * Creates a new car with the specified make and model.
     *
     * @param make The make of the car.
     * @param model The model of the car.
     */
    public Car(String make, String model) {
        this.make = make;
        this.model = model;
        this.speed = 0; // The car starts stationary
    }

    /**
     * Retrieves the make of the car.
     *
     * @return The make of the car.
     */
    public String getMake() {
        return make;
    }

    /**
     * Retrieves the model of the car.
     *
     * @return The model of the car.
     */
    public String getModel() {
        return model;
    }

    /**
     * Increases the car's speed.
     *
     * @param increment The number of kilometers per hour to increase the speed by.
     */
    public void accelerate(int increment) {
        speed += increment;
    }

    /**
     * Retrieves the current speed of the car.
     *
     * @return The speed in kilometers per hour.
     */
    public int getSpeed() {
        return speed;
    }
}
```

The class-level comments should summarize the purpose of the class.

THe method-level comments should explain what the method does, its parameters (@param), and its return value (@return).



Include external jar from the Maven repository
---------------------------

The [Maven repository](https://mvnrepository.com/) holds different versions of external software libraries that can be downloaded and used in your application.



### Install Gson using Gradle configuration

This can easily be integrated into Gradle by configuration. For example, the library Gson can produce a [JSON](https://en.wikipedia.org/wiki/JSON) output from a Java class. If we would like to use it we could update the Gradle configuration in `src/build.gradle` and add the following dependency to include Gson in our project.

```
dependencies {
    // https://mvnrepository.com/artifact/com.google.code.gson/gson
    implementation 'com.google.code.gson:gson:2.11.0'
}
```

You can checkout how the [Maven page for Gson](https://mvnrepository.com/artifact/com.google.code.gson/gson) looks like. You can check if we are using the latest version, or not.

Now we can import the classes into our code. If could also be useful to get the [javadoc for the Gson](https://javadoc.io/doc/com.google.code.gson/gson/latest/com.google.gson/com/google/gson/Gson.html).



### Example to use Gson module

We can verify that we can import and use the Gson module by updating the `App.java` code.

First we need to import the packages. would look like this in the source code.

```
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
```

Then we add a function that takes an array and prints it out as a JSON string.

```
/**
 * Use the external module Gson for converting objects to JSON
 */
public static void json ( ) {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String hello[] = {"Hello", "World!"};
    String json;

    json = gson.toJson(hello);
    System.out.println("JSON: " + json);
}
```

Now update the main method and call the `json()` method.

It can look like this.

![JSON with Gson](img/gson.png)

_Figure. Excuting an example program using Gson to export a Java variable as a JSON string._

These type of online code repositories like Maven are quite important for a programming language since it provides a vital part of the infrastructure to publish, share, reuse and update software modules and packages.

If your vscode does not recognize the Gson class, then try to right-click with the mouse and reload the project (or restart vscode).



Move your own code into Gradle
---------------------------

You can now take your own project that uses a package structure and move it into the directory `app/src/main/java`.

Review this [code example](./gradle/app/src/main/java/) where the namespace `se.mos.dice` contains a Dice class and a Main program.

When the code is in place you need to update the configuration file `app/build.gradle` and set the main class you want to execute. You set this in the `application {}` setting of the configuration file.

```
application {
    // Define the main class for the application.
    //mainClass = 'org.example.App'
    //mainClass = 'Main'
    mainClass = 'se.mos.dice.Main'
}
```

Now you can run the main program in the class you defined as your mainClass.

```
$ ./gradlew run --console plain -q
The rolled die was: 5
The last roll of the dice was [5]
```

It can look like this.

![gradle dice main](img/gradle-dice-main.png)

_Figure. Running another main program by configuring its main class._

Now you should be ready to move your own code into a gradle project.



Summary
---------------------------

Carry on to get more aquainted with Gradle and its usage. It is now wise to write your applications in the Gradle structure.
