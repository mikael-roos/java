---
revision:
    "2024-11-29": "(C, mos) Use .equals() to compare strings."
    "2024-11-17": "(B, mos) Add uml class diagram."
    "2024-11-15": "(A, mos) Enhancing an existing example program."
---
![uml diagram](img/class-diagram.png)

A dice class in Java
==========================

This is an example on how to create a Dice class in Java and how to instantiate an object of it and the use it to create a dice game.

You will also pracice how to read from the terminal and how to write output to the terminal.

[[_TOC_]]

<!--
TODO

* Fixa graphic till static.

-->



Precondition
--------------------------

You have understanding of the basic programming constructs in Java like data types, conditions and iterations.



Prepare
--------------------------

Create a working directory where you can save all your files. 

```bash
cd java

mkdir dice-class
cd dice-class
ls
```

Open your vscode to the directory.

```bash
code .
```

Now you are ready to work with this exercise.



Create the main program and a Dice class with constructor
--------------------------

I will show you how to create the main program an you should be able to create the Dice class on your own. There will be a suggestion for a solution, if you need it.

First we create the main program in `Main.java`, it will be the classic main template.

```java
public class Main {
    public static void main(String[] args) {
        
    }
}
```

Then we continue to create an object, an instance, of the Dice class and we save it into a variable. We want the dice to have 6 sides.

```java
public class Main {
    public static void main(String[] args) {
        Dice die = new Dice(6);
    }
}
```

It is the class constructor that is called and in that we can initiate the object (instance).

You should now create the file `Dice.java` and create the smallest amount of code that makes it possible to compile and execute the code.

<details>
<summary>Compile and execute in Windows PowerShell terminal</summary>


If you are on an older version of PowerShell (pre 7), then you need to compile and execute like this.

```bash
javac *.java; java Main
```

On newer versions of PowerShell you can compile all Java files `*.java` and if it is successful execute the main program.

```bash
javac *.java && java Main
```

</details>

<details>
<summary>Compile and execute in Bash terminal</summary>

Compile all Java files `*.java` and if it is successful execute the main program.

```bash
javac *.java && java Main
```

</details>

<details>
<summary>Solution class Dice with constructor</summary>

This is how the class Dice can look with a constructor taking one argument and having an empty body.

Lets add a print statement in the empty constructor, to verify that it is called.

```java
public class Dice {
    public Dice (int sides) {
        System.out.println("Constructor called with argument: " + sides);
    }
}
```

</details>



Roll the dice
--------------------------

In the main program we want to be able to roll the dice to se its outcome.

```java
public class Main {
    public static void main(String[] args) {
        Dice die = new Dice(6);

        int roll1 = die.roll();
        int roll2 = die.roll();
        int sum = roll1 + roll2;
        System.out.print("You rolled: " + roll1 + " and " + roll2);
        System.out.println(". The sum is: " + sum);
    }
}
```

Now create the smallest amount of code in the Dice class that can suffice the main program and make the code executable. Remember to take care of the argument sent to the constructor and store that as a privae member of the class.

<details>
<summary>Solution roll the dice</summary>

We use the built-in random method to roll the dice. We need to store the sides of the dice as a private member so we know the max of the dice when rolling it.

```java
import java.util.Random;

public class Dice {
    private int sides;

    public Dice (int sides) {
        this.sides = sides;
    }

    public int roll () {
        Random random = new Random();
        return random.nextInt(sides) + 1;
    }
}
```

</details>



Print a dice as string
--------------------------

We would like to print the dice directly, or rather the string representation of the dice.

We add the following code to the main program and we expect a graphic representation of the dice to be printed out.

```java
        System.out.println("The die is: " + die);
    }
}
```

Add code to make the program pass compilation and execute to print out the graphic representation of the die.

<details>
<summary>Solution print Dice as string</summary>

We implement the built-in method `toString()` to create a graphic representation of the dice. To make this happen we also need to remember the last roll of the dice and we add that to the state of the object, as a private member.

```java
import java.util.Random;

public class Dice {
    private int sides;
    private int lastRoll;

    public Dice (int sides) {
        this.sides = sides;
    }

    public int roll () {
        Random random = new Random();
        lastRoll = random.nextInt(sides) + 1;
        return lastRoll;
    }

    @Override
    public String toString() {
        return "[" + lastRoll + "]";
    }
}
```

</details>

The output can now look like this.

```
The die is: [6]
```



Print a dice as integer value
--------------------------

We would like to be able to get the integer representatio of the dice so we can compare it and calculate with it.

We add the following code to the main program and we expect a integer value representing the dice to be printed out.

```java
        System.out.println("The die is: " + die.getValue());
    }
}
```

Add code to make the program pass compilation and execute to print out the graphic representation of the die.

<details>
<summary>Solution get integer value of Dice</summary>

We implement a method  to return the value of the lastRoll of the dice. If the dice has not been rolled it will be the default value that is returned.

```java
public int getValue() {
    return lastRoll;
}
```

</details>

The output can now look like this.

```
The die is: [6]
The die is: 6
```



Graphic representation of the dice
--------------------------

We can use UTF-8 representations of the dice and update our string representation of the dice. The UTF-8 characters look like this: ⚀ ⚁ ⚂ ⚃ ⚄ ⚅

We do not need to update the main program to do this since the graphic represnetatino of the Dice is encapsulated within the class.

Update the Dice class to show a graphical representation in the method `toString()`.

<details>
<summary>Solution print UTF-8 representation of the Dice</summary>

First we add an array holding the graphic representations.

```java
private String[] diceGraphics = {"⚀", "⚁", "⚂", "⚃", "⚄", "⚅"};
```

Then we update the `toString()` to use them.

```java
@Override
public String toString() {
    //return "[" + lastRoll + "]";
    return diceGraphics[lastRoll - 1];
}
```

</details>

The output can now look like this.

```
The die is: ⚅
The die is: 6
```



Roll dices in a loop
--------------------------

We want to roll many dices and we roll them in a loop until we say stop. The main program rolls the dice and then asks the user if it should roll another dice or stop. 

The loop-part of the code can look like this.

```java
Scanner scanner = new Scanner(System.in);
String input;
do {
    die.roll();
    System.out.println("Dice rolled: " + die + " (" + die.getValue() + ")");

    System.out.println("Roll once more [Yn] ");
    input = scanner.nextLine();
} while (input.equals("y") || input.equals(""));
scanner.close();
```

There is no need to change the implementation of the Dice class for this.

The output can now look like this.

```
Dice rolled: ⚅ (6)
Roll once more [Yn] 

Dice rolled: ⚄ (5)
Roll once more [Yn]
```


Class diagram
--------------------------

To represent the Main class and the Dice class we can write an UML diagram like this.

![uml diagram](img/class-diagram.png)

_Figure. An UML diagram of the Dice class being used by the Main class._

Can you review the UML diagram and see all parts that you have implemented in this exercise?



Summary
--------------------------

You have now built and run a Java program that works with a class.
