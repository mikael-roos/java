---
revision:
    "2023-08-03": "(B, mos) Worked through and updated."
    "2022-08-26": "(A, mos) For autumn 2022."
---
Split your Java project into files and packages
===========================

This shows how to split your code into several files and how to split them into a directory structure using the package construct.

[[_TOC_]]



About Files and Packages
---------------------------

You can write all your code in one file, but eventually it grows to large to manage. When the program grows bigger you will want to split it into several files and packages (code modules).

Packages, or modules, are great when structuring larger projects or when sharing code from your teammates and share your code with others. Splitting the code into packages makes the code easier to reuse between projects.

You can read more on packages in the [Java Tutorial: Packages](https://docs.oracle.com/javase/tutorial/java/package/index.html). 

The example programs that follows shows how to setup, compile and build the code using a single file, multiple files and with packages.



Example files in repo
---------------------------

The example files is available in the example repo which you can clone.

```
git clone https://gitlab.com/mikael-roos/java.git
cd java
```

You can also view [the repo online in GitLab](https://gitlab.com/mikael-roos/java).



Single file
---------------------------

In the directory [`sample/dice_single_file`](https://gitlab.com/mikael-roos/java/-/tree/main/sample/dice_single_file) you will find a file containing both the main class and a dice class.

```
.
└── Main.java
```

You can can build and run it like this in your terminal.

```
# Go to the directory
javac Main.java
java Main
```

Or like this.

```
javac Main.java && java Main
```

You have the following files in your directory, after you have compiled the code.

```
.
├── Dice.class
├── Main.class
└── Main.java
```

Each class is compiled into its own file.

It can look like this.

![Single file](img/single_file.png)



Split classes in files
---------------------------

In the directory [`sample/dice_separate_files`](https://gitlab.com/mikael-roos/java/-/tree/main/sample/dice_separate_files) you will find the same code split into two files.

```
.
├── Dice.java 
└── Main.java 
```

You can build and run it like this in your terminal.

```
# Go to the directory
javac Dice.java
javac Main.java
java Main
```

Or like this.

```
javac *.java && java Main
```

You have the following files in your directory, after you have compiled the code.

```
.
├── Dice.class
├── Dice.java
├── Main.class
└── Main.java
```

It can look like this.

![Separate file](img/separate_files.png)



Packages
---------------------------

In the directory [`sample/dice`](https://gitlab.com/mikael-roos/java/-/tree/main/sample/dice) you will find the code split into files in a directory structure matching the package name `com.example.dice`.

Using package names you create namespaces and you can reuse the same class name in separate packages which makes it easy to share code with others.

```
.                            
└── src                      
    ├── Main.java            
    └── com                  
        └── example          
            └── dice         
                └── Dice.java
```

You can notice that the Main-program is not part of the package. That was my choice for this example.

You can can build and run it like this in your terminal.

```
# Go to the directory dice/src
javac com/example/dice/*.java
javac Main.java
java Main
```

You have the following files in your directory, after you have compiled the code.

```
.
└── src
    ├── Main.class
    ├── Main.java
    └── com
        └── example
            └── dice
                ├── Dice.class
                └── Dice.java
```

It can look like this.

![Package](img/package.png)



Package with main program
---------------------------

I could have the main-program as part of the package and execute it as that. That could sometimes be useful, for example if I want to have some test programs together with my package code. To update the example to do this I need to do a few things.

First copy the main program to the namespace where I want it to be.

```
cd src
cp Main.java com/example/dice/
```

Then I add the package name in the file and I comment out the import statement.

Then you can can build and run it like this in your terminal.

```
javac com/example/dice/*.java
java com.example.dice.Main
```

It can look like this.

![Package with main](img/package_main.png)



Package with several main programs
---------------------------

You can have as many main programs as you wish. This might be helpful when developing and testing different aspects of your code.

Just create another main program, like this.

```java
package com.example.dice;

public class Main2 {
    
    public static void main(String[] args) {
        System.out.println("This is main 2.");
    }
}
```

Compile it as before and run it like this.

```
java com.example.dice.Main2
```

It might be a good iea to have many small test programs during development. It will ease development and time to debug.



How to proceed
---------------------------

Since you will most likely create programs with several file, the best way to proceed is to start using packages to build your code.

Where you put your main-programs can be up to you and depend on what you are doing and how tight the relationship is between the main and the package code.
