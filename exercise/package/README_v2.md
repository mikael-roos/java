---
revision:
    "2024-11-25": "(C, mos) Reworked and changed its structure."
    "2023-08-03": "(B, mos) Worked through and updated."
    "2022-08-26": "(A, mos) For autumn 2022."
---
Split your Java project into files and packages
===========================

This shows how to split your code into several files and how to split them into a directory structure using the package construct.

[[_TOC_]]



About Files and Packages
---------------------------

You can write all your code in one file, but eventually it grows to large to manage. When the program grows bigger you will want to split it into several files and packages (code modules).

Packages, or modules, are great when structuring larger projects or when sharing code from your teammates and share your code with others. Splitting the code into packages makes the code easier to reuse between projects.

You can read more on packages in the [Java Tutorial: Packages](https://docs.oracle.com/javase/tutorial/java/package/index.html). 

The example programs that follows shows how to setup, compile and build the code using a directory structure and packages.



Packages
---------------------------

In the directory [`src`](src/) you will find the code split into files in a directory structure matching the package name `se.mos.dice`.

The package name comes from a reversed url of "mos.se" which is my imaginary url representing me as a coder. You should choose your own url and base for a package name. 

Using package names you create namespaces and you can reuse the same class name in separate packages which makes it easy to share code with others. The class can have the same name as long as they are in different namespaces.

```
.                            
└── src                      
    ├── Main.java            
    └── se                  
        └── mos          
            └── dice         
                └── Dice.java
```

You can notice that the Main-program is not part of the package. That was my choice for this example. You can also create a Main-program as part of the package, if that is suitable.

You can can build and run it like this in your terminal.

```bash
# Go to the directory src/
javac se/mos/dice/*.java
javac Main.java
java Main
```

You can also compile and run in one line.

```bash
javac se/mos/dice/*.java Main.java; java Main
# or like this
javac se/mos/dice/*.java Main.java && java Main  
```

You have the following files in your directory, after you have compiled the code.

```
.
└── src
    ├── Main.class
    ├── Main.java
    └── se
        └── mos
            └── dice
                ├── Dice.class
                └── Dice.java
```

It can look like this.

![Package structure](img/package-structure.png)



Package with main program
---------------------------

I could have the main-program as part of the package and execute it as that. That could sometimes be useful, for example if I want to have some test programs together with my package code. To update the example to do this I need to do a few things.

First copy the main program to the namespace where I want it to be.

```
cd src
cp Main.java se/mos/dice/
```

Then I add the package name in the file and I comment out the import statement.

Then you can can build and run it like this in your terminal.

```bash
javac se/mos/dice/*.java
java se.mos.dice.Main
```

Or like this in one line.

```bash
javac se/mos/dice/*.java; java se.mos.dice.Main
# or
javac se/mos/dice/*.java && java se.mos.dice.Main
```

It can look like this.

![Package with main](img/package-main.png)

As you can see you now have two classes with the same name `Main` but they live in separate namespaces since they have different package names.



Summary
---------------------------

Since you will most likely create programs with several files, the best way to proceed is to start using packages and directory structures to build your code.

Start by selecting your own start of the package name, your version of my `se.mos`.

Where you put your main-programs can be up to you and depend on what you are doing and how tight the relationship is between the main and the package code. It can be quite useful to during development have several main programs testing various parts of your code.
