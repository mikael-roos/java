---
revision:
    "2024-09-22": "(C, mos) Walkthrough for ht24, minor edits."
    "2023-08-10": "(B, mos) Walkthrough for ht23, minor edits."
    "2022-09-21": "(A, mos) First release."
---
Unit testing with JUnit
===========================

This is how to get going with JUnit to write and execute unit tests for Java using Gradle.

[[_TOC_]]


<!--

TODO

* Mocking
    * https://site.mockito.org/
* Random
* Mocka input stream

-->

Read up on JUnit
---------------------------

We are going to use the framework [JUnit 5](https://junit.org/junit5/) as our unittest framework. You can quickly check out their web page. 

In the user documentation of JUnit 5, the following sections could be nice to know about.

You can also quickly glance the section on "[What is JUnit 5?](https://junit.org/junit5/docs/current/user-guide/#overview-what-is-junit-5)", it is just 10 lines in the user documentation.

You might want to install the [Visual Studio Code extention for JUnit 5](https://junit.org/junit5/docs/current/user-guide/#running-tests-ide-vscode) (it might already be installed as part of another extension).

You can also see that there is a section on [Gradle integration with JUnit 5](https://junit.org/junit5/docs/current/user-guide/#running-tests-build-gradle).



Example code saved in `sample/junit5`
---------------------------

The code generated and used in this exercise is all [stored in the repository in the directory `sample/junit5`](https://gitlab.com/mikael-roos/java/-/tree/main/sample/junit5).

Use it to compare your own code generated in this exercise.



Get going
---------------------------

Start with setting up a project using Gradle with the following parameters.

```
mkdir junit5
cd junit5
gradle init
```

Choose:

* Select type of project to generate: "application"
* Select implementation language: "java"
* Split functionality across multiple subprojects?: "no"
* Select build script DSL: "groovy"
* Select test framework: "JUnit Jupiter"
* For the rest use the default proposed value

Gradle will generate a directory structure looking like this.

```
├── app                               
│   ├── build.gradle                  
│   └── src                           
│       ├── main                      
│       │   ├── java                  
│       │   │   └── junit5            
│       │   │       └── App.java      
│       │   └── resources             
│       └── test                      
│           ├── java                  
│           │   └── junit5            
│           │       └── AppTest.java  
│           └── resources             
├── gradle                            
│   └── wrapper                       
│       ├── gradle-wrapper.jar        
│       └── gradle-wrapper.properties 
├── gradlew                           
├── gradlew.bat                       
└── settings.gradle                   
                                      
12 directories, 8 files               
```

Try to execute the main hello program through Gradle.

```
./gradlew run
```



Build and execute the tests
---------------------------

The gradle task for executing all tests in the testsuite is named "test". Start by checking out the helptext for the "test" task.

```
./gradlew help --task test
```

The testsuite for the unittests are located in the directory `app/src/test/java/`.

Lets view the file `app/src/test/java/junit5/AppTest.java` that contains the test cases.

![AppTest.java](img/apptest-java.png)

Now try to execute the testsuite using the gradle task. It might look like this.

```
$ ./gradlew test

BUILD SUCCESSFUL in 838ms
3 actionable tasks: 3 executed
```

You can also execute the tests through your gradle plugin in vscode.

![Run tests though vscode and gradle](img/run-tests-though-vscode-and-gradle.png)

You can also use the test plugin to run (or debug) the testsuite or individual tests. Sometimes it might be quite valuable to be able to execute or debug only a single and particular test.

![Run tests though vscode and test plugin](img/run-tests-though-vscode-and-testplugin.png)

As you see, there are several ways to execute (and debug) your tests.



Configure for JUnit 5
---------------------------

The configuration for JUnit 5, or the JUnit Platform as it also is called, is already configured by gradle in the configuration file `app/build.gradle`.

It looks like this, the version number might vary (and the configuration file varies between different versions of gradle).

First the dependency.

```json
dependencies {
    // Use JUnit Jupiter for testing.
    testImplementation 'org.junit.jupiter:junit-jupiter:5.9.2'
}
```

Then the test task that executes the tests.

```json
tasks.named('test') {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}
```



The test case
---------------------------

The first gradle test case is automatically generated for us. It looks slightly like this.

![gradle test case](img/gradle-testcase.png)

The code can be explained like this.

It has the same package declaration as your ordinary code.

```java
package junit5;
```

Import the test framework and the assertion methods.

```java
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
```

Create a testclass and name it a proper name for the tests it will contain.

```java
class AppTest {

}
```

Now create test methods, these are equal to a test case.

```java
    @Test
    void appHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull(classUnderTest.getGreeting(), "app should have a greeting");
    }
```

There are [several different annotations for a test](https://junit.org/junit5/docs/current/user-guide/#writing-tests-annotations), the `@Test` states that this is a test method.

In the method, or in the class, you need to prepare the code for the test case. In this example it is done by instantiating the class.

```java
// Prepare for the test - setup the test
App classUnderTest = new App();
```

Now we are ready to execute the test. These are the method, or the combination of methods, that are our current testobject and focus to test. This could slightly be rewritten like this.

```java
String res = classUnderTest.getGreeting();
```

We know expect that the `String res` contains the text "Hello world", or something that could be a greeting. We know want to assert that this is correct. Since it is hard to state what really a "greeting" migh contain, we just settle with the string to contain a value and not be null.

```java
String res = classUnderTest.getGreeting();
assertNotNull(res, "app should have a greeting");
```

You can see this like the complete test case is executed through the following steps.

```java
    @Test
    void thetest caseMethod() {
        // Prepare for the test case
        // Execute the focus for the test
        // Save the result (or change in state)
        // Assert that the proper result (or change in state) has happend
    }
```

Remember that you always should assert that the proper result, or change in state, occures by using the [various methods for assertions](https://junit.org/junit5/docs/current/user-guide/#writing-tests-assertions) that are all gathered in the class  [`org.junit.jupiter.api.Assertions`](https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/Assertions.html).

You can make the test method as long as you like, it can contain series of events and multiple assertions. Try to give each test method a proper name so you only test one feature, or part of feature, at a time. It will help you organise your test code.



Extend the current test case
---------------------------

For the sport of it we now try to rewrite the current test method to do another assertion that the message contains the string "Hello" somewhere in it.

It could then be rewritten and extended like this. I will create a new test method for this, just for show.

![App has greeting containing Hello](img/app-greeting-with-hello.png)

The code for the new test method looks like this.

```java
    @Test
    void appHasAGreetingWithHello() {
        // Prepare for the test case
        App classUnderTest = new App();

        // Execute the focus for the test
        String res = classUnderTest.getGreeting();

        // Save the result (or change in state)
        Boolean check = res.contains("Hello");

        // Assert that the proper result (or change in state) has happend
        assertNotNull(res, "app should have a greeting");
        assertTrue(check, "greeting should contain Hello");
    }
```

Read the code above to see its structure in preparing, executing, saving and asserting the test case.



Failing a test case
---------------------------

<!--
If you try to develop your test code inline with your actual code, you will then get the added benefit of the procedure like.

* Write code
* Write the test code
* Execute the test
* If fail, fix it
* It pass, do it again
-->

When a test case fails, it is usually the assertions that fail. It could look like this.

I will rewrite `appHasAGreetingWithHello()` to simulate a failing test case by doing an invalid assertions.

```java
        // Invalid assertions to show off failing the test case
        //assertNotNull(res, "app should have a greeting");
        assertNull(res, "app should have a greeting");

        //assertTrue(check, "greeting should contain Hello");
        assertFalse(check, "greeting should contain Hello");
```

First the test case fails at the `assertNull()`.

![Assert null fails](img/fail-assert-null.png)

If that is corrected and we then rerun the test, the test it will fail at `assertFalse()`.

![Assert true fails](img/fail-assert-true.png)

Can you see how this might help you in developing your application code, if you write your tests inline when writing your application code and you test your code by one feature at a time?



Test-driven development
---------------------------

There is a development process called "Test-driven development TDD" that focuses on writing the test cases at the same time as you write your code. Well, it really proposes that you write your test case before you write your application code. 

You could think of this as creating your code the other way around, start by writing the test case and then implement the code that solves the test case. One benefit of this development process is that you focus on the interface (the public API) of your application code and not its hidden implementation. The test case can only use the public API of your classes.

Another benefit is that you will get a testsuite that covers all your code.

The "TDD mantra" could be spelled like this.

* Decide on a small feature
* Write the test case
    * Execute it and watch it fail
* Write the smallest part of application code to make the test case pass
    * Execute it and watch it pass
* Do it again

Think about how the TDD mantra might be different from the ordinary way you write your application code today and consider if you see any benefits in doing it the TDD way.



Example - TDD style
---------------------------

To learn more on how to work with unittests we will work through an example where we write a Calculator class and we create the unittests along with it. For the fun of it we do this in the TDD say, writing the test cases first and then the application code.



### Calculator.add()

The calculator should be able to add to values. Lets write a test case that asserts that.

First we need a test class.

```java
/**
 * test cases for a calculator, to show off how unit tests works.
 */
package junit5;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class CalculatorTest {
}
```

Then we add the first test method asserting that two values can be added. I decide to add the calculator object as a privat member since all test methods will use it.

```java
    private final Calculator calculator = new Calculator();

    @Test
    void addition() {
        int res, exp;

        res = calculator.add(1, 1);
        exp = 2;
        assertEquals(exp, res);

        res = calculator.add(2, 2);
        exp = 4;
        assertEquals(exp, res);
    }
```

I do two assertions in the code.

In the TDD way I would now execute the test method to watch it fail.

![TDD fail 1](img/tdd-fail-1.png)

To fix that failure I create the class Calculator.

```java
/*
 * Class to show off how unit tests works.
 */
package junit5;

public class Calculator {

}
```

Then I execute the test again which gives another failure.

![TDD fail 2](img/tdd-fail.2.png)

I fix that failure by adding the method (and implementing it).

```java
    int add (int a, int b) {
        return a + b;
    } 
```

Then I execute the test to watch them pass.

![TDD pass](img/tdd-pass.png)

The I proceed with the next method.



### Calculator.subtract()

The subtraction part is the same setup as the addition. You can try to add that on your own.



### Calculator.division()

The division usually has a odd behaviour when trying to divide with zero. How should that be handled in the application and in the test case?

Lets try it out. First the test case.

```java
        res = calculator.division(1, 0);
        exp = 0; // Hmm perhaps exception?
        assertEquals(exp, res);
```

Then we implement the body.

```java
    double division (double a, double b) {
        return a / b;
    } 
```

While executing the test code I find out that Java handles division with 0 to produce the result `Double.POSITIVE_INFINITY` so I can actually update my test case using that as an assertion.

```java
        res = calculator.division(1, 0);
        exp = Double.POSITIVE_INFINITY;
        assertEquals(exp, res);
```

Now this also works.



### Test method throwing exception

Lets say that we do not approve of doing division with zero and we want the method to throw an exception when the user tries to divide by zero. 

The method throwing an exception might look like this.

```java
double divisionWithException (double a, double b) {
    if (b == 0) {
        throw new java.lang.ArithmeticException("Division by zero not allowed.");
    }
    return a / b;
} 
```

We can now write a unittest using the `assertException()` that looks slightly different since it must be called before the code throws the exception.

```java
@Test
void divisionWithException() {
    Exception exception;
    
    exception = assertThrows(
        ArithmeticException.class,
        () -> calculator.divisionWithException(1, 0)
    );

    Boolean check = exception.getMessage().contains("zero");
    assertTrue(check);
}
```

In this test case we assert that the exception is thrown, and then we also assert that the exception message contains a part of the expected message.



Debug a test case
---------------------------

You can debug a test case, more or less like you debug your ordinary code, using the test plugin. You click the "Debug test" instead of the "Run test" image near the test.

![Debug test case](img/debug-unit-tests.png)

When you work using test cases you can use your application almost like it was a main program. You get the opportunity to try out your code through various constructs and it will help your code to grow, verified, step by step.



Test report with Gradle
---------------------------

We can now execute the complete testsuite with Gradle.

Firts we clean to get a clean build.

```
./gradlew clean
```

Then we execute the testsuite.

```
./gradlew test
```

We can now review the report generated in the directory `app/build/reports/tests/test/index.html`, it can look like this.

![Test report main](img/test-report-main.png)

The status for the class `AppTest` looks like this.

![Test report AppTest](img/test-report-apptest.png)

The status for the class `CalculatorTest` looks like this.

![Test report CalculatorTest](img/test-report-calculatortest.png)

A report from the tests like this is good to have to get an overview of what tests you have and their status.



Execute only parts of the tests
---------------------------

Using gradle you can execute only parts of the testsuite, for example a particular test class or a specific test method.

```
./gradlew test --tests CalculatorTest
./gradlew test --tests CalculatorTest.division
```

This is helpful when developing new tests or testing out a single feature. Remember that this can also be done using the plugin in vscode.



Code coverage
---------------------------

When running the testsuite we also want to see what lines of code are really covered by tests and what lines of code do still need testing. For this purpose we use a tool to show us the code coverage of the tests.

The tool [JaCoCo Java Code Coverage Library](https://www.jacoco.org/jacoco/) is one available tool for generating code coverage reports and it can easily be integrated into Gradle.

To start generating code coverage report we follow the "[Gradle guide: The JaCoCo Plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html)" which shows us to add the following lines to our configuration file `app/build.gradle`.

```
plugins {
    id 'jacoco'
}

test {
    // report is always generated after tests run
    finalizedBy jacocoTestReport
}
jacocoTestReport {
    // tests are required to run before generating the report
    dependsOn test
}
```

This will make the code coverage report to be run after the test task. We can also run only the task "jacocoTestReport" like this.

```
./gradlew jacocoTestReport
```

But it perhaps makes more sense to just execute the testsuite and the coverage report is generated as part of that task.

```
./gradlew test
```

A note can be made that the testrunner only executes the test cases if needed, you might see that some tests are "up-to-date" and thus not executed. If you want to re-run all tests you can do like this.

```
./gradlew test --rerun-tasks
```

The code coverage report is generated into the directory `app/build/reports/jacoco` and it looks like this.

![Coverage report main](img/coverage-report-main.png)

And this is what we see if we click into the App class.

![Coverage report app](img/coverage-report-app.png)

And this is what we see if we click into the Calendar class.

![Coverage report calculator](img/coverage-report-calculator.png)

If we step further into the code of the Calculator class we can see exactly what rows are covered with unit tests and what rows still need to be covered.

![Coverage report calculator class](img/coverage-report-calculator-class.png)

You now see what parts are not covered by tests. Can you add test cases to reach 100% of code coverage to the example code?



Summary
---------------------------

That was all for now, carry on to get more aquainted with Gradle, JUnit and unittesting.



