---
revision:
    "2023-08-01": "(B, mos) Workthrough with minor edits."
    "2022-08-26": "(A, mos) For autumn 2022."
---
Hello world - Build and execute your first program in Java
==========================

This is to learn the basics on how to write a Java class and then build and run it.

Feel free to work with your friends and discuss.

[[_TOC_]]



Prepare
--------------------------

There is an exercise repo that you can clone to your own machine to get access of the example files.

You can clone it like this.

```
git clone https://gitlab.com/mikael-roos/java.git
cd java
```

You will find the files for this exercise in the drectory `exercise/hello_world`.

```
cd exercise/hello_world
ls -l
```

The repo itself is always under development. You can always check that you have the latest version of it.

```
# Go to the directory containing the repo 
git pull
```



Work with your own files in `me/`
--------------------------

When you work with an exercise it is helpful to copy the source files to another directory. An excellent place for this is below the directory `me/`. Copy the exercise files there and then edit and rework them as you wish. Doing so will help you stay in sync with the latest updates of the repo since you will not be editing any files in the repo itself.

You can also work in your own repo, that is also a good idea for saving your work.



Check what Java you have installed
--------------------------

Ensure that you have Java installed on your local machine, start the terminal and check for its version. You may have another version from the versions I show below.

This is how it may look on Windows.

```
$ java --version
java 18.0.2 2022-07-19
```

<!--
This is how it may look on Mac OS.

```
$ java --version
openjdk 11.0.16 2022-07-19
```
-->

This is how it may look on Ubuntu/Linux when running Open JDK as an alternative.

```
$ java --version
openjdk 18.0.2-ea 2022-07-19
```



Open the code in Visual Studio Code
--------------------------

Start the Visual Studio Code application (vscode/code) in the exercise directory. You should (usually) be able to do it like this from the terminal.

```
# Go to the directory where the exercise files are
code .
```

Add an ampersand to start the editor in the background without locking the terminal.

```
code . &
```

Visual Studio Code may ask you to install the "[Extension Pack for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)", feel free to do so, it will make the development environment more pleasant.



Build and run the code
--------------------------

You can now open and see the source code of the Java program in the file `HelloWorld.java`. We can compile it and then run it.

We start by compiling it using the compiler `javac`. This will generate a file `HelloWorld.class` containing byte code.

```
javac HelloWorld.java
ls -l
```

Use the command `file` to check what type of files it is.

```
$ file HelloWorld.*
HelloWorld.java:  Java source, ASCII text
HelloWorld.class: compiled Java class data, version 62.0
```

Now we can run the program using the `java` interpretator running the byte code.

```
java HelloWorld
```

It can look like this when you are done.

![Hello World in VisualStudio Code](img/hello_world.png)



Investigate the code
--------------------------

You can now quickly browse through the short article "[Lesson: A Closer Look at the "Hello World!" Application](https://docs.oracle.com/javase/tutorial/getStarted/application/index.html)" that gives some insight into the details of the HelloWorld program and its constructs.



Summary
--------------------------

You have now built and run your first Java program.
