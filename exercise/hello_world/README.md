---
revision:
    "2024-11-03": "(C, mos) Make generic to support powershell and add debugger."
    "2023-08-01": "(B, mos) Workthrough with minor edits."
    "2022-08-26": "(A, mos) For autumn 2022."
---
Hello world - Build and execute your first program in Java
==========================

This is to learn the basics on how to write a Java class and then build and run it.

Feel free to work with your friends and discuss.

[[_TOC_]]



Precondition
--------------------------

You have installed Java and you have a terminal and you have a text editor.



Prepare
--------------------------

Create a working directory where you can save all your files. 

```bash
mkdir java
cd java

mkdir hello
cd hello
ls
```

Now you are ready to work with this exercise.



Open the code in Visual Studio Code
--------------------------

Start the Visual Studio Code application (vscode/code) in the directory you created. You should be able to do it like this from the terminal.

```
# Go to the directory where you want to work
code .
```

On some terminals you might need to add ampersand to start the editor in the background without locking the terminal.

```
code . &
```

Visual Studio Code may ask you to install the "[Extension Pack for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)", feel free to do so, it might enhance your development environment.



The source code for "Hello World"
--------------------------

Here is a sample source code for a "Hello World" in java.

```java
/**
 * A hello world program in Java.
 */
public class Hello {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
```

Create a file `Hello.java` in your editor and save it.



Build and run the code
--------------------------

To execute the program we first need to compile it. In Java we compile the source code into byte code.

We compile it using the compiler `javac`. This will generate a file `HelloWorld.class` containing byte code.

Open the terminal in vscode and execute the following commands.

```
javac Hello.java
ls
```

You should see a file `Hello.class` which contains Java byte code. The byte code can be executed on any system, it is independent of the operating system you are using.

Now we can run the program using the `java` interpretator running the byte code.

```
java Hello
```

It can look like this when you are done.

![Hello World in VisualStudio Code](img/hello.png)

_Figure. The Hello world program is compiled from its source to byte code and then executed._



Execute in the debugger
--------------------------

You can execute the program using the debugger. To try it out you can add a (red) breakpoint in the code on line 6 by clicking left from the row number. A red dot should appear. 

You run the debugger through `F5` or "Run - Start Debugging".

The debugger will stop att your breakpoint.

![Debugger stopped](img/debugger-stop.png)

_Figure. The debugger has stopped at the breakpoint on line 6._

A menu appears on top of the source code, you can choose how to proceed. Press "Continue" and the code will start executing again.

Since there are no more breakpoints, the code will execute to its end.

![debugger done](img/debugger-done.png)

_Figure. The debugger executes until the end of the code._



Investigate the code
--------------------------

You can now quickly browse through the short article "[Lesson: A Closer Look at the "Hello World!" Application](https://docs.oracle.com/javase/tutorial/getStarted/application/index.html)" that gives some insight into the details of the HelloWorld program and its constructs.



A larger hello world program (OPTIONAL)
--------------------------

You can study a larger hello world program in the file [`HelloWorld.java`](https://gitlab.com/mikael-roos/java/-/blob/main/sample/hello/HelloWorld.java).

Feel free to copy its source into a new file and execute it to find out what happens.



Summary
--------------------------

You have now built and run your first Java program.
