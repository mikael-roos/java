Copyright 2022-2024 Mikael Roos, mikael.t.h.roos@gmail.com, https://opensource.org/license/fair

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
