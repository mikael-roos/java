---
author: mos
revision: 
    2023-08-11: "(A, mos) first version"
---
Programming with Java - Unit testing
====================

Learn how to write unit tests with Java, Gradle and JUnit 5 and the API JUnit Jupiter. Code coverage is generated using JaCoCo Java Code Coverage Library.

<!--
TODO

* https://site.mockito.org/
* How to mock?

-->

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/unit-testing-with-java/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [Unit testing with JUnit](../../../exercise/junit/README.md)



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [junit5](../../../sample/junit5/)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. [JUnit 5](https://junit.org/junit5/)
    * [What is JUnit 5?](https://junit.org/junit5/docs/current/user-guide/#overview-what-is-junit-5)
    * [JUnit Jupiter API](https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api)

1. [JaCoCo Java Code Coverage Library](https://www.jacoco.org/jacoco/)
    * [Gradle guide: The JaCoCo Plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html)

1. [Visual Studio Code extention for JUnit 5](https://junit.org/junit5/docs/current/user-guide/#running-tests-ide-vscode)

1. [Gradle integration with JUnit 5](https://junit.org/junit5/docs/current/user-guide/#running-tests-build-gradle)
