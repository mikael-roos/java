---
author: mos
revision: 
    2023-08-08: "(A, mos) first version"
---
[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/collections-with-java/slide.html)

Programming with Java - Array and collections
====================

Overview of the Java Collection Framework with its structure, interfaces and classes. Example code to view how to get going with the different collection classes.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/collections-with-java/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->


<!--
Exercise
------------------------

The following exercise is related to this lecture.

* [Split your Java project into files and packages](../../exercise/package/README.md)
-->



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [Collection code samples](../../../sample/collection/)
    * [array](../../../sample/collection/array)
    * [arraylist](../../../sample/collection/arraylist)
    * [iterator_collection](../../../sample/collection/iterator_collection/)
    * [set](../../../sample/collection/set)
    * [map](../../../sample/collection/map/)
    * [queue](../../../sample/collection/queue/)
    * [deque](../../../sample/collection/deque/)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. The Java® Language Specification
    * [Chapter 10. Arrays](https://docs.oracle.com/javase/specs/jls/se18/html/jls-10.html)

1. Oracle website on Java
    * [The Collections Framework](https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/doc-files/coll-index.html)
    * Tutorial
        * [Trail: Collections](https://docs.oracle.com/javase/tutorial/collections/index.html)
        * [Lesson: Introduction to Collections](https://docs.oracle.com/javase/tutorial/collections/intro/index.html)
    * [Package java.util](https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/package-summary.html)
