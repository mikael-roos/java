import java.util.Arrays;
import java.util.Scanner;

public class DiceGameCeeLo {
    private Dice die1;
    private Dice die2;
    private Dice die3;
    private int result1;
    private int result2;

    public DiceGameCeeLo(Dice die1, Dice die2, Dice die3) {
        this.die1 = die1;
        this.die2 = die2;
        this.die3 = die3;
    }

    private void roll() {
        die1.roll();
        die2.roll();
        die3.roll();
        System.out.println(die1.toString() + die2.toString() + die3.toString());
    }

    private int result(String player) {
        int result = 0;
        int val[] = {
            die1.getValue(),
            die2.getValue(),
            die3.getValue()
        };
        Arrays.sort(val);

        if (val[0] == val[2]) {
            System.out.println(player + " got three of a kind and win!");
            result = 6;
        } else if (val[0] == 4 && val[2] == 6) {
            System.out.println(player + " got high ladder and win!");
            result = 6;
        } else if (val[0] == 1 && val[2] == 3) {
            System.out.println(player + " got low ladder and looses!");
            result = 1;
        } else if (val[0] < val[1] && val[1] < val [2]) {
            System.out.println(player + " got no result, roll again!");
            result = 0;
        } else {
            if (val[0] == val[1]) {
                result = val[2];
            } else if (val[1] == val[2]) {
                result = val[0];
            }

            String winloose = "!";
            if (result == 6) {
                winloose = " and win!";
            } else if (result == 1) {
                winloose = " and loose!";
            }
            System.out.println(player + " got pair and result is " + result + winloose);
        }
        return result;
    }

    private void checkWinner(String player1, String player2) {
        if (result1 == 1) {

        } else if (result1 == 6) {

        } else if (result1 > result2) {
            System.out.print(player1 + " win this round!");
        } else {
            System.out.print(player2 + " win this round!");
        }
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        String input;
        do {
            System.out.print("Press enter to roll the dices. ");
            input = scanner.nextLine();

            do {
                roll();
                result1 = result("You");   
            } while (result1 == 0);

            result2 = 0;
            if (result1 != 6 && result1 != 1) {
                do {
                    roll();
                    result2 = result("Avatar");
                } while (result2 == 0);
            }

            checkWinner("You", "Avatar");

            System.out.print("Press enter to play another round [q] ");
            input = scanner.nextLine();
        } while (input == "y" || input == "");
    }
}
