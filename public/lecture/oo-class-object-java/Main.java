public class Main {
    public static void main(String[] args) {
        Dice die1 = new Dice(6);
        Dice die2 = new Dice(6);
        Dice die3 = new Dice(6);

        DiceGameCeeLo game = new DiceGameCeeLo(die1, die2, die3);
        game.run();
    }
}
