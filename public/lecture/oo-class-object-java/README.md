---
author: mos
revision: 
    2024-11-18: "(B, mos) Rewritten to focus on classes and objects."
    2023-08-08: "(A, mos) Reviewed from last year, added pictures."
---
[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/oo-class-object-java/slide.html)

Programming in Java: Class and object
====================

First we review how to construct classes and objects in Java and the basic terminology used for doing so.

Then we review an example and how to think when creating a program from scratch.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/oo-class-object-java/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->


<!--
Exercise
------------------------

The following exercise is related to this lecture.

* [Split your Java project into files and packages](../../exercise/package/README.md)
-->


Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [Main, Dice, DiceGameCeeLo](.)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Oracle website
    * [Lesson: Object-Oriented Programming Concepts](https://docs.oracle.com/javase/tutorial/java/concepts/index.html)

1. [Wikipedia: Dependency injection](https://en.wikipedia.org/wiki/Dependency_injection)


1. [Wikipedia: Cee-lo (Dice game)](https://en.wikipedia.org/wiki/Cee-lo)
