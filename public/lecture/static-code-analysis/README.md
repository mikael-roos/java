Nothing here.

<!--

* Roughly review a few concepts and terms of clean code through Wikipedia.
    * [Coding conventions](https://en.wikipedia.org/wiki/Coding_conventions)
    * [Code smell](https://en.wikipedia.org/wiki/Code_smell)
    * [Cohesion](https://en.wikipedia.org/wiki/Cohesion_(computer_science))
    * [Coupling](https://en.wikipedia.org/wiki/Coupling_(computer_programming))
    * [Code coverage](https://en.wikipedia.org/wiki/Code_coverage)
    * [Cyclomatic complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity)
    * [Technical debt](https://en.wikipedia.org/wiki/Technical_debt)
        * [Broken windows theory](https://en.wikipedia.org/wiki/Broken_windows_theory)
-->
