---
author: mos
revision: 
    2024-11-25: "(A, mos) First version."
---
[![Slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/gradle/slide.html)

About Gradle, a build and automation tool
====================

Introduction to the build and automation tool Gradle and how to use it in a Java project.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/gradle/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [Build your Java project with Gradle](../../../exercise/gradle/README.md)



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [Example on directory structure for a gradle project](../../../exercise/gradle/gradle/)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Gradle
    1. [What is Gradle?](https://docs.gradle.org/current/userguide/userguide.html)
    1. [Building Java & JVM projects](https://docs.gradle.org/current/userguide/building_java_projects.html#building_java_projects)
    1. [Building Java Applications Sample](https://docs.gradle.org/current/samples/sample_building_java_applications.html)
    1. [Install Gradle](https://docs.gradle.org/current/userguide/installation.html)
        * [Compatibility Matrix](https://docs.gradle.org/current/userguide/compatibility.html)

1. [Maven repository](https://mvnrepository.com/)
    * [Maven page for Gson](https://mvnrepository.com/artifact/com.google.code.gson/gson)
    * [javadoc for the Gson](https://javadoc.io/doc/com.google.code.gson/gson/latest/com.google.gson/com/google/gson/Gson.html)