---
author: mos
revision: 
    2023-08-09: "(A, mos) first version"
---
Programming with Java - Features of Java and Java eco system
====================

...

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/features-of-java/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



<!--
Exercise
------------------------

The following exercise is related to this lecture.

* [Split your Java project into files and packages](../../exercise/package/README.md)
-->


<!--
Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [dice_hand_interface](../../../sample/dice_hand_interface)

-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

<!--

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 3. Interfaces and Lambda Expressions

1. Object-Oriented Thought Process, 5th Edition
    * Chapter 2. How to Think in Terms of Objects

1. The Java Tutorials
    * [Lesson: Interfaces](https://docs.oracle.com/javase/tutorial/java/IandI/createinterface.html) (approx 7 pages)

1. Stack overflow
    * [What does it mean to "program to an interface"?](https://stackoverflow.com/questions/383947/what-does-it-mean-to-program-to-an-interface)

1. Wikipedia
    * [Code/Design to contract](https://en.wikipedia.org/wiki/Design_by_contract)
    * [Interface (object-oriented programming)](https://en.wikipedia.org/wiki/Interface_(object-oriented_programming))

-->