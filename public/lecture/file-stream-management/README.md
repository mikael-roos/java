---
author: mos
revision: 
    2023-08-03: "(A, mos) first version"
---
[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/file-stream-management/slide.html)

Programming with Java - Working with files and streams
====================

This lecture presents how to work with files and streams in Java.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/file-stream-management/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [File and stream management with Java](../../../exercise/file-management/README.md)



<!--
Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [shapes](../../../sample/shapes)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. The Java Tutorials
    * [The Java™ Tutorials Lesson: Basic I/O](https://docs.oracle.com/javase/tutorial/essential/io/)

1. Java API
    * [java.io](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/io/package-summary.html)
    * [java.nio.file](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/nio/file/package-summary.html)
    * [java.nio.file.Files](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/nio/file/Files.html)
    * [java.nio.file.Paths](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/nio/file/Paths.html)
    * [java.nio.file.Path](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/nio/file/Path.html)

1. Maven repository
    * [com.google.code.gson](https://mvnrepository.com/artifact/com.google.code.gson/gson)
    * [com.opencsv](https://mvnrepository.com/artifact/com.opencsv/opencsv)
