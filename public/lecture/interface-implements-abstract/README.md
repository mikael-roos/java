---
author: mos
revision: 
    2024-12-09: "(B, mos) Added exercise and improved lecture"
    2023-08-03: "(A, mos) first version"
---
[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/interface-implements-abstract/slide.html)

Programming with Java - Interface, implements and abstract class
====================

This lecture works through an example of designing a class structure for shapes and it includes inheritance, abstract classes and interfaces.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/interface-implements-abstract/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [Java programming: Implement interface and abstract class](../../../exercise/interface-abstract/README.md)



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [shapes](../../../sample/shapes)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. The Java Tutorials
    * [Lesson: Interfaces](https://docs.oracle.com/javase/tutorial/java/IandI/createinterface.html) (approx 7 pages)
    * [Lesson: Abstract Methods and Classes](https://docs.oracle.com/javase/tutorial/java/IandI/abstract.html) (1 page)

1. Stack overflow
    * [What does it mean to "program to an interface"?](https://stackoverflow.com/questions/383947/what-does-it-mean-to-program-to-an-interface)

1. Wikipedia
    * [Code/Design to contract](https://en.wikipedia.org/wiki/Design_by_contract)
    * [Interface (object-oriented programming)](https://en.wikipedia.org/wiki/Interface_(object-oriented_programming))