---
author: mos
revision: 
    2023-10-08: "(A, mos) first version"
---
Programming with Java - User Interface with JavaFX
====================

[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/user-interface-javafx/slide.html)

This lecture presents how to work with user interface in Java, mainly through JavaFX.

The lecture also talks about application architecture on GUI applications and presents the MVC and the MVVM architectural design patterns.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/user-interface-javafx/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->


Exercise
------------------------

The following exercise is related to this lecture.

* [User interface with JavaFX](../../../exercise/user-interface-javafx/README.md)



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* JavaFX
    * [JavaFX: Hello World](../../../sample/javafx/)
    * [JavaFX: Guessing Game](../../../sample/javafx-guess/)
    * [JavaFX: Scene Builder FXML Hello World](../../../sample/scene-builder-hello-world/)

* Swing
    * [swing-panel-with-button](../../../sample/swing-panel-with-button/)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Java FX
    * [JavaFX Documentation Project](https://fxdocs.github.io/docs/html5/)
    * [Wikipedia JavaFX](https://en.wikipedia.org/wiki/JavaFX)
    * [JavaFX API docs](https://openjfx.io/javadoc/21/)

1. Study about JavaFX to learn the basics on how to build GUI.
    * [JavaFX Documentation Project](https://fxdocs.github.io/docs/html5/) as a guide and tutorial on how to work with JavaFX.
    * [JavaFX Tutorial](https://jenkov.com/tutorials/javafx/index.html) with code examples on the ui elements and some of the various options for layout.

1. [Scene Builder](https://gluonhq.com/products/scene-builder/)

1. Review the basics on related architectural design patterns for GUI applications.
    * [Wikipedia on Model View ViewModel (MVVM)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel)
    * [Wikipedia on Model View Controller (MVC)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)

1. Java Swing
    * Package [`javax.swing`](https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/package-summary.html)
    * [Creating a GUI With Swing](https://docs.oracle.com/javase/tutorial/uiswing/)
    * [Wikipedia on Java Swing](https://en.wikipedia.org/wiki/Swing_(Java))
