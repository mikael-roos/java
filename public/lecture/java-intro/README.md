---
author: mos
revision: 
    2024-11-11: "(A, mos) first version"
---
Java programming language - Introduction to Java
====================

[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/java-intro/slide.html)

This lecture is an introduction to the programming language Java.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/java-intro/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [Hello world - Build and execute your first program in Java](../../../exercise/hello_world/README.md).



<!--
Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [Design patterns](../../../sample/design-patterns/)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Wikipedia on "[Java (programming language)](https://en.wikipedia.org/wiki/Java_(programming_language))" for a quick overview of the history and the Java context.

1. [Orace website on Java](https://www.oracle.com/java/) the main resource for Java programming language.
    * [JavaDoc reference documentation](https://docs.oracle.com/en/java/javase/21/) (choose the version you want).
    * [Java® Platform, Standard Edition & Java Development Kit Version 21 API Specification](https://docs.oracle.com/en/java/javase/21/docs/api/index.html)
    * [The Java® Language Specification: Java SE 21 Edition](https://docs.oracle.com/javase/specs/jls/se21/html/index.html)

1. [OpenJDK](https://openjdk.org/) open source version of Java.

1. [TIOBE Index of popular programming languages](https://www.tiobe.com/tiobe-index/)