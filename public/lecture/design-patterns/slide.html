<!doctype html>
<html class="theme-5">
<meta charset="utf-8" />
<link href="../html-slideshow.bundle.min.css" rel="stylesheet" />
<link href="../style.css" rel="stylesheet" />
<script src="https://dbwebb.se/cdn/js/html-slideshow_v1.1.0.bundle.min.js"></script>

<title>Object-orientation with Java</title>

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Object-orientation
## Software design patterns
### Mikael Roos
</script>



<script data-role="slide" data-markdown type="text/html">
# Agenda

* Design patterns
* What is it?
* Why use them?
* Examples

</script>



<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Software Design Patterns
</script>



<script data-role="slide" data-markdown type="text/html">
# Design patterns

> A general, reusable solution to a commonly occurring problem within a given context in software design.

> A description or template for how to solve a problem that can be used in many different situations.

<p class="footnote">Read more https://en.wikipedia.org/wiki/Software_design_pattern</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Design patterns...

* Solve common problems
* Formalized best practices
* Terminology when talking about software design

</script>



<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Singleton design pattern
## Example of a design pattern
</script>



<script data-role="slide" data-markdown type="text/html">
# Need of a solution

You have resources (classes/objects) in your code that needs to be used all over in your code.

* Provide a global object container that can be reached from anywhere
* Container holds access to objects that are central resources to the system
* Container, with resources, are used all over on several places in the code
* Container is a shared resource, containing the same setup for anyone using it

</script>



<script data-role="slide" data-markdown type="text/html">
# How it could work

* Get access to the same object, anywhere in the code
* Use a static method to get hold of the instance

```
// Get access to Singleton class holding services
ServiceContainer sc = ServiceContainer.getInstance();
```

</script>



<script data-role="slide" data-markdown type="text/html">
# How to implement

* Use static resources that are part of the class

```
class ServiceContainer {
    private static ServiceContainer instance = null;

    public static ServiceContainer getInstance() {
        if (instance == null) {
            instance = new ServiceContainer();
        }
        return instance;
    }
}
```

<p class="footnote">Declare all constructors of the class to be private, which prevents it from being instantiated by other objects.</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<img src="img/singleton.png" width="80%">
</script>



<script data-role="slide" data-markdown type="text/html">
# Singleton

* Ensure only one instance of the class
* Control the instantiation through private constructors
* Provide easy access to the instance

<p class="footnote">Read more https://en.wikipedia.org/wiki/Singleton_pattern</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Usage

* Preferred over global variables because they do not pollute the global namespace
* Permit lazy allocation and initialization
* Use-case for singletons
    * Runtime class
    * Service Locator
    * Logging

<p class="footnote">What is lazy allocation and initialization?</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Critiscism

* Anti-pattern that introduces global state into an application, often unnecessarily
* Potential dependency on the singleton in all code it is visible
* Increased coupling can introduce difficulties with unit testing
* Preventing concurrent use of multiple instances
* Singletons also violate the single-responsibility principle (?)

<p class="footnote">Critiscism always exists, does it seem valid and what are the alternatives?</p>

</script>



<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Design patterns
## More on what it is
</script>



<script data-role="slide" data-markdown type="text/html">
# History

* Design patterns have been applied practically for a long time,
    * formalization of the concept languished for several years
* Dates back to -87 and -77 in different papers and conferences
* Gained popularity due to book "Design Patterns: Elements of Reusable Object-Oriented Software" published 1994
    * Authors called "Gang of Four (GoF)"
    * Documented various design patterns

</script>



<script data-role="slide" data-markdown type="text/html">
# Practice

> Design patterns provide general solutions, documented in a format that does not require specifics tied to a particular problem nor programming language.

* Speed up the development process
* Helps to prevent subtle issues in new fresh code
* Terminology to discuss design

</script>



<script data-role="slide" data-markdown type="text/html">
# Areas

* Creational patterns
* Structural patterns
* Behavioral patterns (communication)
* Concurrency patterns
* Architectural patters

(and even more areas)

</script>


    
<script data-role="slide" data-markdown type="text/html">
# Critiscism

* May be a sign that some features are missing in a given programming language
* Inappropriate use of patterns may unnecessarily increase complexity

</script>



<script data-role="slide" data-markdown type="text/html">
# Anti pattern

> Common response to a recurring problem that is usually ineffective and risks being highly counterproductive.

1. Despite initially appearing to be an appropriate and effective response to a problem, has more bad consequences than good ones.

1. Another solution exists to the problem the anti-pattern is attempting to address.

* Book AntiPatterns (1998) popularized the idea (not only to software use)
* Inspired by the book Design Patterns (GoF) 

<p class="footnote">Read on: https://en.wikipedia.org/wiki/Anti-pattern</p>

</script>



<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Examples of design pattern
</script>



<script data-role="slide" data-markdown type="text/html">
# Examples

* (Singleton)
* Dependency injection
* Factory method
* Abstract factory
* Builder
* Observer / Publisher-Subscriber

</script>



<script data-role="slide" data-markdown type="text/html">
# Dependency injection

* An object or function receives other objects or functions that it depends on
* Passing parameters to a method, inject the dependencies
* Code to interface not class
* Constructor, setter, interface injection

> A form of inversion of control, dependency injection aims to separate the concerns of constructing objects and using them, leading to loosely coupled programs.

<p class="footnote">Read more: https://en.wikipedia.org/wiki/Dependency_injection</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Dependency injection...

```
DiceHand hand = new DiceHand();

hand.add(new DiceSixSided());
hand.add(new DiceSixSidedGraphic());
```

```
class DiceHand {
    public void add (Dice die) { }
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Factory method

* Creational pattern to create ONE object
* Call a factory method instead of the constructor
* Create objects without having to specify the exact class of the object

<p class="footnote">Read more: https://en.wikipedia.org/wiki/Factory_method_pattern</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Factory method...

```
Dice die;

die = DiceFactory.create("SixSided");
die.roll();

die = DiceFactory.create("Winner");
die.roll();

die = DiceFactory.create("Looser");
die.roll();
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Factory method...

```
class DiceFactory {
    public static Dice create(String type) throws Exception {
        switch (type) {
            case "SixSided":
                return new DiceSixSided();
            case "Winner":
                return new DiceWinner();
            case "Looser":
                return new DiceLooser();
            default:
                throw new Exception("Not supported Dice type");
        }
    }
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Abstract factory pattern

* Creational pattern to create a family of related objects
* Encapsulate object creation in a separate (factory) object
* Make the application independent of how its objects are created
* Make a class independent of how the objects it requires are created

> The class YatzyFactory could create all the individual classes for the game (Dice, DiceGraphic, DiceHand, Player, Game)

<p class="footnote">Read more: https://en.wikipedia.org/wiki/Abstract_factory_pattern</p>

</script>


<script data-role="slide" data-markdown type="text/html">
# Abstract factory pattern...

```
int main() {
    MazeGame game;
    MazeFactory factory;

    game.createMaze(factory);
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Abstract factory pattern...

```
void createMaze(MazeFactory& factory) {
    Maze* aMaze = factory.makeMaze();
    Room* r1 = factory.makeRoom(1);
    Room* r2 = factory.makeRoom(2);
    Door* aDoor = factory.makeDoor(r1, r2);

    aMaze->addRoom(r1);
    aMaze->addRoom(r2);

    r1->setSide(North, factory.makeWall());
    r1->setSide(East, aDoor);
    r1->setSide(South, factory.makeWall());
    r2->setSide(North, factory.makeWall());
    r2->setSide(West, aDoor);

    this->maze = aMaze;
}
```

</script>


<script data-role="slide" data-markdown type="text/html">
# Abstract factory pattern...

```
class MazeFactory {
    public:
        MazeFactory();
    
        virtual Maze* MakeMaze() const { return new Maze; }
        virtual Wall* MakeWall() const { return new Wall; }
        virtual Room* MakeRoom(int n) const { return new Room(n); }
        virtual Door* MakeDoor(Room* r1, Room* r2) const {
            return new Door(r1, r2);
        }
    }
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Create one/many objects

What is the difference between factory method and abstract factory?

> Factory Method pattern hides the construction of a **single object** whereas Abstract Factory hides the construction of a **family of related objects**.

</script>



<script data-role="slide" data-markdown type="text/html">
# Builder

* How can a class (the same construction process) create different representations of a complex object?
* How can a class that includes creating a complex object be simplified?
* Create a building, car, bicycle from its bits and pieces

</script>



<script data-role="slide" data-markdown type="text/html">
# Builder...

* Separate the construction of a complex object from its representation
* Encapsulate creating and assembling the parts of a complex object in a separate Builder object
* A class delegates object creation to a Builder object instead of creating the objects directly

<p class="footnote">Read more: https://en.wikipedia.org/wiki/Builder_pattern</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Builder...
    
```
// Builder
class FruitBuilder {
    String name, color, firmness;
    FruitBuilder setName(name)         { this.name     = name;     return this; }
    FruitBuilder setColor(color)       { this.color    = color;    return this; }
    FruitBuilder setFirmness(firmness) { this.firmness = firmness; return this; }
    Fruit build() {
        return new Fruit(this); // Pass in the builder
    }
}

// Usage
Fruit fruit = new FruitBuilder()
        .setName("apple")
        .setColor("red")
        .setFirmness("crunchy")
        .build();
```

</script>
    


<script data-role="slide" data-markdown type="text/html">
# Builder or factory?

* Factory method creates one object
* Abstract factory creates objects from a family of related objects
* Builder creates complex objects by partially building up or configuring the object
* In essence, a class delegates the object creation to other classes
* To become more loosely coupled

> Builder focuses on constructing a complex object step by step and returns the product as a final step

> Abstract Factory emphasizes a family of product objects (either simple or complex), the product gets returned immediately.

</script>



<script data-role="slide" data-markdown type="text/html">
# Observer

* Observer or Publish-Subscribe (behavioral)
* Listen to changes in objects and get notified
* Compare to how events works

> Define a one-to-many dependency between objects where a state change in one object results in all its dependents being notified and updated automatically.

<p class="footnote">Read more:<br>https://en.wikipedia.org/wiki/Observer_pattern<br>https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Observer...

```
public interface Subject {

	//methods to register and unregister observers
	public void register(Observer obj);
	public void unregister(Observer obj);
	
	//method to notify observers of change
	public void notifyObservers();
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Observer...

```
public interface Observer {
	
	//method to update the observer, used by subject
	public void update();
}
```

</script>



<!--
<script data-role="slide" data-markdown type="text/html">
# Other types of patterns

* MVC
* Database
* ... Look at https://www.martinfowler.com/eaaCatalog/

</script>
-->


<script data-role="slide" data-markdown type="text/html">
# Why patterns

* Someone else has probably solved your type of problem before, in a nice way
* Terminology, a way to communicate about code and architecture
* Quality assurance
* Best practice

</script>



<script data-role="slide" data-markdown type="text/html">
# Reading material

1. Catalog of Patterns of Enterprise Application Architecture
    * https://www.martinfowler.com/eaaCatalog/

1. Object-Oriented Thought Process, 5th Edition
    * 15 Design Patterns

</script>


<script data-role="slide" data-markdown type="text/html">
# Summary

* About software design patterns
* Example on patterns
    * Singleton
    * Dependency injection
    * Factory method
    * Abstract factory
    * Builder
    * Observer / Publish-Subscribe

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# The End
</script>



<script data-role="slide" data-markdown type="text/html">
</script>
