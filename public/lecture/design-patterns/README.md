---
author: mos
revision: 
    2023-10-16: "(A, mos) first version"
---
Programming with Java - Software design patterns
====================

[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/design-patterns/slide.html)

This lecture talks about software design patterns and why they can be useful strategies when developing software. Some basic design patterns are introduced and discussed upon.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/design-patterns/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->


<!--
Exercise
------------------------

The following exercise is related to this lecture.

* [User interface with JavaFX](../../../exercise/user-interface-javafx/README.md)
-->


Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [Design patterns](../../../sample/design-patterns/)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Wikipedia on "[Software design pattern](https://en.wikipedia.org/wiki/Software_design_pattern)" for a quick overview of some design patterns.

    * What is a [Anti pattern](https://en.wikipedia.org/wiki/Anti-pattern)?

1. Various design patterns on Wikipedia

    * [Singleton pattern](https://en.wikipedia.org/wiki/Singleton_pattern)
    * [Dependency injection](https://en.wikipedia.org/wiki/Dependency_injection)
    * [Factory method pattern](https://en.wikipedia.org/wiki/Factory_method_pattern)
    * [Abstract factory pattern](https://en.wikipedia.org/wiki/Abstract_factory_pattern)
    * [Builder pattern](https://en.wikipedia.org/wiki/Builder_pattern)
    * [Publish–subscribe pattern](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern)
    * [Observer pattern](https://en.wikipedia.org/wiki/Observer_pattern)

1. Website for "[Catalog of Patterns of Enterprise Application Architecture](https://www.martinfowler.com/eaaCatalog/)" where you can look up the definition of some more design patters.
