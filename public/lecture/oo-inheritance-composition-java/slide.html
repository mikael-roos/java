<!doctype html>
<html class="theme-5">
<meta charset="utf-8" />
<link href="../html-slideshow.bundle.min.css" rel="stylesheet" />
<link href="../style.css" rel="stylesheet" />
<script src="https://dbwebb.se/cdn/js/html-slideshow_v1.1.0.bundle.min.js"></script>

<title>Programming in Java: Packages, inheritance and composition</title>

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Programming in Java
## Packages, inheritance and composition
### Mikael Roos
</script>

<!--
TODO

* Add terminology?
* Too short?
* Improve flow between text and images?
* Might still get usefuls stuff from older pres:
    * inheritance-and-composition-with-java/slide.html
    * oo-inheritance-composition/slide.html
-->

<script data-role="slide" data-markdown type="text/html">
# Agenda

* Package, directory structure, import
* Inheritance
* Composition

<!--
* Terminology
-->

</script>



<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Package
## Structure and import code modules
</script>



<script data-role="slide" data-markdown type="text/html">
# Import

* Java class library modules are structured into packages
* A package is "imported" when used

```
import java.lang.Math;

public class Dice {

    public final int roll () {
        this.lastRoll = (int) (Math.random() * MAX_SIDES + 1);
```

<p class="footnote">https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/lang/Math.html</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Package

* A way to organise code into larger (and reusable) modules
* The code lives in the scope of the namespaced package
* Share your package with other and reuse other packages
* Package name is like a reversed domain name

```
package com.example.dice;
package se.mos.dice;
```

<p class="footnote">https://docs.oracle.com/javase/tutorial/java/concepts/package.html</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# File structure

* Structure your project into a directory structure
* The directory structure should match the package name(s)
* The main program can be part of the package, or reside outside of it

```
.
└── src
    ├── Main.java
    └── com
        └── example
            └── dice
                └── Dice.java
                └── DiceGraphic.java
                └── DiceHand.java
                └── Main.java
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Import your package

* Import all or some parts of the package

```
// import com.example.dice.*;
import com.example.dice.Dice;
import com.example.dice.DiceHand;

public class Main {
    
    public static void main(String[] args) {
        Dice die = new Dice();
        DiceHand hand = new DiceHand();
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Compile, run with package

Move to the top of the source directory

```
// Main is on the top level
javac se/mos/dice/*.java Main.java
java Main
```

```
// Main is in the package structure
javac se/mos/dice/*.java
java se.mos.dice.Main
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Default imports

The package `java.lang` is imported by default.

* Object - All Java classes inherit from Object.
* String - Used for working with text strings.
* Math - Contains mathematical functions like sqrt, pow, sin, etc.
* System - Used for input and output operations, such as System.out.println.
* Throwable - The base for exception handling (Exception, Error).
* Integer, Double, Boolean, etc. - Wrapper classes for primitive types.

<p class="footnote">https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/lang/package-summary.html</p>

</script>



<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Inheritance
## Reuse by extending an existing class
</script>



<script data-role="slide" data-markdown type="text/html">
# Inheritance Dice, DiceGraphic

```
class Dice {
    private static final SIDES = 6;
    protected int lastRoll = 1;

    public int roll () {}
}
```

```
class GraphicalDice extends Dice {
    String representation = "img/dice.ico"

    public String toString () {}
}
```

</script>



<script data-role="slide" data-markdown type="text/html" class="center full">
<figure><img src="img/inheritance-dice-dicegraphic.png" style="width: 40%"></figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Inheritance

Inheritance is a fundamental concept in Java that allows one class (called the subclass or child class) to inherit the fields and methods of another class (called the superclass or parent class). It promotes code reuse and helps in organizing related classes into a hierarchy.

</script>



<script data-role="slide" data-markdown type="text/html">
# Inheritance

* A (sub)class can inherit from another class, 
    * it "extends" its superclass, or
    * it "specializes" its parentclass, or
    * it "redefines" its parent/base class
* "Is a" relationship
    * Superman "is a" Person
    * DiceGraphic "is a" Dice
    * Mountainbike "is a" Bike

</script>



<script data-role="slide" data-markdown type="text/html">
# Inheritance

* Do not change the parent signature
    * Keep the public API of the parent class

* Add or modify (override) methods (behaviour)
* Add properties

* In child class
    * Items marked as `final` can not be overridden
    * Items marked as protected are visible
    * Items marked as private are not visible

* Reuse all parts of the parent class

</script>



<script data-role="slide" data-markdown type="text/html">
# Inheritance example

```
class MountainBike extends Bicycle {
    // Another set of gear for sustainability
}
```

```
class Superman extends Person {
    Boolean abilityToFly = TRUE
}
```

</script>



<script data-role="slide" data-markdown type="text/html" class="center full">
<figure><img src="img/acmebycycle-bycycle.png" style="width: 50%"></figure>
</script>



<script data-role="slide" data-markdown type="text/html" class="center full">
<figure><img src="img/uml-person-supreman.png" style="width: 50%"></figure>
</script>



<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Composition
## A class is composed of several objects
</script>



<script data-role="slide" data-markdown type="text/html">
# Composition

Composition is a design principle in Java where one class contains an instance of another class as a field. Instead of inheriting from a class (as in inheritance), composition models a "has-a" relationship, meaning one object is made up of or uses other objects.

</script>



<script data-role="slide" data-markdown type="text/html">
# Composition...

* The relationship between object/classes "has/have a"
* An object that consists of other objects

</script>



<script data-role="slide" data-markdown type="text/html">
# Composition...

A Bycycle has two Wheels and Steering.

```
class Bycycle {
    Wheel front;
    Wheel back;
    Steering handlebars;
}
```

</script>



<script data-role="slide" data-markdown type="text/html" class="center full">
<figure><img src="img/bycycle-wheel-steering2.png" style="width: 100%"></figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Composition...

* A class contain other classes as members
* A DiceHand holds one Dice

```
public class DiceHand {

    private Dice die;

    public Dice () {
        this.die = new Dice();
    }
```

</script>



<script data-role="slide" data-markdown type="text/html" class="center full">
<figure><img src="img/composition-dicehand.png" style="width: 100%"></figure>
</script>


    
<script data-role="slide" data-markdown type="text/html">
# Composition and aggregation

* Variations of the "has a" relation
* Difference is how strong the relation is between objects
* Composition is a strong relation (filled romb)
    * House has Room(s)
    * The Room can not exist without the house
* Aggregation is a weaker relation (clear romb)
    * House has Tenant(s)
    * The Tenants can live as independent objects without the House
* The actual implementation is the same in Java
* (Only relates to how you draw a class diagram)

</script>



<script data-role="slide" data-markdown type="text/html" class="center full">
<figure><img src="img/composition-aggregation.png" style="width: 100%"></figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Inheritance versus composition

> "Favor composition over inheritance"

<p class="footnote">https://en.wikipedia.org/wiki/Composition_over_inheritance</p>

</script>


<!--
<script data-role="slide" data-markdown type="text/html" class="titlepage center">
# Terminology
</script>



<script data-role="slide" data-markdown type="text/html">
# Terminology

* package
* import
* Dice extend Object
* Object.toString
* Object.equals
* CheatDice extends Dice
* call parent constructor
* visibility - public, private, protected
* Overloading methods
* Record
* getter
* setter 
* Exception, try-catch and exception strategy.
* Constant
* Enum
* Polymorfism
* Object reference
* final
* nested classes
* interface
* abstract 

</script>



<script data-role="slide" data-markdown type="text/html">
# Main

The main program in Java looks like this. The class can be named anything, but the signature for the main method must be like this.

```
public class Main {
    public static void main(String[] args) {

    }
}
```

</script>
-->



<script data-role="slide" data-markdown type="text/html">
# Summary

* Package
    * Directory structure
    * import
* Inheritance
    * Subclass, superclass, parent, base class
    * "Is a" relationship
    * Specialize an existing class
* Composition
    * Class holds other objects
    * Relationship "has a"
* "is a" versus "has a"
    * Prefer "has a"

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# The End
</script>



<script data-role="slide" data-markdown type="text/html">
</script>
