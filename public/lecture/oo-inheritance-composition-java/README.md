---
author: mos
revision: 
    2024-11-25: "(A, mos) Shorter lecture on inheritance and composition."
---
[![Slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/oo-inheritance-composition-java/slide.html)

Programming in Java: Packages, inheritance and composition
====================

Talk about packages and import on how to structure Java code into submodules and their own namespace. Then introduce the concept on inheritance and how to think and structure code when working with it. Composition is related and another way to structure code. Inheritance and composition are two basic foundations on how to organise your code in an object oriented manner.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/oo-inheritance-composition-java/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [Split your Java project into files and packages](../../../exercise/package/README.md)



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [Example on packages](../../../exercise/package/src/)
* [Example on inheritance and composition](../../../exercise/inheritance-composition/src/)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. The Java Tutorials
    * [Lesson: Inheritance](https://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html) (approx 10 pages)
