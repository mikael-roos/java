---
author: mos
revision: 
    2023-08-03: "(A, mos) first version"
---
Programming with Java - Inheritance and composition
====================

Walk through an example of classes Dice, DiceGraphic and DiceHand to learn how to structure a Java programs into packages with inheritance and composition.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/inheritance-and-composition-with-java/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [Split your Java project into files and packages](../../exercise/package/README.md)



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [dice](../../../sample/dice)
* [dice_graphic](../../../sample/dice_graphic)
* [dice_hand](../../../sample/dice_hand)
    * [array](../../../sample/array)

<!--
* [static](../../../sample/static)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Core Java SE 9 for the Impatient, 2nd Edition
    * Chapter 2. Object-Oriented Programming
    * Chapter 4. Inheritance and Reflection

1. Oracle website on Java
    * [Package](https://docs.oracle.com/javase/tutorial/java/concepts/package.html)

1. The Java Tutorials
    * [Lesson: Inheritance](https://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html) (approx 10 pages)

1. [Dia](https://wiki.gnome.org/Apps/Dia) an opensource tool to draw UML diagrams (and more)