<!doctype html>
<html class="theme-5">
<meta charset="utf-8" />
<link href="../html-slideshow.bundle.min.css" rel="stylesheet" />
<link href="../style.css" rel="stylesheet" />
<script src="https://dbwebb.se/cdn/js/html-slideshow_v1.1.0.bundle.min.js"></script>

<title>Inheritance and composition | Programming with Java</title>

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Inheritance and composition
## Programming with Java
### Mikael Roos
</script>



<script data-role="slide" data-markdown type="text/html">
# Agenda

* Packages / modules
* Class, property, method, object
* Inheritance
* Composition
* Related concepts

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# A dice program
## with inheritance, composition and structured in a package
</script>



<script data-role="slide" data-markdown type="text/html">
# Import

* A package is "imported" when used
* Java class library modules are structured into packages

```
import java.lang.Math;

public class Dice {

    public final int roll () {
        this.lastRoll = (int) (Math.random() * MAX_SIDES + 1);
```

<p class="footnote">https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/lang/Math.html</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Package

* A way to organise code into larger (and reusable) modules
* The code lives in the scope of the namespaced package
* Share your package with other and reuse other packages
* External modules are structured as packages

```
package com.example.dice;
```

<p class="footnote">https://docs.oracle.com/javase/tutorial/java/concepts/package.html</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# File structure

* Structure your project into a directory structure
* The directory structure should match the package name(s)
* The main program can be part of the package, or reside outside of it

```
.
└── src
    ├── Main.java
    └── com
        └── example
            └── dice
                └── Dice.java
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Import your package

* Import parts from your own package

```
// import com.example.dice.*;
import com.example.dice.Dice;

public class Main {
    
    public static void main(String[] args) {
        Dice die = new Dice();
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Class

```
package com.example.dice;

public class Dice { }
```

* A class can be standalone or part of a package
* A class can have a visibility within the package
    * no classifier (class visible within package)
    * public (everybody can use it outside of the package)
    * abstract (needs to be subclassed)
    * final (can not be subclassed)

</script>



<script data-role="slide" data-markdown type="text/html">
# Properties (state)

```
public class Dice {

    protected int lastRoll = 0;

    private final int MAX_SIDES = 6;
}
```

* Instance variables (properties, member variables, fields)
* A property can have a visibility
    * public, protected, private, static, final

</script>



<script data-role="slide" data-markdown type="text/html">
# Methods (behaviour)

```
public class Dice {
    public final int roll () {
        lastRoll = (int) (Math.random() * MAX_SIDES + 1);
        return lastRoll;
    }

    public int getValue () {
        return lastRoll;
    }
}
```

* A method can have a visibility
    * public, protected, private, static, abstract, final
* Accessor and Mutator methods may be used to set/get values of private/protected properties

</script>



<script data-role="slide" data-markdown type="text/html">
# this

* References the current object
* Use for verbosity, you may emit "this"

```
public final int roll () {
    this.lastRoll = (int) (Math.random() * MAX_SIDES + 1);
    return this.lastRoll;
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Instantiate

* Instantiate a class into an object and invoke its method
* The object is an instance of the class

```
Dice die = new Dice();

int roll;
roll = die.roll();
```

</script>



<script data-role="slide" data-markdown type="text/html" class="center">
<figure>
    <img src="img/class-dice.png" style="width: 60%">
</figure>
</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Inheritance
</script>



<script data-role="slide" data-markdown type="text/html">
# Extend

* A class can inherit from another class
* Inherit, extend, subclass, specialize
* The subclass inherits all public/protected properties/methods
```
public class DiceGraphic extends Dice {

    private String[] representation = {" ","⚀","⚁","⚂","⚃","⚄","⚅"};

    @Override
    public String toString () {
        return representation[lastRoll];
    }
}
```

Methods can be overridden.

</script>



<script data-role="slide" data-markdown type="text/html">
# The Object

* The parent superclass of all objects
* Supplies methods like toString(), hashCode(), equals(), clone(), getClass()

```
public class Dice { }
    
// implies this
public class Dice extends Object { }
```

```
Dice die = new Dice();

System.out.printlin(die.hashCode());
```

</script>


    
<script data-role="slide" data-markdown type="text/html">
# Constructor

* Construct and initiate the object

```
public class Dice {
    public Dice () {
        this.MAX_SIDES = 6;
    }

    public Dice (int sides) {
        this.MAX_SIDES = sides;
    }
}
```

```
die = new Dice();
die = new Dice(12);
```

<p class="footnote">Having several methods with the same name is called method overloading</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Calling parent constructor

* Parent constructor `super()` is called implicitly
* Sometimes its needed to call explicitly, like `super(sides)`

```
public class DiceGraphic extends Dice {

    public DiceGraphic (int sides) {
        // super();
        super(sides);
    }
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Using super

* Use super.method() and super.property to access details in the parent class
* Or emit super and it will find it

```
public String toString () {
    return representation[super.lastRoll];
}
```

```
public String toString () {
    return representation[lastRoll];
}
```

</script>



<script data-role="slide" data-markdown type="text/html" class="center">
<figure>
    <img src="img/inheritance-dice-dicegraphic.png" style="width: 60%">
</figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Using inheritance

* Extend a base class and add more features to it
* Specialize it by adapting its behaviour
* A way to organise and reuse your code
* The relation is "is a",
    * the DiceGraphic is a Dice

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Composition
</script>



<script data-role="slide" data-markdown type="text/html">
# Composition

* A class contain other classes as members
* A DiceHand holds one Dice

```
public class DiceHand {

    private Dice die;

    public Dice () {
        this.die = new Dice();
    }
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Composition...

* Injecting the objects
* A DiceHand holds many Dice

```
public class DiceHand {

    private ArrayList<Dice> hand = new ArrayList<Dice>();

    public void add (Dice die) {
        hand.add(die);
    }
```

```
DiceHand hand = new DiceHand();

hand.add(new Dice());
hand.add(new DiceGraphic());
```

</script>



<script data-role="slide" data-markdown type="text/html" class="center">
<figure>
    <img src="img/composition-dicehand.png" style="width: 100%">
</figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Composition

* The relation is "has a"
* The DiceHand has many Dice objects
* "Favor composition over inheritance"

<p class="footnote">https://en.wikipedia.org/wiki/Composition_over_inheritance</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Related concepts
</script>



<script data-role="slide" data-markdown type="text/html" class="center">
# Related

* Composition and aggregation
* Unified Modelling language (UML)
* Overloading
* Dependency injection
* Polymorfism
* Object reference
* Pass by value

</script>



<script data-role="slide" data-markdown type="text/html">
# Composition and aggregation

* Variations of the "has a" relation
* Difference is how strong the relation is between objects
* Composition is a strong relation (filled romb)
    * House has Room(s)
    * The Room can not exist without the house
* Aggregation is a weaker relation (clear romb)
    * House has Tenant(s)
    * The Tenants can live as independent objects without the House
* The actual implementaion is the same in Java

</script>



<script data-role="slide" data-markdown type="text/html" class="center">
# UML

* Unified Modelling language (UML)
* Class diagram
    * Show relations between classes
* Sequence diagram
    * Show how the program interacts over time

</script>



<script data-role="slide" data-markdown type="text/html" class="center">
<figure>
    <img src="img/UML_diagrams_overview.svg" style="width: 100%; background-color: white;">
</figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Overloading

* Method overloading
* Same name of the method but different parameter list
* The signature of the method

```
public Dice () {
    this.lastRoll = 0;
    this.MAX_SIDES = 6;
}

public Dice (int sides) {
    this.lastRoll = 0;
    this.MAX_SIDES = sides;
}
```

<p class="footnote">The return type does not affect the method signature (and a constructor does not have a return type...).</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Dependency injection

* Inject the dependencies
* Loose coupling between the classes

```
public class DiceHand {

    private ArrayList<Dice> hand = new ArrayList<Dice>();

    public void add (Dice die) {
        hand.add(die);
    }

}
```

Method will accept any object that has Dice as parent class.

</script>



<script data-role="slide" data-markdown type="text/html">
# Polymorfism

* Important aspect of oo programming languages
* Decide what type an object is and what method to call
* It will know the actual object and call the correct methods
    * The DiceHand holds Dice or DiceGraphic

```
public class DiceHand {

    public void print () {
        for (Dice die: this.hand) {
            die.toString();
        }
    }
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Object reference

* A variable holds a reference to the object
* References behave like pointers in C and C++
* Below example "points" to the same object

```
Dice die = new Dice();
die.roll();

Dice die2 = die;
```

</script>

    

<script data-role="slide" data-markdown type="text/html">
# Pass by value

* Send argument to method, passed by value
* The variabel, holding the reference to the object, is passed by its value

```
    printDie(die2);
}

protected static void printDie(Dice die) {
    System.out.println("The rolled die was: " + die.getValue());
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Visibility

* Apply on class, method, property
* public
* protected (visible for subclasses)
* private (visible within class)

</script>



<!--
<script data-role="slide" data-markdown type="text/html">
# Modifiers

* static
* abstract
* final

</script>


    
<script data-role="slide" data-markdown type="text/html">
# Static variable

* Part of the class, same for all objects
* Employee has a global id for all employees 

```
public class Employee {

    private static int lastId = 0;
    private int id;

    public Employee() {
        lastId++;
        id = lastId;
    }
}
```

</script>


    
<script data-role="slide" data-markdown type="text/html">
# Static constant

* Part of the class, same for all objects

```
public class Math {
    public static final double PI = 3.14159265358979323846;
}

pi = Math.PI;
```

</script>


    
<script data-role="slide" data-markdown type="text/html">
# Static method

* Do not operate on object/instance variables

```
public class Math {
    public static double pow(double base, double exponent) {
        
     }
}

result = Math.pow(2, 2);
```

</script>


    
<script data-role="slide" data-markdown type="text/html">
# Abstract

* In an inheritance hirarchy, the Circle and Rectangle extends the Shape
* Declare Shape as abstract, it can not be instantiated

```
public abstract class Shape {
    // declare fields
    // declare non abstract methods
    abstract void draw();
}
```

```
public class Circle extends Shape {
    public void draw() {
    }
}
```

</script>



<script data-role="slide" data-markdown type="text/html">
# Final

* Properties can be instantiated but not changed
* Method can not be overridden by subclasses

```
public class Dice {
    private final int MAX_SIDES = 6;

    public final int roll () {
        this.lastRoll = (int) (Math.random() * MAX_SIDES + 1);
        return this.lastRoll;
    }
} 
```

</script>
    


<script data-role="slide" data-markdown type="text/html">
# Nested classes

* Nested and inner classes

```
public class Invoice {

    private static class Item { // Item is nested inside Invoice
```

```
public class Network {

    public class Member { // Member is an inner class of Network
```

<p class="footnote">Nested (static) can not access outside class, as can inner classes.<br>
A Member knows what Network it is within.<br>
A Item has no access to the Invoice.</p>

</script>

    

<script data-role="slide" data-markdown type="text/html">
# Good and clean code

* Do one thing only and do it well
* Encapsulation
    * Provide a public API, do not change it
    * Hide your implementation details from other
* Injection (versus hardcode dependency within class)
* State and Behaviour
    * Accessors (get) versus Mutators (set)
    * Accessors are easier to test
    * Methods returning a value, not changing state, is easier to test

</script>
-->



<script data-role="slide" data-markdown type="text/html">
# Summary

* package, import
* class
* object with properties and methods
* extend (is a) and composition (has a)
* visibility
    * private, protected, public
<!--
* static, final, abstract
-->
</script>
    


<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# The End
</script>



<script data-role="slide" data-markdown type="text/html">
</script>
