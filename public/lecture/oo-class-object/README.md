---
author: mos
revision: 
    2024-11-18: "(B, mos) Rewritten to only touch classes and objects."
    2023-08-08: "(A, mos) Reviewed from last year, added pictures."
---
[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/oo-class-object/slide.html)

Object-orientation - Programming concepts
====================

Starting with talking about some of the different programming paradigms and then moving on to talk about object-orientation as a programming technique and what aspects are important. Touching basics such as class, object, behaviour (methods), state (properties)inheritance, interface, composition and what information hiding and encapsulation means.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/oo-class-object/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->


<!--
Exercise
------------------------

The following exercise is related to this lecture.

* [Split your Java project into files and packages](../../exercise/package/README.md)
-->


<!--
Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [array](../../../sample/array)
* [arraylist](../../../sample/arraylist)
* [iterator_collection](../../../sample/iterator_collection/)
* [map](../../../sample/map/)
* [queue](../../../sample/queue/)
* [deque](../../../sample/deque/)
-->


Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Oracle website with [Lesson: Object-Oriented Programming Concepts](https://docs.oracle.com/javase/tutorial/java/concepts/index.html) for Java.

1. [Wikipedia: Information hiding](https://en.wikipedia.org/wiki/Information_hiding)

1. [Wikipedia: Encapsulation (computer programming)](https://en.wikipedia.org/wiki/Encapsulation_(computer_programming))
