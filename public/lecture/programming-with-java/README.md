---
author: mos
revision: 
    2024-11-11: "(A, mos) first version"
---
Programming with Java - Basic constructs
====================

[![slide](img/slide.png)](https://mikael-roos.gitlab.io/java/lecture/programming-with-java/slide.html)

This lecture is about practicing the basic constructs in the Java programming language to get going to code.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/programming-with-java/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->

<!--
TODO

* Ensure that the lecture includes 
    * Java expression, statments, data types, if-statements and loops
    * Main program with arguments, functions
-->



Exercise
------------------------

The following exercise is related to this lecture.

* [Lab 01: Data types, expressions, statements and built in functions](../../../lab/lab_01/README.md)


<!--
Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [Design patterns](../../../sample/design-patterns/)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. [The Java™ Tutorials: Primitive Data Types](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)

1. [The Java™ Tutorials: Strings](https://docs.oracle.com/javase/tutorial/java/data/strings.html)

1. [The Java™ Tutorials: Arrays](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html)

1. [The Java™ Tutorials: Expressions, Statements, and Blocks](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/expressions.html)

1. [The Java™ Tutorials: Control Flow Statements](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/flow.html)
