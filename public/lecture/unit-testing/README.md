---
author: mos
revision: 
    2023-08-11: "(A, mos) first version"
---
Software testing and unit testing
====================

Learn about software testing, unit testing and test driven development. Why should software be tested and what are the different types of tests that can be done. Why is unit testing importat and what does test-driven development add to the process of developing and testing software.


<!--
TODO

* 
-->

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/java/lecture/unit-testing/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->


<!--
Exercise
------------------------

The following exercise is related to this lecture.

* [Unit testing with JUnit](../../../exercise/junit/README.md)



Code samples
------------------------

The following code examples can be reviewed to further study the topic of this lecture.

* [junit5](../../../sample/junit5/)
-->


Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

Start by getting a general overview of software testing by reviewing Wikipedia pages.

* "[Software testing](https://en.wikipedia.org/wiki/Software_testing)"
* "[Software bug](https://en.wikipedia.org/wiki/Software_bug)"
* "[Software verification and validation](https://en.wikipedia.org/wiki/Software_verification_and_validation)"
* "[List of (known) software bugs](https://en.wikipedia.org/wiki/List_of_software_bugs)" to learn about known bugs and their consequences.

<!--
* Read on unit testing
* Read on TDD
-->

SWEBOK is a overview of the different parts of software engineering and tests being one part of it.

* Wikipedia about [SWEBOK](https://en.wikipedia.org/wiki/Software_Engineering_Body_of_Knowledge)
* IEEE Computer Society with "[The Guide to the Software Engineering Body of Knowledge (SWEBOK Guide)](https://www.computer.org/education/bodies-of-knowledge/software-engineering)"


<!--

* Good for "how to write testable code"

* Läs om "[Side effect (computer science)](https://en.wikipedia.org/wiki/Side_effect_(computer_science))" och lära sig om referens transparens, idempotens och deterministisk.

    * Läs om den relaterade termen "[Pure function](https://en.wikipedia.org/wiki/Pure_function)".
    * Läs om den relaterade termen "[Deterministic algorithm](https://en.wikipedia.org/wiki/Deterministic_algorithm)".

* Försök få en förståelse för begreppet "[False positives and false negatives](https://en.wikipedia.org/wiki/False_positives_and_false_negatives)" och hur det hänger ihop med enhetstester.

-->
